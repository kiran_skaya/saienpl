<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>About Us | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<?php include('header.php');?>

		<!-- Page Title
		============================================= -->
		
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
																	<h3 style="text-align:center;"> About Us:</h3>



					<div class="row col-mb-50 mb-0">

						<div class="col-lg-12">

							<div class="heading-block fancy-title border-bottom-0 title-bottom-border">
								<h4>Why choose <span>Us</span>.</h4>
							</div>

							<p>Established in the year <b>2005,</b> Sai Enterprises is one of the well-known distributor, trader and supplier of a  wide range of automation products. Our range covers <b>Liquid Level Gauges, Liquid Level Switches, Solid Level Switches, Channel Scanners, Process Control Instruments, Analytical Instruments and many more</b>. These high performance and innovative components enhance the productivity of various plants and have excellent levels of performance and reliability, guaranteeing high level of results. The firm is a well known Sole Proprietorship concern located at Vijaynagar, Bengaluru, Karnataka from where we are catering to the entire needs of our clients in a highly effective manner. We intend is to achieve maximum client's satisfaction through our range of automation products and have been <b>successful to a great extent with our tie- ups with distinguished manufactures like ERI, Tectrol, Vatts, Eureka, Nippon and Aditi</b>. We have extremely talented sourcing agents who meticulously survey the market for trustable and reliable vendors who help us in upholding our credibility in the market. We have gained excellent competency in offering quality oriented products at cost effective price owing to or massive experience in procurement and supply of components</p>

							<p>The firm has already realized many of its dreams and is on the way to accomplish many more owing to the kind support of its Proprietor <b>Mr. Ajith Kumar Nimbalkar</b>. He has worked day in and day out in making the organization a highly reputed and trustworthy name in the market. It is owing to his enthusiasm that we have made a steady progress in the market and are moving ahead to gain more ground in arena of cut throat competition.


							</p>

						</div>

						

					</div>

				</div>

				<div class="section m-0">
					<div class="container clearfix">

						<div class="row col-mb-50">

							<div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn">
								<i class="i-plain i-xlarge mx-auto icon-line2-directions"></i>
								<div class="counter counter-lined"><span data-from="10" data-to="100" data-refresh-interval="50" data-speed="2000"></span>K+</div>
								<h5>Number of Devices</h5>
							</div>

							<div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="200">
								<i class="i-plain i-xlarge mx-auto mb-0 icon-line2-graph"></i>
								<div class="counter counter-lined"><span data-from="45" data-to="6750" data-refresh-interval="100" data-speed="2500"></span>+</div>
								<h5>Completion Projects</h5>
							</div>

							<div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="400">
								<i class="i-plain i-xlarge mx-auto mb-0 icon-line2-layers"></i>
								<div class="counter counter-lined"><span data-from="10" data-to="57" data-refresh-interval="25" data-speed="3500"></span>*</div>
								<h5>On going projects</h5>
							</div>

							<div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="600">
								<i class="i-plain i-xlarge mx-auto mb-0 icon-line2-clock"></i>
								<div class="counter counter-lined"><span data-from="60" data-to="1200" data-refresh-interval="30" data-speed="2700"></span>k+</div>
								<h5>Man hours spent</h5>
							</div>

						</div>

					</div>
				</div>
			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>