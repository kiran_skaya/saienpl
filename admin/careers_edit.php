<?php
include '../connection.php';

$crid = $_GET['crid'];
$editquery="SELECT * FROM careers WHERE career_id='".$crid."' ";
$editqueryresult = mysqli_query($con,$editquery);
while ($row=mysqli_fetch_array($editqueryresult))
{
	$id = $_GET['crid'];
	$job_pos = $row['job_position'];
    $job_exp = $row['experiance'];
    $job_desc = $row['job_description'];
}

if (isset($_POST['submit']))
{
    $job_pos = $_POST['jpos'];
    $job_exp = $_POST['jexp'];
    $job_desc = $_POST['jdesc'];

    $careerquery = "UPDATE careers SET job_position='".$job_pos."', experiance='".$job_exp."', job_description='".$job_desc."' WHERE career_id='".$crid."' ";
    $careerqueryresult = mysqli_query($con,$careerquery);
    if ($careerqueryresult==1)
    {
        header('location:careers.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit Career</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

    <!-- <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                jpos:"required",
                jexp:"required",
                jdesc:"required"
            },
            messages:{
                jpos:"<span>*Enter Job Position</span>",
                jexp:"<span>*Enter Experiance</span>",
                jdesc:"<span>*Enter Job Description</span>"
            }
        })
    })
</script> -->

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Edit Career</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Job Position</label>
                                <input type="text" class="form-control" name="jpos" value="<?php echo $job_pos; ?>" id="jpos" >
                                </div>
                            </div>

                         <div class="row">
                                <div class="col-sm-4">
                                <label>Experiance</label>
                                <input type="text" class="form-control" name="jexp" value="<?php echo $job_exp; ?>" id="jexp">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Job Description</label>
                                <input type="text" class="form-control" name="jdesc" value="<?php echo $job_desc; ?>" id="jdesc">
                                </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="careers.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="submit">Save</button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
</body>

</html>
