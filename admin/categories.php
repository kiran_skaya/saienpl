<?php

include '../connection.php';
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $sql = "update category set category_status = 0 where category_id = $id";
    $con->query($sql);
    header("location:categories.php");
} 
$sql = "select * from category order by product_order";
//$result = mysqli_query($con,$sql);
$result        = $con->query($sql);
$categorylist = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($categorylist, $row);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Categories</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    function ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?category_id='+id;
      }
    }
  </script>

<style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   #page_list li
   {
    padding:16px;
    background-color:#f9f9f9;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   #page_list li.ui-state-highlight
   {
    padding:24px;
    background-color:#ffffcc;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
  </style>


<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Categories</h3>
                        <a href="category_add.php" class="btn btn-primary">+ Add Category</a>
                    </div>
                    <table class="table table-striped" style="display: none;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <!-- <th>Status</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($categorylist as $category) { ?>

                            <tr>
                                <td><?php echo $category['category_name']; ?></td>
                                <!-- <td><?php if($category['category_status'] == 1){ echo "Active";} else{ echo "Inactive";} ?></td> -->
                                <td>
                                    <a href="category_add.php?id=<?php echo $category['category_id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <!-- &nbsp;&nbsp;&nbsp; -->
                                    <!-- <a href="categories.php?id=<?php echo $category['category_id'] ?>"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
                                </td>
                            </tr>
                        <?php }?>


                        </tbody>
                    </table>



                     <ul class="list-unstyled" id="page_list">
   <?php 
    foreach ($categorylist as $item) 
   {
     $id = $item['category_id'];
    echo '<li id="'.$item['category_id'].'"><a href="category_add.php?id='.$item['category_id'].'">'.$item['category_name'].'</a>';
    ?>

    <br/><br/>
    <span><a href="javascript:ondelete(<?php echo $id; ?>);">Delete</a></span></li>
     <?php
   }

   ?>
   </ul>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
 <script>
     var page_id_array = new Array();

$(document).ready(function(){
 $( "#page_list" ).sortable({
  placeholder : "ui-state-highlight",
  update  : function(event, ui)
  {
   $('#page_list li').each(function(){
    page_id_array.push($(this).attr("id"));
   });
   $.ajax({
    url:"update_category.php",
    method:"POST",
    data:{page_id_array:page_id_array},
    success:function(data)
    {
     //alert(data);
    }
   });
  }
 });

});
</script>
</body>

</html>