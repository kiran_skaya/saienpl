<?php

include '../connection.php';

$viewquery = "Select * from certificate";
$viewqueryresult = mysqli_query($con,$viewquery);
$certificate = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $certificate[$i]['id'] = $row['id'];
  $certificate[$i]['approval'] = $row['approval'];
  $certificate[$i]['standards'] = $row['standards'];
  $certificate[$i]['agency'] = $row['agency'];
  $certificate[$i]['product'] = $row['product'];
  $certificate[$i]['file'] = $row['file'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Certificates</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    function Ondelete(cid)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?cid='+cid;
      }
    }
  </script>

<style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   #page_list li
   {
    padding:16px;
    background-color:#f9f9f9;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   #page_list li.ui-state-highlight
   {
    padding:24px;
    background-color:#ffffcc;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
  </style>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Yogesh <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Certificate</h3>
                        <a href="certificate_add.php" class="btn btn-primary">+ Add Certificate</a>
                    </div>
                    <div class="header">
                      <table class="table table-bordered table-striped table-food">
                        <tr>
                          <th>Approval</th>
                          <th>Standards</th>
                          <th>Agency</th>
                          <th>Products</th>
                          <th>File</th>
                          <th>Edit&nbsp; | &nbsp;Delete</th>
                        </tr>
                          <?php
                          for ($i=0; $i<count($certificate); $i++)
                          {
                            $cid = $certificate[$i]['id'];
                            $appr = $certificate[$i]['approval'];
                            $std = $certificate[$i]['standards'];
                            $agn = $certificate[$i]['agency'];
                            $prod = $certificate[$i]['product'];
                            $file = $certificate[$i]['file'];
                            ?>

                        <tr>
                          <td><?php echo $appr; ?></td>
                          <td><?php echo $std; ?></td>
                          <td><?php echo $agn; ?></td>
                          <td><?php echo $prod; ?></td>
                          <td><?php if($file){?>
                            <a href="<?php echo 'uploads/'.$file;?>">view file</a>
                            <?php } else{ echo "file not found";} ?></td>
                          <td><a href="certificate_edit.php?cid=<?php echo $cid; ?>"><i class="fa fa-edit fa-2x"></i></a> <font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $cid; ?>);"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        
                      </table>
                    </div>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
  
</body>

</html>