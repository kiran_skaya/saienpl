<?php
ini_set('display-errors',1);
error_reporting(E_ALL);
include '../connection.php';

if (isset($_POST['submit']))
{
    $approval = $_POST['approval'];
    $standards = $_POST['standards'];
    $agency = $_POST['agency'];
    $product = $_POST['product'];

      $file_name = $_FILES['file']['name'];
      $file_tmp =$_FILES['file']['tmp_name'];
      move_uploaded_file($file_tmp,"uploads/".$file_name);


    $certquery = "INSERT INTO certificate(approval, standards, agency, product, file) VALUES('$approval','$standards','$agency','$product','$file_name') ";
    $certqueryresult = mysqli_query($con,$certquery);
    if ($certqueryresult==1)
    {
        echo "<script>parent.location='certificate.php'</script>";
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Certificate</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                approval:"required",
                standards:"required",
                agency:"required",
                product:"required",
                // file:"required"
            },
            messages:{
                approval:"<span>*Enter approval</span>",
                standards:"<span>*Enter  standards</span>",
                agency:"<span>*Enter  agency</span>",
                product:"<span>*Enter  product</span>",
                // file:"<span>*Choose file</span>"
            },
        })
    })
</script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Yogesh <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Add Certificate</h3>
                    </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Approval</label>
                                <input type="text" class="form-control" name="approval" id="approval">
                                </div>
                            </div>

                         <div class="row">
                                <div class="col-sm-4">
                                <label>Standards</label>
                                <input type="text" class="form-control" name="standards" id="standards">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Agency</label>
                                <input type="text" class="form-control" name="agency" id="agency">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Product</label>
                                <input type="text" class="form-control" name="product" id="product">
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>File</label>
                               <input type="file" name="file" id="file">
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="certificate.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="submit">Save</button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
</body>

</html>
