<?php
include '../connection.php';

$cid = $_GET['cid'];
$editquery="SELECT * FROM certificate WHERE id='".$cid."' ";

$editqueryresult= $con->query($editquery);
$certificate = $editqueryresult->fetch_assoc();

if (isset($_POST['submit']))
{
    $appr = $_POST['approval'];
    $std = $_POST['standards'];
    $agn = $_POST['agency'];
    $prod = $_POST['product'];


    $file_name =$_FILES['file']['name'];
    if ($file_name=="")
    {
        $file_name = $certificate['file'];
    }
    else
    {
        $file_name =$_FILES['file']['name'];
        $tempfile =$_FILES['file']['tmp_name'];
        move_uploaded_file($tempfile,"uploads/".$file_name);
    }
    
    
    $certquery = "UPDATE certificate SET approval='".$appr."', standards='".$std."', agency='".$agn."', product='".$prod."', file='".$file_name."' WHERE id='".$cid."' ";
    $certqueryresult = mysqli_query($con,$certquery);
    if ($certqueryresult==1)
    {
        header('location:certificate.php');
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit Certificate</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Edit Certificate</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Approval</label>
                                <input type="text" class="form-control" name="approval" value="<?php echo $certificate['approval']; ?>" id="approval" >
                                </div>
                            </div>

                         <div class="row">
                                <div class="col-sm-4">
                                <label>Standards</label>
                                <input type="text" class="form-control" name="standards" value="<?php echo $certificate['standards']; ?>" id="standards">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Agency</label>
                                <input type="text" class="form-control" name="agency" value="<?php echo $certificate['agency']; ?>" id="agency">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Product</label>
                                <input type="text" class="form-control" name="product" value="<?php echo $certificate['product']; ?>" id="product">
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>File</label>
                               <input type="file" name="file" id="file"><br>
                               <span><img width="160px" height="100px" src="<?php echo 'uploads/'.$certificate['file']; ?>"></span>
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="certificate.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="submit">Save</button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
</body>

</html>
