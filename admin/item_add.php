<?php
include '../connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from item where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}
$categoryquery = "select * from sub_category";
$subcategorylist =mysqli_query($con,$categoryquery);
$i=0;
while ($row=mysqli_fetch_array($subcategorylist))
{
    $subcategory[$i]['sub_category_id']=$row['sub_category_id'];
    $subcategory[$i]['sub_category_name']=$row['sub_category_name'];
    $i++;
}

if (isset($_POST['save']))
{

    $id_category = $_POST['id_category'];
    $item_name = $_POST['item_name'];
    $price = $_POST['price'];
    $model = $_POST['model'];
    $application_media = $_POST['application_media'];

    $image_1 = $_FILES['image1']['name'];
    $file_tmp1 =$_FILES['image1']['tmp_name'];
    move_uploaded_file($file_tmp1,"uploads/".$image_1);
   

    $image_2 = $_FILES['image2']['name'];
    $file_tmp2 =$_FILES['image2']['tmp_name'];
    move_uploaded_file($file_tmp2,"uploads/".$image_2);
    
    $image_3 = $_FILES['image3']['name'];
    $file_tmp3 =$_FILES['image3']['tmp_name'];
    move_uploaded_file($file_tmp3,"uploads/".$image_3);

    $image_4 = $_FILES['image4']['name'];
    $file_tmp4 =$_FILES['image4']['tmp_name'];
    move_uploaded_file($file_tmp4,"uploads/".$image_4);

    $video_name1 = $_POST['video_name1'];

    $video_file1 = $_FILES['video1']['name'];
    $video_tmp1 = $_FILES['video1']['tmp_name'];
    move_uploaded_file($video_tmp1,"uploads/".$video_file1);

    $video_name2 = $_POST['video_name2'];

    $video_file2 = $_FILES['video2']['name'];
    $video_tmp2 = $_FILES['video2']['tmp_name'];
    move_uploaded_file($video_tmp2,"uploads/".$video_file2);


    $brochure_file = $_FILES['brochure']['name'];
    $brochure_tmp = $_FILES['brochure']['tmp_name'];
    move_uploaded_file($brochure_tmp,"uploads/".$brochure_file);


    $description = $_POST['description'];
    $details = $_POST['details'];
    $warranty = $_POST['warranty'];

   
    $sql = "insert into item(id_category, item_name, price, model, application_media, image_1, image_2, image_3, image_4, video_name1, video1, video_name2, video2, brochure, description, details, warranty) values('$id_category', '$item_name', '$price', '$model', '$application_media', '$image_1', '$image_2', '$image_3', '$image_4', '$video_name1', '$video_file1', '$video_name2', '$video_file2', '$brochure_file', '$description', '$details', '$warranty') ";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: item.php");
}

if (isset($_POST['update']))
{

    $id_category = $_POST['id_category'];
    $item_name = $_POST['item_name'];
    $price = $_POST['price'];
    $model = $_POST['model'];
    $application_media = $_POST['application_media'];

    //image1
    $image1 = $_FILES['image1']['name'];
    if($image1=="")
    {
        $image1=$item['image_1'];
    }
    else
    {
        $image1 = $_FILES['image1']['name'];
        $file_tmp1 =$_FILES['image1']['tmp_name'];
        move_uploaded_file($file_tmp1,"uploads/".$image1);
    }

    //image2
    $image2 = $_FILES['image2']['name'];
    if($image2=="")
    {
        $image2=$item['image_2'];
    }
    else
    {
        $image2 = $_FILES['image2']['name'];
        $file_tmp2 =$_FILES['image2']['tmp_name'];
        move_uploaded_file($file_tmp2,"uploads/".$image2);
    }

    //image3
    $image3 = $_FILES['image3']['name'];
    if($image3=="")
    {
        $image3=$item['image_3'];
    }
    else
    {
        $image3 = $_FILES['image3']['name'];
        $file_tmp3 =$_FILES['image3']['tmp_name'];
        move_uploaded_file($file_tmp3,"uploads/".$image3);
    }
    
    //image4
    $image4 = $_FILES['image4']['name'];
    if ($image4=="")
    {
        $image4=$item['image_4'];
    }
    else
    {
        $image4 = $_FILES['image4']['name'];
        $file_tmp4 =$_FILES['image4']['tmp_name'];
        move_uploaded_file($file_tmp4,"uploads/".$image4);
    }

    $video_name1 = $_POST['video_name1'];

    //video1
    $video_file1 = $_FILES['video1']['name'];
    if ($video_file1=="")
    {
        $video_file1=$item['video1'];
    }
    else
    {
        $video_file1 = $_FILES['video1']['name'];
        $video_tmp1 =$_FILES['video1']['tmp_name'];
        move_uploaded_file($video_tmp1,"uploads/".$video_file1);
    }

    $video_name2 = $_POST['video_name2'];

    //video2
    $video_file2 = $_FILES['video2']['name'];
    if ($video_file2=="")
    {
        $video_file2=$item['video2'];
    }
    else
    {
        $video_file2 = $_FILES['video2']['name'];
        $video_tmp2 =$_FILES['video2']['tmp_name'];
        move_uploaded_file($video_tmp2,"uploads/".$video_file2);
    }

    //brochure
    $brochure_file = $_FILES['brochure']['name'];
    if ($brochure_file=="")
    {
        $brochure_file=$item['brochure'];
    }
    else
    {
        $brochure_file = $_FILES['brochure']['name'];
        $brochure_tmp =$_FILES['brochure']['tmp_name'];
        move_uploaded_file($brochure_tmp,"uploads/".$brochure_file);
    }

    $description = $_POST['description'];
    $details = $_POST['details'];
    $warranty = $_POST['warranty'];
    
    
    $id  = $item['id'];
   $updatequery = "update item set id_category = '$id_category', item_name = '$item_name', 
                price = '$price', model='$model', application_media='$application_media',
                image_1 = '$image1', image_2 = '$image2', image_3 = '$image3', 
                image_4 = '$image4', video_name1 = '$video_name1', video1='$video_file1', 
                video_name2='$video_name2', video2='$video_file2', brochure='$brochure_file', 
                description='$description', details='$details', warranty='$warranty' 
                where id = $id";
    $res=$con->query($updatequery);
    if ($res==1)
    {
        echo '<script>alert("Updated successfully")</script>';
        echo "<script>parent.location='item.php'</script>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Item</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_category:"required",
                item_name:"required"
            },
            messages:{
                id_category:"<span>*Select id_category</span>",
                item_name:"<span>*Enter Item Name</span>"
            },
        })
    })
</script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Item</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">

                              <div class="col-sm-4">

                                <label>Category</label>
                                <select name="id_category" class="form-control">
                                    <option value="">Please Select--</option>
                                    <?php
                                    $id_category=$item['id_category'];
                                    for ($i=0; $i <count($subcategory); $i++)
                                    { 
                                        $value=$subcategory[$i]['sub_category_id'];
                                        $label=$subcategory[$i]['sub_category_name'];
                                    ?>
                                    
                                        <option value="<?php echo $value;?>"
                                            <?php if($value==$id_category)
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                            </option>;
                                    <?php
                                    }
                                    ?>
                                </select>
                            
                            </div>
                           <div class="col-sm-4">
                                <label>Name</label>
                                <input type="text" class="form-control" name="item_name" value="<?php if (!empty($item['item_name'])) {echo $item['item_name'];}?>">
                            </div>
                             <div class="col-sm-4">
                                <label>Price</label>
                                <input type="text" class="form-control" name="price" value="<?php if (!empty($item['price'])) {echo $item['price'];}?>">
                            </div>

                            </div>
                            <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Model</label>
                                <input type="text" class="form-control" name="model" value="<?php if (!empty($item['model'])) {echo $item['model'];}?>">
                            </div>

                           
                            <div class="col-sm-4">
                                    <hr>
                                <label>Application Media</label>
                                <input type="text" class="form-control" name="application_media" value="<?php if (!empty($item['application_media'])) {echo $item['application_media'];}?>">
                            </div>
                           

                            </div>
                        <div class="row">
                          
                            <div class="col-sm-4">
                                <hr>
                                <label>Display in Home Page?</label>&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" class="checkbox-inline" name="display" value="true" <?php if (!empty($item['display']) && $item['display'] == 1) {echo "checked";}?>>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-1</label>
                               <input type="file" name="image1" >
                                <?php if (!empty($item['image_1']))
                                {
                                    ?>
                                    <img width="40px" height="50px" src="<?php echo 'uploads/'.$item['image_1']; ?>">
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-2</label>
                               <input type="file" name="image2" >
                               <?php if (!empty($item['image_2']))
                                {
                                    ?>
                                    <img width="40px" height="50px" src="<?php echo 'uploads/'.$item['image_2']; ?>">
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                                 <div class="col-sm-4">
                                <hr>
                                <label>Image-3</label>
                               <input type="file" name="image3" >
                               <?php if (!empty($item['image_3']))
                                {
                                    ?>
                                    <img width="40px" height="50px" src="<?php echo 'uploads/'.$item['image_3']; ?>">
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                                 <div class="col-sm-4">
                                <hr>
                                <label>Image-4</label>
                               <input type="file" name="image4" >
                               <?php if (!empty($item['image_4']))
                                {
                                    ?>
                                    <img width="40px" height="50px" src="<?php echo 'uploads/'.$item['image_4']; ?>">
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <input type="text" name="video_name1" id="video_name1" placeholder="Enter Video Name & upload video" class="form-control" value="<?php if (!empty($item['video_name1'])) {echo $item['video_name1'];}?>" /> <br>
                                <input type="file" name="video1" id="video1" />
                                <?php if (!empty($item['video1']))
                                {
                                    ?>
                                    <video width="160px" height="100px" controls><source src="<?php echo 'uploads/'.$item['video1']; ?>" type="video/mp4">
                                    </video>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <hr>
                                <input type="text" name="video_name2" id="video_name2" placeholder="Enter Video Name & upload video" class="form-control" value="<?php if (!empty($item['video_name2'])) {echo $item['video_name2'];}?>" /><br>
                                <input type="file" name="video2" id="video2" />
                                <?php if (!empty($item['video2']))
                                {
                                    ?>
                                    <video width="160px" height="100px" controls><source src="<?php echo 'uploads/'.$item['video2']; ?>" type="video/mp4">
                                    </video>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Brochure</label>
                                <input type="file" name="brochure" id="brochure" />
                                <?php if (!empty($item['brochure']))
                                {
                                    ?>
                                    <a href="<?php echo 'uploads/'.$item['brochure']; ?>">Click here to View Uploaded brochure</a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Description</label>
                                <textarea class="form-control ckeditor" style="height:120px;" name="description"><?php if (!empty($item['description'])) {echo $item['description'];}?></textarea>
                            </div>
                          </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Details</label>
                                <textarea class="form-control ckeditor"  style="height:120px;" name="details"><?php if (!empty($item['details'])) {echo $item['details'];}?></textarea>
                            </div>
                          </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Warranty</label>
                                <textarea class="form-control" style="height:120px;" name="warranty"><?php if (!empty($item['warranty'])) {echo $item['warranty'];}?></textarea>
                            </div>

                            </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="item.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
</body>

</html>