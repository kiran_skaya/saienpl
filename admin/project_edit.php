<?php
ini_set('display-errors',1);
error_reporting(E_ALL);
include '../connection.php';
$pid = $_GET['pid'];
$editquery="SELECT * FROM projects WHERE project_id='".$pid."' ";
$editqueryresult = $con->query($editquery);
    $projects = $editqueryresult->fetch_assoc();

if (isset($_POST['submit']))
{

    $project_name = $_POST['project_name'];

    $file_name1 = $_FILES['image1']['name'];
    if ($file_name1=="")
    {
        $file_name1=$projects['image_1'];
    }
    else
    {
        $file_name1 = $_FILES['image1']['name'];
        $file_tmp1 =$_FILES['image1']['tmp_name'];
        move_uploaded_file($file_tmp1,"uploads/".$file_name1);
    }

    $file_name2 = $_FILES['image2']['name'];
    if ($file_name2=="")
    {
        $file_name2=$projects['image_2'];
    }
    else
    {
        $file_name2 = $_FILES['image2']['name'];
        $file_tmp2 =$_FILES['image2']['tmp_name'];
        move_uploaded_file($file_tmp2,"uploads/".$file_name2);
    }

    $file_name3 = $_FILES['image3']['name'];
    if ($file_name3=="")
    {
        $file_name3=$projects['image_3'];
    }
    else
    {
        $file_name3 = $_FILES['image3']['name'];
        $file_tmp3 =$_FILES['image3']['tmp_name'];
        move_uploaded_file($file_tmp3,"uploads/".$file_name3);
    }
       
    $file_name4 = $_FILES['image4']['name'];
    if ($file_name4=="")
    {
        $file_name4=$projects['image_4'];
    }
    else
    {
        $file_name4 = $_FILES['image4']['name'];
        $file_tmp4 =$_FILES['image4']['tmp_name'];
        move_uploaded_file($file_tmp4,"uploads/".$file_name4);
    }
    
    $description = $_POST['description'];

    $projectquery = "UPDATE projects SET project_name='".$project_name."', image_1='".$file_name1."', image_2='".$file_name2."', image_3='".$file_name3."', image_4='".$file_name4."', description='".$description."' WHERE project_id='".$pid."' ";
    $projectqueryresult = mysqli_query($con,$projectquery);
    if ($projectqueryresult==1)
    {
        echo '<script>alert("Updated successfully")</script>';
        echo "<script>parent.location='projects.php'</script>";
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit Project</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Edit Project</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                <label>Project Name</label>
                                <input type="text" class="form-control" name="project_name" id="project_name" value="<?php echo $projects['project_name']; ?>">
                                </div>
                            </div>

                         <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-1</label>
                               <input type="file" name="image1" id="image1" ><br>
                               <span><img width="160px" height="100px" src="<?php echo 'uploads/'.$projects['image_1']; ?>"></span> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-2</label>
                               <input type="file" name="image2" id="image2" ><br>
                               <span><img width="160px" height="100px" src="<?php echo 'uploads/'.$projects['image_2']; ?>"></span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-3</label>
                               <input type="file" name="image3" id="image3" ><br>
                               <span><img width="160px" height="100px" src="<?php echo 'uploads/'.$projects['image_3']; ?>"></span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-4</label>
                               <input type="file" name="image4" id="image4" ><br>
                               <span><img width="160px" height="100px" src="<?php echo 'uploads/'.$projects['image_4']; ?>"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Description</label>
                                <textarea class="form-control ckeditor" style="height:120px;" name="description" id="description"><?php echo $projects['description']; ?></textarea>
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="projects.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="submit">Save</button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
</body>

</html>
