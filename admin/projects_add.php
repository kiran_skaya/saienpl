<?php
include '../connection.php';


if (isset($_POST['submit']))
{
    $project_name = $_POST['project_name'];

    $errors= array();
    $file_name1 = $_FILES['image1']['name'];
    $file_tmp1 =$_FILES['image1']['tmp_name'];

    if(empty($errors)==true)
    {
        move_uploaded_file($file_tmp1,"uploads/".$file_name1);
        echo "Success";
    }else
    {
        print_r($errors);
    }

    $file_name2 = $_FILES['image2']['name'];
    $file_tmp2 =$_FILES['image2']['tmp_name'];

    if(empty($errors)==true)
    {
        move_uploaded_file($file_tmp2,"uploads/".$file_name2);
        echo "Success";
    }else
    {
        print_r($errors);
    }

    $file_name3 = $_FILES['image3']['name'];
    $file_tmp3 = $_FILES['image3']['tmp_name'];

    if(empty($errors)==true)
    {
        move_uploaded_file($file_tmp3,"uploads/".$file_name3);
        echo "Success";
    }else
    {
        print_r($errors);
    }

    $file_name4 = $_FILES['image4']['name'];
    $file_tmp4 = $_FILES['image4']['tmp_name'];

    if(empty($errors)==true)
    {
        move_uploaded_file($file_tmp4,"uploads/".$file_name4);
        echo "Success";
    }else
    {
        print_r($errors);
    }

    $description = $_POST['description'];

    $projectquery = "INSERT INTO projects(project_name, image_1, image_2, image_3, image_4, description) VALUES('$project_name','$file_name1','$file_name2','$file_name3','$file_name4','$description') ";
    $projectqueryresult = mysqli_query($con,$projectquery);
    if ($projectqueryresult==1)
    {
        echo "<script>parent.location='projects.php'</script>";
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Project</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>


<script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                project_name:{
                    required:true
                },
                description:"required"
            },
            messages:{
                project_name:"<span>*Enter Project Name</span>",
                description:"<span>*Enter Description</span>"
            },
        })
    })
</script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Add Project</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                <label>Project Name</label>
                                <input type="text" class="form-control" name="project_name" id="project_name">
                                </div>
                            </div>

                         <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-1</label>
                               <input type="file" name="image1" id="image1" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-2</label>
                               <input type="file" name="image2" id="image2">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-3</label>
                               <input type="file" name="image3" id="image3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <hr>
                                <label>Image-4</label>
                               <input type="file" name="image4" id="image4">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Description</label>
                                <textarea class="form-control ckeditor" style="height:120px;" name="description" id="description"></textarea>
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="projects.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="submit">Save</button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

    <!-- <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
</body>

</html>
