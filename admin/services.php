<?php

include '../connection.php';

$viewquery = "Select * from services";
$viewqueryresult = mysqli_query($con,$viewquery);
$service_description = "";
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $service_description =$row['service_desc'];
}
if (isset($_POST['submit']))
{
  $service = $_POST['service'];
  $serviceUpdate = "UPDATE services SET service_desc='".$service."' ";
  $serviceUpdateresult = mysqli_query($con,$serviceUpdate);
  if ($serviceUpdateresult==1)
  {
    echo '<script>alert("Updated successfully")</script>';
    echo "<script>parent.location='services.php'</script>";
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Services</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
  
  <script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   #page_list li
   {
    padding:16px;
    background-color:#f9f9f9;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   #page_list li.ui-state-highlight
   {
    padding:24px;
    background-color:#ffffcc;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
  </style>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Yogesh <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="post" action="" id="form">
                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Services</h3>
                    </div>
                    <div class="form-group">
                    <div class="row">
                            <div class="col-sm-10">
                                <hr>
                                <label>Enter Services</label>
                                <textarea class="form-control ckeditor" style="height:120px;" name="service" id="service"><?php echo $service_description; ?></textarea>
                            </div>
                    </div>

                    </div>
                    <div class="button-block clearfix">
                      <div class="pull-right">
                        <button class="btn btn-error">Cancel</button>
                        <button class="btn btn-success" type="submit" name="submit">Save</button>
                      </div>
                    </div>
                  </form>


                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
  
</body>

</html>