<?php
ini_set('display-errors',1);
error_reporting(E_ALL);
include '../connection.php';
$itemid = $_GET['id'];

$sql = "select * from item where id='$itemid'";
$result        = $con->query($sql);
$itemslist = array();
while ($row = mysqli_fetch_assoc($result)) {
    $itemName = $row['item_name'];
}


$sql = "select * from  item_has_specification where id_item='$itemid'";
//$result = mysqli_query($con,$sql);
$result        = $con->query($sql);
$categorylist = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($categorylist, $row);
}
if($_POST) {
	if($_POST['Save']=='Save') {
		  $label = $_POST['label'];
		  $value = $_POST['value'];

		  $sql = "Insert into  item_has_specification (id_item,label,value) values ('$itemid','$label','$value') ";
		    $con->query($sql);
		} else {

			$id= $_POST['Save'];
			 $sql = "Delete from item_has_specification where id='$id'";
		    $con->query($sql);
		}

    header("location:specification_add.php?id=$itemid");
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Item</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php echo $itemName;?></h3>
                    </div>


                        <div class="form-group">
                            <div class="row">

                              
                           <div class="col-sm-6">
                                <label>Label</label>
                                <input type="text" class="form-control" name="label" id="label" value="">
                            </div>
                             <div class="col-sm-6">
                                <label>Value</label>
                                <input type="text" class="form-control" name="value" id="value" value=''>
                            </div>

                            </div>
                             <div class="button-block clearfix">
		                   <div class="pull-right">
		                    <a href="/admin/item.php" class="btn btn-error">Cancel</a>
		                    <button class="btn btn-success" type="submit" name="Save" value="Save">Save</button>
		                   </div>
		                </div>

                            <div class="row">
                             <table class="table table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th>Label</th>
                                <!-- <th>Status</th> -->
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($categorylist as $category) { ?>

                            <tr>
                                <td><?php echo $category['label']; ?></td>
                               
                                <td><?php echo $category['value']; ?></td>
                                <td><button class="btn btn-success" type="submit" name="Save" value="<?php echo $category['id']?>">Delete</button></td>
                            </tr>
                        <?php }?>


                        </tbody>
                    </table>
		                     </div>
		                </div>
		               
		                </form>
		                    <div class="footer">
		                        <p>&copy; 2018, Allrights reserved</p>
		                    </div>
		                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
</body>

</html>
