<?php
include '../connection.php';
if (isset($_GET['id'])) {
    
    $id          = $_GET['id'];
    $sql         = "select * from sub_category  inner join category on id_category = category_id where sub_category_id = $id";
    $result      = $con->query($sql);
    $subcategory = $result->fetch_assoc();
    

}
$sql              = "select * from category";
$result           = $con->query($sql);
$categorylist = array();
while ($row = $result->fetch_assoc()) {
    array_push($categorylist, $row);
}
if (isset($_POST['update'])) {
    
        $name       = $_POST['name'];
        $id_category  = $_POST['id_category'];

        $sql    = "update sub_category set sub_category_name = '$name',id_category = '$id_category' where sub_category_id = $id";
        $con->query($sql);
        header("location:sub_category.php");
    }
    $gst = "";
if (isset($_POST['save']))
{
    $name       = $_POST['name'];
    $id_category  = $_POST['id_category'];
    $gst = $_POST['gst'];


    $sql    = "insert into sub_category(sub_category_name,id_category) values('$name','$id_category');";
    $result = $con->query($sql);
    echo "<script>parent.location='sub_category.php'</script>";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Sub Category</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <div class="logo">
                    <a href="#">LOGO</a>
                </div>
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($subcategory['sub_category_id'])){echo "Edit";}else{echo "Add";}?> Sub Category</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">

                                <label>Category</label>
                                <select name="id_category" class="form-control">
                                    <option value="">Please Select--</option>
                                    <?php
                                        foreach ($categorylist as $category) {
                                        $category_name = $category['category_name'];
                                        $category_id   = $category['category_id'];
                                        if($subcategory['id_category'] == $category_id){

                                            $selected = "selected";
                                        }
                                        else{
                                            $selected = "";
                                        }
                                        echo "<option value='$category_id' $selected>$category_name</option>";
                                        }
                                        ?>
                                </select>
                            </div>
                              <div class="col-sm-4">

                                <label>Name</label>
                                <input type="text" class="form-control" name="name" value="<?php if (!empty($subcategory['sub_category_name'])) {echo $subcategory['sub_category_name'];}?>">
                            </div>
                            
                            
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="sub_category.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if(!empty($subcategory['sub_category_id'])){echo "update";}else{echo "save";}?>"><?php if(!empty($subcategory['sub_category_id'])){echo "Update";}else{echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
</body>

</html>
