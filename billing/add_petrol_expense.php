<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from petrol_expense where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{
    $date = date("Y-m-d", strtotime($_POST['date']));
    $name = $_POST['id_employee'];
    $customer = $_POST['customer_visited'];
    $places = $_POST['visited_places'];
    $places_km = $_POST['visited_places_km'];
    $per_km = $_POST['per_km'];
    $total = $_POST['total_expense'];

    $sql = "insert into petrol_expense(date, id_employee, customer_visited, visited_places, visited_places_km, per_km, total_expense) values('$date', '$name', '$customer', '$places', '$places_km', '$per_km', '$total') ";
    $con->query($sql) or die(mysqli_error($con));
      

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: petrol_expense.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];
    
    $date = date("Y-m-d", strtotime($_POST['date']));
    $name = $_POST['id_employee'];
    $customer = $_POST['customer_visited'];
    $places = $_POST['visited_places'];
    $places_km = $_POST['visited_places_km'];
    $per_km = $_POST['per_km'];
    $total = $_POST['total_expense'];

  $updatequery = "update petrol_expense set date = '$date', id_employee = '$name', customer_visited='$customer', visited_places='$places', visited_places_km='$places_km', per_km='$per_km', total_expense='$total' where id = $id";
  
    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Updated successfully")</script>';
        header("location: petrol_expense.php");
        
    }
    header("location: petrol_expense.php");
}

$sql = "SELECT id, employee_name, employee_id FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }

$sql = "SELECT id, type_name FROM transaction_type order by type_name asc";
$result = $con->query($sql);
$tTypeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($tTypeList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Petrol Expense</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Petrol Expense</h3>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Employee Name</label>
                                <select name="id_employee" class="form-control selitemIcon">
                                    <option value="">SELECT</option>
                                    <?php
                                    for($i=0; $i<count($employeeList); $i++){?>
                                    <option value="<?php echo $employeeList[$i]['id']; ?>" <?php if($employeeList[$i]['id']==$item['id_employee']){ echo "selected"; } ?>><?php echo $employeeList[$i]['employee_name']."-".$employeeList[$i]['employee_id']; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Date</label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($item['date'])) {echo $item['date'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>Customer Visited</label>
                                <input type="text" class="form-control" name="customer_visited" value="<?php if (!empty($item['customer_visited'])) {echo $item['customer_visited'];}?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Visited Places</label>
                                <input type="text" class="form-control" name="visited_places" value="<?php if (!empty($item['visited_places'])) {echo $item['visited_places'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Visited Places KM</label>
                                <input type="text" class="form-control" name="visited_places_km" id="visited_places_km" value="<?php if (!empty($item['visited_places_km'])) {echo $item['visited_places_km'];}?>" onkeyup="getTot()">
                            </div>
                            <div class="col-sm-4">
                                <label>Per KM</label>
                                <input type="text" class="form-control" name="per_km" id="per_km" value="<?php if (!empty($item['per_km'])) {echo $item['per_km'];}?>" onkeyup="getTot()">
                            </div>
                            <div class="col-sm-4">
                                <label>Total Petrol Expense</label>
                                <input type="text" class="form-control" name="total_expense" id="total_expense" value="<?php if (!empty($item['total_expense'])) {echo $item['total_expense'];}?>">
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="petrol_expense.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_bank:"required",
                id_transaction_type:"required",
                amount : "required",
                date : "required",
                hsn_code : "required"
            },
            messages:{
                id_bank:"<span>*Select Bank Name</span>",
                id_transaction_type:"<span>*Select Transaction Type</span>",
                amount:"<span>*Enter Amount</span>",
                date:"<span>*Select Date</span>",
                hsn_code:"<span>*Enter HSN Code</span>"
            },
        })
    })
</script>
<script>
    function getTot(){
        var totKM= $("#visited_places_km").val();
        var perKM= $("#per_km").val();
        var tot = totKM * perKM;
        var value = tot.toFixed(2);
        $('#total_expense').val(value);
    }
    
    var totKM= $("#visited_places_km").val();
        var perKM= $("#per_km").val();
        var tot = totKM * perKM;
        var value = tot.toFixed(2);
        $('#total_expense').val(value);
    
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>