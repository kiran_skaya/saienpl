<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from sales_engineer_report where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}
    
    $viewquery = "Select * from visited_customer where id_sales_report='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['customer_name'] = $row['customer_name'];
      $career[$i]['address'] = $row['address'];
      $career[$i]['contact_person'] = $row['contact_person'];
      $career[$i]['mobile'] = $row['mobile'];
      $career[$i]['email'] = $row['email'];
      $career[$i]['visit_card'] = $row['visit_card'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }

if ($_POST) {
    $ename = $_POST['id_employee'];
    $date = date("Y-m-d", strtotime($_POST['date']));
    
    $sql ="INSERT INTO sales_engineer_report(id_employee, date)values('$ename', '$date')";
    
    $con->query($sql);
    header("location: sales_engineer_report.php");
}

if (isset($_POST['update']))
{
    
    $id  = $item['id'];
    

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="sales_engineer_report.php"</script>';
}

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
$sql = "SELECT id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }
 
 $sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_sales_report_has_items");
mysqli_query($con, "DELETE FROM temp_visited_customer");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sales Engineer Report</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_customer_from_sales.php?id="+id+"&sid="+sid;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Sales Engineer Report</h3>
                        <?php if(!empty($item['id'])){ ?>
                        <a href="sales_engineer_report.php" class="btn btn-primary">Back</a>
                        <?php } ?>
                    </div>
                    
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sales Engineer<span class="error">*</span></label>
                            <select name="id_employee" class="form-control selitemIcon" id="id_employee" style="width: 300px;" <?php if (!empty($item['id'])) {echo "disabled";}?>>
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($employeeList); $i++) {  ?>
                                <option value="<?php echo $employeeList[$i]['id']; ?>" <?php if($employeeList[$i]['id'] == $item['id_employee'])  { echo "selected=selected"; }
                                    ?>><?php echo $employeeList[$i]['employee_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="date" id="date" maxlength="50" autocomplete="off" value="<?php echo $item['date']; ?>" <?php if (!empty($item['id'])) {echo "readonly";}?>>
                            </div>
                    </div>
                    <?php if (!empty($item['id'])) { ?>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label> </label><br><br>
                        <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Customer Details</button>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php if (!empty($item['id'])) { ?>

                <div class="page-title clearfix">
                    <h3>Customer Details</h3>
                </div>
                <?php } ?>
                <div id="addfiles"></div>
                <div style="<?php if($id==''){echo "display:none";}?>">
                                <table id="example" class="table table-striped">
                                   <thead>
                            		<tr>
                                		<th>Customer Name</th>
                                        <th>Address</th>
                                        <th>More Contacts</th>
                                        <th>View Items</th>
                                        <th>Add Items</th>
                                        <th>Send Mail</th>
                                        <th>Make Quotation</th>
                                        <th>Action</th>
                            		</tr>
                            		</thead>
                            		<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){?>
                            		    <tr>
                            		        <td><?php echo $career[$i]['customer_name']; ?></td>
                            		        <td><?php echo $career[$i]['address']; ?></td>
                                            <td style="text-align: center;"><a href="#" name="modal2" data-toggle="modal" data-target="#myModal2" onclick="pass(this)" id="<?php echo $career[$i]['id']; ?>">ADD</a> | <a href="customer_contacts_list.php?cid=<?php echo $career[$i]['id']; ?>&sid=<?php echo $id; ?>">VIEW </a></td>
                                            <td style="text-align: center;"><a href="customer_items_list.php?cid=<?php echo $career[$i]['id']; ?>&sid=<?php echo $id; ?>">VIEW ITEMS</a></td>
                                            <td style="text-align: center;"><a href="#" name="modal1" data-toggle="modal" data-target="#myModal1" onclick="pass(this)" id="<?php echo $career[$i]['id']; ?>">ADD ITEM</a></td>
                                            <td style="text-align: center;"><a href="send_mail_to_customer.php?cid=<?php echo $career[$i]['id']; ?>&sid=<?php echo $id; ?>">SEND MAIL </a></a></td>
                                            <td><a href="">Quotation</a></td>
                                            <td style="text-align: center;"><a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                            		    </tr>
                            		    <?php } ?>
                            		</tbody>
                        		</table>
                            </div>


                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                       <?php if (empty($item['id'])) { ?>
                    <button class="btn btn-error"><a href="sales_engineer_report.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Add";} else {echo "Save";}?></button>
                    <?php } ?>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>
<input type="hidden" id="custId" name="custId">
            </div>
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Customer Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Customer Name</label>
                    <input type="text" class="form-control" name="customer" id="customer" autocomplete="off">
                </div>
                <div class="col-sm-6">
                    <label>Address</label>
                    <input type="text" class="form-control" name="address" id="address" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="addFile" id="addFile" data-dismiss="modal" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                </div>
                <div class="col-sm-6">
                    <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_subcategory'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" ><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getDesign()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" ><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                </div>
                <div class="col-sm-6">
                    <label>Remarks<span class="error"> *</span></label>
                    <input type="text" class="form-control" name="remarks" id="remarks" maxlength="250" autocomplete="off" >
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Contacts</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Contact Name<span class="error"> *</span></label>
                    <input type="text" class="form-control" name="contactname" id="contactname" maxlength="50" autocomplete="off" >
                </div>
                <div class="col-sm-6">
                    <label>Contact Number<span class="error"> *</span></label>
                    <input type="text" class="form-control" name="contactnum" id="contactnum" maxlength="10" autocomplete="off" >
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Contact Email<span class="error"> *</span></label>
                    <input type="text" class="form-control" name="contactemail" id="contactemail" maxlength="50" autocomplete="off" >
                </div>
                <div class="col-sm-6">
                    <label>Attach visit Card</label>
                    <input type="file" class="form-control" name="file" id="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Remarks</label>
                    <input type="text" class="form-control" name="remark" id="remark" autocomplete="off">
                </div>
            </div>

            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="addContact" id="addContact" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            brand_name : "required",
            address:"required",
            supplier_code:"required",
            balance : "required",
            scope : "required",
            contact_person : "required",
            btype : "required",
            email : "required",
            gst : "required",
            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            
            brand_name :"<span>Enter Brand name</span>",
            address:"<span>Enter Address</span>",
            supplier_code:"<span>Enter supplier code</span>",
            balance:"<span>Enter balance in rupees</span>",
            email:"<span>Enter Valid Email Id</span>",
            scope : "<span>enter scope of supply</span>",
            contact_person : "<span>enter name of contact person</span>",
            btype : "<span>Select business type</span>",
            gst : "<span>Enter GST Number</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $( "#invoicedate" ).datepicker();
  } );
  </script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }

    function sendMail(cid){
      var id = '<?php echo $id; ?>';
      console.log(id);
      console.log(cid);

      $.ajax({url: "send_mail_to_customer.php?id="+id+"&cid="+cid, success: function(result){
        $("#mailResult").html(result);
      }
    });
    }
</script>
<script type="text/javascript">
      $(document).ready(function (e) {
          $('#addFile').on('click', function () {
              var sid = '<?php echo $id; ?>';
              var customer = $("#customer").val();
              if(customer == ''){
                alert('ENTER CUSTOMER NAME');
                return false;
              }
              var address = $("#address").val();
              if(address == ''){
                alert('ENTER ADDRESS');
                return false;
              }

              $.ajax({
                  url: 'add_visited_customer.php', // point to server-side PHP script 
                  data:{
                      'sid': sid,
                      'customer': customer,
                      'address': address,
                    },
                  success: function (response) {
                      $('#addfiles').html(response); // display success response from the PHP script
                      location.reload();
                  },
              });
          });
      });
    </script>
    <script type="text/javascript">
    
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#custId").val(strid);
      
      
      var id = $("#store").val();
          console.log(id);

          $.ajax({url: "get_price.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
    }
    
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category = $("#id_category").val();
    if(id_category == ""){
        alert("Select Category Name");
        return false;
    }
    var id_subcategory = $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert("Select Sub Category Name");
        return false;
    }
    var id_item = $("#id_item").val();
    if(id_item == ""){
        alert("Select Item Name");
        return false;
    }
      var cid = $("#custId").val();

    var remark = $("#remarks").val();
    if(remark == ""){
        alert("Enter Remark");
        return false;
    }
    
      $.ajax({

        url: 'add_item_to_sales_report.php',
        data:{

          'sid': sid,
          'cid': cid,
          'id_category': id_category,
          'id_subcategory': id_subcategory,
           'id_item': id_item,
            'remark': remark,
        },
        success: function(result){
        $("#previous").html(result);
        location.reload();
      }
        });
      });
    </script>
    <script type="text/javascript">
    
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#custId").val(strid);
    }
    
    $("#addContact").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var cid = $("#custId").val();
    var cname = $("#contactname").val();
    if(cname == ""){
        alert("Enter Contact Name");
        return false;
    }
    var cnum = $("#contactnum").val();
    if(cnum == ""){
        alert("Enter Contact Number");
        return false;
    }
    var cmail = $("#contactemail").val();
    if(cmail == ""){
        alert("Enter Contact Email");
        return false;
    }
    var remark = $("#remark").val();
    if(remark == ""){
        alert("Enter Remark");
        return false;
    }
    var file_data = $('#file').prop('files')[0];
      if(file_data == undefined){
        alert('UPLOAD FILE');
        return false;
      }
      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('sid', sid);
      form_data.append('cid', cid);
      form_data.append('cname', cname);
      form_data.append('cnum', cnum);
      form_data.append('cmail', cmail);
      form_data.append('remark', remark);
    
      $.ajax({

        url: 'add_contacts_visited_customer.php',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
        success: function(result){
        $("#previous").html(result);
        location.reload();
      }
        });
      });
    </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
</html>