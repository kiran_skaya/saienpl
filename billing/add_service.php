<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from service where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $viewquery = "Select * from service_has_images where id_service='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['image'] = $row['image'];
      $i++;
    }
}

if (isset($_POST['save']))
{

    $id_customer = $_POST['id_customer'];
    $id_employee = $_POST['id_employee'];
    $id_quotation = $_POST['id_quotation'];
    $materail_sent = $_POST['materail_sent'];
    $courier_details = $_POST['courier_details'];
    $sql      = "select * from service";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.03d", $resnum);
    $servNo = date('m-d')."-".$ref;

    $date = date("Y-m-d", strtotime($_POST['date']));
    $remarks = $_POST['remarks'];
    $issue= $_POST['issue'];
    $solution= $_POST['solution'];
    $status= $_POST['status'];
    if($status == 'Completed'){
        $approved_date = date('Y-m-d');
    }
    $approved= $_POST['approved'];

    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);

    $sql = "insert into service(service_no, date, id_customer, id_employee, id_quotation, materail_sent, courier_details, remarks, attachment, issue, solution, status, approved_by, approved_date) values('$servNo', '$date', '$id_customer', '$id_employee', '$id_quotation', '$materail_sent', '$courier_details', '$remarks', '$file', '$issue', '$solution', '$status', '$approved', '$approved_date') ";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];
    
    $tempsql = mysqli_query($con,"SELECT * FROM temp_service_has_images WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['image'] = $row['image'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['image'];

   $insertbill = "INSERT INTO service_has_images(id_service, image) VALUES ('$last_id', '$cname')";
    $result = mysqli_query($con,$insertbill);
    }

    header("location: service.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];

    $id_customer = $_POST['id_customer'];
    $id_employee = $_POST['id_employee'];
    $id_quotation = $_POST['id_quotation'];
    $materail_sent = $_POST['materail_sent'];
    $courier_details = $_POST['courier_details'];

    $date = date("Y-m-d", strtotime($_POST['date']));
    $remarks = $_POST['remarks'];

    $file = $_FILES['attachment']['name'];
    if($file == ""){
        $file = $item['attachment'];
    }
    else{
        $file = $_FILES['attachment']['name'];
     $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);   
    }
    $issue= $_POST['issue'];
    $solution= $_POST['solution'];
    $status= $_POST['status'];
    if($status == 'Completed'){
        $approved_date = date('Y-m-d');
    }
    $approved= $_POST['approved'];

  $updatequery = "update service set issue = '$issue', date = '$date', id_customer='$id_customer', id_employee='$id_employee', id_quotation='$id_quotation', materail_sent= '$materail_sent', courier_details='$courier_details', remarks='$remarks', attachment='$file', solution='$solution', status='$status', approved_by='$approved', approved_date='$approved_date' where id = $id";
  
    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Approved successfully")</script>';
        header("location: service.php");
    }
    header("location: service.php");
}

mysqli_query($con, "DELETE FROM temp_service_has_images");

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, employee_name, employee_id FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }
  
  $sql = "SELECT id, qtn_no FROM quotation";
$result = $con->query($sql);
$quotationList = array();
while ($row = $result->fetch_assoc()) {
    array_push($quotationList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "";} else {echo "Add";}?> Service</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Approve";} else {echo "Add";}?> Service</h3>
                    </div>


                        <div class="form-group">
                                <?php if (!empty($item['service_no'])) {?>
                                <div class="row">
                                <div class="col-sm-4">
                                    <h4>Service No : <?php echo $item['service_no']; ?></h4>
                                </div>
                                </div><br>
                            <?php } ?>
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Customer Name</label>
                                <select name="id_customer" class="form-control selitemIcon" id="id_customer">
                                    <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($companyList); $i++)
                                    { 
                                        $value=$companyList[$i]['id'];
                                        $label=$companyList[$i]['customer_name'];
                                    ?>
                                        <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_customer'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                            </option>;
                                    <?php
                                    }
                                    ?>
                                </select>
                              </div>
                            <div class="col-sm-4">
                                <label>Date</label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($item['date'])) {echo $item['date'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>Service Attended Engineer</label>
                                <select name="id_employee" class="form-control selitemIcon" id="id_employee">
                                    <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($employeeList); $i++)
                                    { 
                                        $value=$employeeList[$i]['id'];
                                        $label=$employeeList[$i]['employee_name']."-".$employeeList[$i]['employee_id'];
                                    ?>
                                        <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_employee'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                            </option>;
                                    <?php
                                    }
                                    ?>
                                </select>
                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Material Sent</label>
                                <input type="text" class="form-control" name="materail_sent" value="<?php if (!empty($item['materail_sent'])) {echo $item['materail_sent'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Quotation </label>
                                <select name="id_quotation" class="form-control selitemIcon" id="id_quotation">
                                    <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($quotationList); $i++)
                                    { 
                                        $value=$quotationList[$i]['id'];
                                        $label=$quotationList[$i]['qtn_no'];
                                    ?>
                                        <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_quotation'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                            </option>;
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Courier Details </label>
                                <textarea class="form-control" name="courier_details" id="courier_details" cols="3"><?php if (!empty($item['courier_details'])) {echo $item['courier_details'];}?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Remarks</label>
                                <input type="text" class="form-control" name="remarks" value="<?php if (!empty($item['remarks'])) {echo $item['remarks'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Service Attachment</label>
                                <input type="file" class="form-control" name="attachment">
                            </div>
                            <div class="col-sm-4"></div>
                                <?php if (!empty($item['attachment'])) {?>
                                <img src="<?php echo 'uploads/'.$item['attachment']; ?>" width="100px"  height="80px">
                                <?php } else {} ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <label>Service Issue </label>
                                <textarea class="form-control" name="issue" id="issue" rows="4" cols="10"><?php if (!empty($item['issue'])) {echo $item['issue'];}?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <label>Solution Given</label>
                                <textarea class="form-control" name="solution" id="solution" rows="4" cols="10"><?php if (!empty($item['solution'])) {echo $item['solution'];}?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Approve</label>
                                <input type="checkbox" name="status" id="status" value="Completed" <?php if($item['status']=='Completed') {echo "checked";}?>>
                            </div>
                            <div class="col-sm-4">
                                <label>Approved By</label>
                                <input type="text" class="form-control" name="approved" id="approved" value="<?php if (!empty($item['approved_by'])) {echo $item['approved_by'];}?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label> </label>    <br>
                                    <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add More Attachments</button>
                            </div>
                        </div>
                        <br>
                        <div id="addedfiles"></div>
                            
                            <div style="<?php if($id==''){echo "display:none";}?>">
                                <table class='table'>
                                   <thead>
                            		<tr>
                                		<th>Attachment List</th>
                            		</tr>
                            		</thead>
                            		<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){?>
                            		    <tr>
                            		        <td><a href='uploads/<?php echo $career[$i]['image']; ?>'><?php echo $career[$i]['image']; ?></a></td>
                            		    </tr>
                            		    <?php } ?>
                            		</tbody>
                        		</table>
                            </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="service.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Attachments</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Upload Attachment</label>
                    <input type="file" class="form-control" name="file" id="file" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
  
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_company:"required",
                id_employee:"required",
                item_name : "required",
                item_code : "required",
                hsn_code : "required"
            },
            messages:{
                id_company:"<span>*Select Company Name</span>",
                id_employee:"<span>*Select Employee Name</span>",
                item_name:"<span>*Enter Item Name</span>",
                item_code:"<span>*Enter Item code</span>",
                hsn_code:"<span>*Enter HSN Code</span>"
            },
        })
    })
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

<script type="text/javascript">
      $(document).ready(function (e) {
          $('#add').on('click', function () {
              var sid = '<?php echo $sid; ?>';
              var file_data = $('#file').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD FILE');
                return false;
              }
              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('sid', sid);

              $.ajax({
                  url: 'add_services_images.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addedfiles').html(response); // display success response from the PHP script
                      $('#file').val('');
                  },
              });
          });
      });
    </script>

</html>