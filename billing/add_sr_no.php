<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from sr_no where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{
     $categoryId = $_POST['id_category'];
     $subCategoryId = $_POST['id_subcategory'];
     $itemId = $_POST['id_item'];
	$date = date("Y-m-d", strtotime($_POST['date']));
	$customerId = $_POST['id_customer'];
	$ponum = $_POST['ponum'];
	$podate = date("Y-m-d", strtotime($_POST['podate']));
	$remarks = $_POST['remarks'];
	
	$sql      = "select * from sr_no";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $curdate = date("d-m"); 
    $ref= sprintf("%'.03d", $resnum);
    $srNo = $ref."/".$curdate;

   
    $sql="INSERT INTO sr_no(id_category, id_subcategory, id_item, sr_num, date, id_customer, po_num, po_date, remark) VALUES('$categoryId', '$subCategoryId', '$itemId', '$srNo', '$date', '$customerId', '$ponum', '$podate', '$remarks')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];


    header("location: srNoList.php");
}

if (isset($_POST['update']))
{
     $categoryId = $_POST['id_category'];
     $subCategoryId = $_POST['id_subcategory'];
     $itemId = $_POST['id_item'];
	$date = date("Y-m-d", strtotime($_POST['date']));
	$customerId = $_POST['id_customer'];
	$ponum = $_POST['ponum'];
	$podate = date("Y-m-d", strtotime($_POST['podate']));
	$remarks = $_POST['remarks'];
    
    $id  = $item['id'];
    $updatequery = "UPDATE sr_no SET id_category='$categoryId', id_subcategory='$subCategoryId', id_item='$itemId', date='$date', id_customer='$customerId', po_num='$ponum',`po_date`='$podate', remark='$remarks' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="srNoList.php"</script>';
}

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }
 

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sr. no</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> SR. NO</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_subcategory'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_subcategory'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 300px;" onchange="getDesign()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    </div>
                    
                <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="date" id="date" maxlength="50" autocomplete="off" value="<?php echo $item['date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Customer Name<span class="error">*</span></label>
                            <select name="id_customer" class="form-control selitemIcon" id="id_customer" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($customerList); $i++) {  ?>
                                <option value="<?php echo $customerList[$i]['id']; ?>" <?php if($customerList[$i]['id'] == $item['id_customer'])  { echo "selected=selected"; }
                                    ?>><?php echo $customerList[$i]['customer_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>PO. NO<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="ponum" id="ponum" maxlength="50" autocomplete="off" value="<?php echo $item['po_num']; ?>">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>PO. Date<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="podate" id="podate" maxlength="50" autocomplete="off" value="<?php echo $item['po_date']; ?>">
                            </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="remarks" id="remarks" maxlength="50" autocomplete="off" value="<?php echo $item['remark']; ?>">
                            </div>
                    </div>
                </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="srNoList.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            brand_name : "required",
            address:"required",
            supplier_code:"required",
            balance : "required",
            scope : "required",
            contact_person : "required",
            btype : "required",
            email : "required",
            gst : "required",
            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            
            brand_name :"<span>Enter Brand name</span>",
            address:"<span>Enter Address</span>",
            supplier_code:"<span>Enter supplier code</span>",
            balance:"<span>Enter balance in rupees</span>",
            email:"<span>Enter Valid Email Id</span>",
            scope : "<span>enter scope of supply</span>",
            contact_person : "<span>enter name of contact person</span>",
            btype : "<span>Select business type</span>",
            gst : "<span>Enter GST Number</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $( "#podate" ).datepicker();
  } );
  </script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>