<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from sub_store where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$viewquery = "SELECT a.*, b.name FROM temp_sub_store_has_items as a INNER JOIN item as b ON a.id_item=b.id WHERE a.id_session ='$sessionId' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['name'] = $row['name'];
      $career[$i]['rack_no'] = $row['rack_no'];
      $career[$i]['self_no'] = $row['self_no'];
      $career[$i]['unit_price'] = $row['unit_price'];
      $career[$i]['quantity'] = $row['quantity'];
    }

if (isset($_POST['save']))
{

    $main =$_POST['main'];
    $name =strtoupper($_POST['name']);
    $person_name = $_POST['person_name'];
    $contact_no = $_POST['contact_no'];
    $racks = $_POST['total_racks'];
    $selfs = $_POST['selfs_per_rack'];
    $id_category = $_POST['id_category'];
    $id_subcategory = $_POST['id_subcategory'];
    $id_item = $_POST['id_item'];
    $rackAlloted = $_POST['rack_alloted_for_item'];
    $selfAlloted = $_POST['self_alloted_for_item'];
    $no_items_self = $_POST['no_items_self'];
   
    $sql="INSERT INTO sub_store(id_main_store, name, person_name, contact_no, total_racks, selfs_per_rack) VALUES('$main', '$name', '$person_name','$contact_no', '$racks', '$selfs')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: sub_store.php");
}

if (isset($_POST['update']))
{

    $main =$_POST['main'];
    $name =strtoupper($_POST['name']);
    $person_name = $_POST['person_name'];
    $contact_no = $_POST['contact_no'];
    $racks = $_POST['total_racks'];
    $selfs = $_POST['selfs_per_rack'];
    $id_category = $_POST['id_category'];
    $id_subcategory = $_POST['id_subcategory'];
    $id_item = $_POST['id_item'];
    $rackAlloted = $_POST['rack_alloted_for_item'];
    $selfAlloted = $_POST['self_alloted_for_item'];
    $no_items_self = $_POST['no_items_self'];
    
    $id  = $item['id'];
    $updatequery = "update sub_store set id_main_store='$main', name = '$name', person_name='$person_name', contact_no='$contact_no', total_racks='$racks', selfs_per_rack='$selfs' where id = $id";
    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="sub_store.php"</script>';
}

$sql = "SELECT id, name FROM main_store";
$result = $con->query($sql);
$mainstoreList = array();
while ($row = $result->fetch_assoc()) {
    array_push($mainstoreList, $row);
  }

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_sub_store_has_items");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sub Store</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    #name{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>

                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sub Store</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Main Store<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="main" id="main">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($mainstoreList); $i++){ ?>
                                    <option value="<?php echo $mainstoreList[$i]['id']; ?>" <?php if($mainstoreList[$i]['id']==$item['id_main_store']){echo "selected=selected";} ?>><?php echo strtoupper($mainstoreList[$i]['name']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sub Store Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" maxlength="50" autocomplete="off" value="<?php echo $item['name']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sub Store handling Person Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="person_name" id="person_name" maxlength="50" autocomplete="off" value="<?php echo $item['person_name']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Contact Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="contact_no" id="contact_no" autocomplete="off" value="<?php echo $item['contact_no']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Racks allotted for sub store<span class="error">*</span></label>
                                <input type="text" class="form-control" name="total_racks" id="total_racks" maxlength="50" autocomplete="off" value="<?php echo $item['total_racks']; ?>" onkeyup="getRackSelect()">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Number of Selfs per Rack<span class="error">*</span></label>
                                <input type="text" class="form-control" name="selfs_per_rack" id="selfs_per_rack" maxlength="50" autocomplete="off" value="<?php echo $item['selfs_per_rack']; ?>" onkeyup="getSelfSelect()">
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">-->
                    <!--    <div class="col-sm-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Items To Store</button>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div id="previous"></div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="sub_store.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-10">
                    <label>Item Name</label>
                    <select class="form-control selitem Icon" name="item" id="item">
                        <option value="">select</option>
                        <?php for($i=0; $i<count($itemList); $i++){?>
                        <option value="<?php echo $itemList[$i]['id'] ?>"><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Rack Number</label>
                    <input type="text" class="form-control" name="rnum" id="rnum" autocomplete="off" maxlength="10">
                </div>
                <div class="col-sm-6">
                    <label>Self Number</label>
                    <input type="text" class="form-control" name="snum" id="snum" autocomplete="off">
                </div>
                <div class="col-sm-6">
                    <label>Unit Price</label>
                    <input type="text" class="form-control" name="uprice" id="uprice" autocomplete="off">
                </div>
                <div class="col-sm-6">
                    <label>Quantity</label>
                    <input type="text" class="form-control" name="qnty" id="qnty" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            name : "required",
            main:"required",
            person_name:"required",
            total_racks : "required",
            no_items_self : "required",
            selfs_per_rack : "required",
            id_category : "required",
            id_subcategory : "required",
            id_item : "required",
            rack_alloted_for_item : "required",
            self_alloted_for_item : "required",

            contact_no: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            name : "<span>Enter Sub store name</span>",
            main:"<span>Select main store</span>",
            person_name:"<span>Enter Person Name</span>",
            total_racks:"<span>Enter Total Number of Racks</span>",
            no_items_self:"<span>Enter Number of Items</span>",
            selfs_per_rack : "<span>Enter Number of Selfs</span>",
            id_category : "<span>Select Category</span>",
            id_subcategory : "<span>Select Sub Category</span>",
            id_item : "<span>Select Item </span>",
            rack_alloted_for_item : "<span>Select Rack </span>",
            self_alloted_for_item : "<span>Select Self </span>",
           contact_no:
           {
            required:"<span>Enter Contact Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#item").html(result);
          }
        });
        }
        
        function getRackSelect(){
          var value = $("#total_racks").val();
          console.log(value);

          $.ajax({url: "get_dropdown.php?value="+value, success: function(result){
            $("#rack_for_item").html(result);
          }
        });
        }
        
        function getSelfSelect(){
          var value = $("#selfs_per_rack").val();
          console.log(value);

          $.ajax({url: "get_dropdown2.php?value="+value, success: function(result){
            $("#self_for_item").html(result);
          }
        });
        }
        
        // var value = $("#total_racks").val();
        //   console.log(value);

        //   $.ajax({url: "get_dropdown.php?value="+value, success: function(result){
        //     $("#rack_for_item").html(result);
        //   }
        // });
        
        // var value = $("#selfs_per_rack").val();
        //   console.log(value);

        //   $.ajax({url: "get_dropdown.php?value="+value, success: function(result){
        //     $("#self_for_item").html(result);
        //   }
        // });
    </script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var item = $("#item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var rnum = $("#rnum").val();
    if(rnum == ""){
        alert('Enter Rack Number');
        return false;
    }
    var snum = $("#snum").val();
    if(snum == ""){
        alert('Enter Self Number');
        return false;
    }
    var uprice = $("#uprice").val();
    if(uprice == ""){
        alert('Enter Unit Price');
        return false;
    }
    var qnty = $("#qnty").val();
    if(qnty == ""){
        alert('Enter Item Quantity');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_store.php',
        data:{

          'sid': sid,
          'item': item,
           'rnum': rnum,
            'snum': snum,
            'uprice': uprice,
            'qnty': qnty,
        },
        success: function(result){
        $("#previous").html(result);
        $('#item').val('');
        $('#rnum').val('');
        $('#snum').val('');
        $('#uprice').val('');
        $('#qnty').val('');
      }
        });
      });
    </script>
    <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
          var sid = '<?php echo $sid; ?>';
          var option = 'delete';
      $.ajax({

        url: 'add_items_to_store.php',
        data:{

          'sid': sid,
          'option': option,
           'id': id,
        },
        success: function(result){
        $("#previous").html(result);
      }
        });
      }
    }
  </script>

</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>