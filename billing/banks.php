<?php

include 'connection.php';

$viewquery = "Select * from banks";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['bank_name'] = strtoupper($row['bank_name']);
  $career[$i]['account_name'] = $row['account_name'];
  $career[$i]['account_type'] = $row['account_type'];
  $career[$i]['account_no'] = $row['account_no'];
  $career[$i]['ifsc_code'] = $row['ifsc_code'];
  $career[$i]['branch_name'] = $row['branch_name'];
  $career[$i]['branch_address'] = $row['branch_address'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banks</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
    
  <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?user_id='+id;
      }
    }
  </script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Banks</h3>
                        <a href="add_banks.php" class="btn btn-primary">+ Add Bank</a>
                    </div>

                      <table class="table table-striped" id="example">
                          <thead>
                        <tr>
                            <th>Sl. No</th>
                          <th>Bank Name</th>
                          <th>Account Type</th>
                          <th>Account Name</th>
                          <th>Account Number</th>
                          <th>IFSC Code</th>
                          <th>Branch Name</th>
                          <th>Branch Address</th>
                          <th>Edit&nbsp; | &nbsp;Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $ifsc = $career[$i]['ifsc_code'];
                            $accType = $career[$i]['account_type'];
                            $accName = $career[$i]['account_name'];
                            $accNo = $career[$i]['account_no'];
                            $name = $career[$i]['bank_name'];
                            $phone = $career[$i]['branch_name'];
                            $address = $career[$i]['branch_address'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $accType; ?></td>
                          <td><?php echo $accName; ?></td>
                          <td><?php echo $accNo; ?></td>
                          <td><?php echo $ifsc; ?></td>
                          <td><?php echo $phone; ?></td>
                          <td><?php echo $address; ?></td>
                          <td><a href="add_banks.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>
       <script>
            $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
  
</body>

</html>