<?php
include 'connection.php';
session_start();
$sid = session_id();
if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from billing where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{

    $name = $_POST['name'];
    $billing_number = $_POST['billing_number'];
    $date = $_POST['date'];
    $amount = $_POST['amount'];
    $type = $_POST['type'];

   
    $sql = "insert into billing(id_vendor, billing_number, date, amount, type) values('$name', '$billing_number', '$date', '$amount', '$type') ";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_billing_has_item WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_item'] = $row['id_item'];
    $fetch[$i]['quantity'] = $row['quantity'];
    $fetch[$i]['block_number'] = $row['block_number'];
    $fetch[$i]['rack_number'] = $row['rack_number'];
    $fetch[$i]['shelf_number'] = $row['shelf_number'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $item = $fetch[$i]['id_item'];
    $qty = $fetch[$i]['quantity'];
    $bno = $fetch[$i]['block_number'];
    $rno = $fetch[$i]['rack_number'];
    $sno = $fetch[$i]['shelf_number'];

   $insertbill = "INSERT INTO billing_has_item(id_billing, id_item, quantity, block_number, rack_number, shelf_number) VALUES ('$last_id', '$item', '$qty', '$bno', '$rno', '$sno')";
    $result = mysqli_query($con,$insertbill);
    }
    

    header("location: billing.php");
}

if (isset($_POST['update']))
{

    $name = $_POST['name'];
    $billing_number = $_POST['billing_number'];
    $date = $_POST['date'];
    $amount = $_POST['amount'];
    $type = $_POST['type'];
    
    $id  = $item['id'];
   $updatequery = "update billing set id_vendor = '$name', billing_number = '$billing_number', date='$date', amount='$amount', type='$type' where id = $id";
    $res=$con->query($updatequery);
    if ($res==1)
    {
        echo '<script>alert("Updated successfully")</script>';
        echo "<script>parent.location='billing.php</script>";
    }
}

$sql = "SELECT id, name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }

$sql = "SELECT id, name FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_billing_has_item");
  
  

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Billing</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                name:"required",
                billing_number:"required",
                date:"required",
                amount:"required",
                type:"required"
            },
            messages:{
                name:"<span>Select vendor name</span>",
                billing_number:"<span>Enter Billing Number</span>",
                date:"<span>Enter Date</span>",
                amount:"<span>Enter Amount</span>",
                type:"<span>Select Billing type</span>"
            },
        })
    })
</script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color: red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown"> -->
                          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a> -->
                          <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a href="#">Action</a></li> -->
                            <!-- <li><a href="#">Another action</a></li> -->
                            <!-- <li><a href="#">Something else here</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="../index.php">Logout</a></li>
                          <!-- </ul> -->
                        <!-- </li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Bill</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Vendor Name <span class="text-danger"> *</span></label>
                                <select name="name" class="form-control" id="name">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($vendorList); $i++) {?>
                                    <option value="<?php echo $vendorList[$i]['id']; ?>" <?php if($vendorList[$i]['id']==item['id_vendor']){ echo "selected=selected"; } ?>>
                                        <?php echo $vendorList[$i]['name']; ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Billing Number <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" name="billing_number" value="<?php if (!empty($item['billing_number'])) {echo $item['billing_number'];}?>">
                            </div>

                              <div class="col-sm-4">
                                <label>Date <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($item['date'])) {echo $item['date'];}else{ echo date("Y-m-d"); } ?>" >
                            </div>
                            </div>
                        <div class="row">
                           <div class="col-sm-4">
                                <label>Amount <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" name="amount" value="<?php if (!empty($item['amount'])) {echo $item['amount'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Type <span class="text-danger"> *</span></label><br>
                                <input type="radio" name="type" value="1" <?php if($item['type']=='1'){echo "checked"; } ?>> Direct To Customer <br>
                                <input type="radio" name="type" value="2" <?php if($item['type']=='2'){echo "checked"; } ?>> In-House
                            </div>

                        </div>
                        <hr><h3 class="text-center">Billing Has Item</h3>
                            <br>
                        <div class="row">
                                <div class="col-sm-4">
                                <label>Item Name</label>
                                <select name="item" class="form-control" id="item">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($itemList); $i++) {?>
                                    <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id']==item['id_item']){ echo "selected=selected"; } ?>>
                                        <?php echo $itemList[$i]['name']; ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Quantity</label>
                                <input type="text" class="form-control" name="quantity" id="quantity">
                            </div>

                              <div class="col-sm-3">
                                <label>Block Number</label>
                                <input type="text" class="form-control" name="block_number" id="block_number" >
                            </div>

                           <div class="col-sm-3">
                                <label>Rack Number</label>
                                <input type="text" class="form-control" name="rack_number" id="rack_number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Shelf Number</label><br>
                                <input type="text" name="shelf_number" class="form-control" id="shelf_number">
                            </div>
                            
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" style="padding:25px;">
                                <label> <br></label>
                                <button type="button" name="add" id="add" class="btn btn-danger">Add</button>
                            </div>

                        </div>
                           
                        </div>
                        
                        <div id="previous"></div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="billing.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date").datepicker();
  } );
  </script>
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript">
    $("#add").on('click',function(){
    var item = $('#item').val();
    var sid = '<?php echo $sid; ?>';
    var quantity = $("#quantity").val();
    var block_number = $("#block_number").val();
    var rack_number = $("#rack_number").val();
    var shelf_number = $("#shelf_number").val();
    
      $.ajax({
        // type: 'POST',
        url: 'add_billing_data.php',
        data:{
          'item': item,
          'sid': sid,
          'quantity': quantity,
           'block_number': block_number,
            'rack_number': rack_number,
             'shelf_number': shelf_number,
        },
        success: function(result){
        $("#previous").html(result);
        $('#item').val(' ');
        $('#quantity').val(' ');
        $('#block_number').val(' ');
        $('#rack_number').val(' ');
        $('#shelf_number').val(' ');
      }
        });
      });
    </script>
    
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>