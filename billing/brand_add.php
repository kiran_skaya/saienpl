<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from brands where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{
    $brandName = strtoupper($_POST['brand_name']);

   
    $sql="INSERT INTO brands(brand_name) VALUES('$brandName')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];


    header("location: brands.php");
}

if (isset($_POST['update']))
{

    $brandName = strtoupper($_POST['brand_name']);
    
    $id  = $item['id'];
    $updatequery = "update brands set brand_name = '$brandName' where id = $id";
    
   

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="brands.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
 

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Brand</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Brand</h3>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label>Brand Name<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="brand_name" id="brand_name" maxlength="50" autocomplete="off" value="<?php echo $item['brand_name']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="brands.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            brand_name : "required",
            address:"required",
            supplier_code:"required",
            balance : "required",
            scope : "required",
            contact_person : "required",
            btype : "required",
            email : "required",
            gst : "required",
            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            
            brand_name :"<span>Enter Brand name</span>",
            address:"<span>Enter Address</span>",
            supplier_code:"<span>Enter supplier code</span>",
            balance:"<span>Enter balance in rupees</span>",
            email:"<span>Enter Valid Email Id</span>",
            scope : "<span>enter scope of supply</span>",
            contact_person : "<span>enter name of contact person</span>",
            btype : "<span>Select business type</span>",
            gst : "<span>Enter GST Number</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var cname = $("#cname").val();
    if(cname == ''){
        alert('Enter Contact Person Name');
        return false;
    }
    var cnum = $("#cnum").val();
    if(cnum == ''){
        alert('Enter Contact Number');
        return false;
    }
    var cmail = $("#cmail").val();
    if(cmail == ''){
        alert('Enter Contact Email');
        return false;
    }
    var dept = $("#department").val();
    if(dept == ''){
        alert('Enter Department Name');
        return false;
    }
    var lnum = $("#lnum").val();
    
      $.ajax({

        url: 'add_vendor_contact.php',
        data:{

          'sid': sid,
          'cname': cname,
           'cnum': cnum,
            'cmail': cmail,
            'dept': dept,
            'lnum': lnum,
        },
        success: function(result){
        $("#previous").html(result);
        $('#cname').val('');
        $('#cnum').val('');
        $('#cmail').val('');
        $('#department').val('');
        $('#lnum').val('');
      }
        });
      });
    </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>