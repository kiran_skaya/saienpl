<?php
include 'connection.php';


if (isset($_SESSION['employee']['company_id']))
{

    $id = $_SESSION['employee']['company_id'];
    $sql = "select * from company where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['update']))
{
    $companyName =$_POST['company_name'];
    $address = $_POST['full_address'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $file = $_FILES['logo']['name'];
    if($file == ""){
        $file == $item['logo'];
    }
    else{
        $file = $_FILES['logo']['name'];
        $temp = $_FILES['logo']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }
    $id  = $item['id'];

    $updatequery = "update company set company_name = '$companyName', full_address = '$address', phone='$phone', email='$email', logo='$file' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("updated successfully")</script>';
        echo '<script>parent.location="company_profile.php"</script>';
}

$sql = "SELECT id, company_name FROM company";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, name FROM department";
$result = $con->query($sql);
$departmentList = array();
while ($row = $result->fetch_assoc()) {
    array_push($departmentList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Profile</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Profile</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Company Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="company_name" id="company_name" maxlength="50" autocomplete="off" value="<?php echo $item['company_name']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Address<span class="error">*</span></label>
                                <input type="text" class="form-control" name="full_address" id="full_address" autocomplete="off" value="<?php echo $item['full_address']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="phone" id="phone" maxlength="12" autocomplete="off" value="<?php echo $item['phone']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                <input type="text" class="form-control" name="email" id="email" maxlength="100" autocomplete="off" value="<?php echo $item['email']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Logo<span class="error">*</span></label>
                                <input type="file" class="form-control" name="logo" id="logo" autocomplete="off">
                            </div>
                            <img src="uploads/<?php echo $logo; ?>" width="100px" height="80px">
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <!--<button class="btn btn-error"><a href="employee.php">Cancel</a></button>-->
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            employee_id : "required",
            employee_name : "required",
            gender:"required",
            designation:"required",
            gaurdian : "required",
            department : "required",
            pf_code : "required",
            esi_code : "required",
            address : "required",
            email : "required",
            password: "required",
            id_company : "required",

            phone: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            employee_id : "<span>Enter Employee Id</span>",
            employee_name : "<span>Enter Employee name</span>",
            gender:"<span>Select Gender</span>",
            designation:"<span>Enter designation</span>",
            gaurdian:"<span>Enter Father/Husband Name</span>",
            department:"<span>Select Department</span>",
            pf_code : "<span>enter PF code number</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
            password : "<span>Enter Password</span>",
            id_company : "<span>Select Company Name</span>",
           phone:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>