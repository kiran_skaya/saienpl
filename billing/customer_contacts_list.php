<?php
include 'connection.php';
$sid = $_GET['sid'];
$cid = $_GET['cid'];

$viewquery = "SELECT a.* from contacts_has_visited_customer as a INNER JOIN visited_customer as c ON c.id=a.id_customer where a.id_sales_report='$sid' AND a.id_customer='$cid'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $itemdata = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $itemdata[$i]['contact_name'] = $row['contact_name'];
      $itemdata[$i]['contact_number'] = $row['contact_number'];
      $itemdata[$i]['contact_email'] = $row['contact_email'];
      $itemdata[$i]['attachment'] = $row['attachment'];
      $itemdata[$i]['remarks'] = $row['remarks'];
      $itemdata[$i]['id'] = $row['id'];
      $i++;
    }

    $id = $_GET['cid'];
    $sql = "select * from visited_customer where id = $id";
    $result = $con->query($sql);
    $item = $result->fetch_assoc();

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Contact List</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $sid ?>';
        var cid = '<?php echo $cid ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_customer_contact_of_engineer.php?id="+id+"&cid="+cid+"&sid="+sid;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
              <div class="page-title clearfix">
                    <h3>Customer Details</h3>
                </div>
                <div>
                  <h4><b>Name : </b><?php echo ucwords($item['customer_name']); ?></h4>
                  <h4><b>Address : </b><?php echo ucwords($item['address']); ?></h4>

                  <!-- <img style="width: 210px; height: 140px; position: absolute; left: 350px; top: 130px; cursor: pointer;" src="uploads/<?php echo $item['visit_card']; ?>" data-toggle="modal" data-target="#myModal"> -->
                  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

            <img style="width: 600px; height: 350px;" src="uploads/<?php echo $item['visit_card']; ?>">

      </div>
      
    </div>
  </div>
                </div>

                <div class="page-title clearfix">
                    <h3>More Contacts List</h3>
                        <a href="add_sales_engineer_report.php?id=<?php echo $sid; ?>" class="btn btn-primary">Back</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>SL. NO</th>
                                <th>CONTACT NAME</th>
                                <th>CONTACT NO</th>
                                <th>CONTACT EMAIL</th>
                                <th>ATTACHMENT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($itemdata); $i++)
                          {
                            $id = $itemdata[$i]['id'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $itemdata[$i]['contact_name']; ?></td>
                          <td><?php echo $itemdata[$i]['contact_number']; ?></td>
                          <td><?php echo $itemdata[$i]['contact_email']; ?></td>
                          <td><a href="uploads/<?php echo $itemdata[$i]['attachment']; ?>" target="_blank"><?php echo $itemdata[$i]['attachment']; ?></a></td>
                          <td style="text-align: center;"><a href="javascript:Ondelete(<?php echo $itemdata[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>