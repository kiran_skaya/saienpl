<?php
include 'connection.php';
$eid = $_SESSION['employee']['id'];
// $empId = $_SESSION['employee']['employee_id'];

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from enquiry where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $sql = "select * from enquiry_notes where id_enquiry = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $reviewList[$i]['id'] = $row['id'];
      $reviewList[$i]['note'] = $row['note'];
      $i++;
    }
}

$sql      = "select * from enquiry where created_by=$eid";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.04d", $resnum);
    $currMonth = date("m");
    $currYear = date("y");
    $nxtDate = date("y", strtotime("+1 year"));
    $eNo = "ENQ".$currMonth.$ref."/".$currYear."-".$nxtDate;

if (isset($_POST['save']))
{

    $enquiry =$_POST['enq_no'];
    $enquiryType = $_POST['enquiry_type'];
    $cname = $_POST['company_name'];
    $cperson = $_POST['enquire_handler'];
    $remarks = $_POST['remarks'];
    $enqDate = date("Y-m-d", strtotime($_POST['date']));
    $file = $_FILES['attachement']['name'];
    $temp = $_FILES['attachement']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);

    $sql="INSERT INTO enquiry(enq_no, enq_type, customer_id, enquire_handler, date, remarks, attachement, status, created_by) VALUES('$enquiry', '$enquiryType','$cname', '$cperson', '$enqDate', '$remarks', '$file', 'NA', '$eid')";
    $con->query($sql) or die(mysqli_error($con));

//     $result = $con->query("SELECT LAST_INSERT_ID() as id");
// $result = mysqli_fetch_array($result,MYSQLI_ASSOC);
// $last_id = $result['id'];

    header("location: enquiry.php");
}

if (isset($_POST['update']))
{

    $enquiry =$_POST['enq_no'];
    $enquiryType = $_POST['enquiry_type'];
    $cname = $_POST['company_name'];
    $cperson = $_POST['enquire_handler'];
    $remarks = $_POST['remarks'];
    $qtnNo = $_POST['qtn_no'];
    $enqDate = date("Y-m-d", strtotime($_POST['date']));
    $file = $_FILES['attachement']['name'];
    if($file == ""){
        $file = $item['attachement'];
    }
    else{
        $file = $_FILES['attachement']['name'];
     $temp = $_FILES['attachement']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);   
    }
    
    $id  = $item['id'];
    $updatequery = "update enquiry set enq_no = '$enquiry', enq_type='$enquiryType', customer_id='$cname', enquire_handler='$cperson', remarks='$remarks', attachement='$file', date='$enqDate', created_by='$eid' where id = $id";

    $res=$con->query($updatequery);
        echo '<script>parent.location="enquiry.php"</script>';
}

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, name FROM enquiry_type";
$result = $con->query($sql);
$enquiryTypeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($enquiryTypeList, $row);
  }
  
  $sql = "SELECT id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }

  $sql = "SELECT id, qtn_no FROM quotation";
$result = $con->query($sql);
$qtnList = array();
while ($row = $result->fetch_assoc()) {
    array_push($qtnList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Enquiry</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Enquiry</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Enquiry Number<span class="error">*</span></label>
                                <div id="enqfield">
                                <input type="text" class="form-control" name="enq_no" id="enq_no" maxlength="50" autocomplete="off" value="<?php if($item['enq_no']){ echo $item['enq_no'];} else{ echo $eNo; } ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Enquiry Type<span class="error"> *</span></label>
                                <select class="form-control selitemIcon" name="enquiry_type" id="enquiry_type" >
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($enquiryTypeList); $i++){?>
                                    <option value="<?php echo $enquiryTypeList[$i]['id']; ?>" <?php if($enquiryTypeList[$i]['id']==$item['enq_type']){ echo "selected=selected"; } ?>><?php echo $enquiryTypeList[$i]['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer Name<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="company_name" id="company_name" >
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($companyList); $i++){?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['customer_id']){ echo "selected=selected"; } ?>><?php echo $companyList[$i]['customer_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Equiry Handled Engineer<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="enquire_handler" id="enquire_handler" onchange="getEmployeeName()">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($employeeList); $i++){?>
                                    <option value="<?php echo $employeeList[$i]['id']; ?>" <?php if($employeeList[$i]['id']==$item['enquire_handler']){ echo "selected=selected"; } ?>><?php echo $employeeList[$i]['employee_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" autocomplete="off" value="<?php echo $item['date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Remarks<span class="error">*</span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Enquiry Attachment<span class="error"></span></label>
                                <input type="file" class="form-control" name="attachement" id="attachement" autocomplete="off" >
                                <?php if($item['attachement']){?>
                                    <a href="uploads/<?php echo $item['attachement']; ?>" target="_blank">Click Here to View</a>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="enquiry.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                <div class="page-title clearfix col-sm-12">
                    <h3>Notes</h3>
                </div>
                <div class="page-container">
                <div class="row add">
                        <div class="col-sm-4">
                            <label>Add New<span class="error">*</span></label>
                                <textarea  class="form-control" name="note" id="note" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Add Note" onclick="saveReview()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="edit_note"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example2">
                        <thead>
                            <tr>
                          <th>Note</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {
                              $cid = $reviewList[$i]['id'];
                          ?>
                        <tr>
                          <td><?php echo $reviewList[$i]['note']; ?></td>
                        <td><a href="javascript:Onedit(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       <div id="addfiles"></div>
                   </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Contacts</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-4">
                    <label>Contact Name</label>
                    <input type="text" class="form-control" name="cname" id="cname" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Department</label>
                    <input type="text" class="form-control" name="department" id="department" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Land Line Number</label>
                    <input type="text" class="form-control" name="lnum" id="lnum" autocomplete="off" maxlength="10">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Contact Number</label>
                    <input type="text" class="form-control" name="cnum" id="cnum" autocomplete="off" maxlength="10">
                </div>
                <div class="col-sm-4">
                    <label>Contact Email</label>
                    <input type="text" class="form-control" name="cmail" id="cmail" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Unit</label>
                    <input type="text" class="form-control" name="unit" id="unit" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
  <script>
      function getEmployeeName(){
        var id = $("#enquire_handler").val();
        var no = '<?php echo $item['enq_no']; ?>';
        if (no) {
            var eno = '<?php echo substr($item['enq_no'], 0, 15); ?>';
        }
        else{
            var eno = '<?php echo $eNo; ?>';
        }
                $.ajax({url: "get_employee_to_enqiryno.php?id="+id+"&eno="+eno, success: function(result){
                    $("#enqfield").html(result);
              }
            });
      }
  </script>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            enq_no : "required",
            company_name:"required",
            enquiry_type:"required",
            enquire_handler : "required",
            qtn_no : "required",
            date : "required",
            remarks : "required",
            qtn_sent : "required",

            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            enq_no : "<span>Enter enquiry Number</span>",
            company_name: "<span>Select Customer Name</span>",
            enquiry_type: "<span>Select Enquiry Type</span>",
            enquire_handler: "<span>Select Enquiry Handled Engineer</span>",
            qtn_no: "<span>Select Quotation No</span>",
            date: "<span>Select Date</span>",
            remarks: "<span>Enter remarks</span>",
            qtn_sent : "<span>Enter quotation sent</span>",

           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    function Onedit(cid)
    {
      $.ajax({url: "get_enquiry_note.php?id="+cid, success: function(result){
        $("#edit_note").html(result);
        $(".add").hide();
      }
    });
    }

function saveReview() {
        
            var note = $("#note").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }
            
            var enq_id='<?php echo $id?>';            
            $.ajax({

        url: 'add_enquiry_notes.php',
        method:'POST',
        data:{

          'note': note,
           'enqId': enq_id
        },
        success: function(result){
            // location.reload();
      }
        });
            
        }

        function editReview(id) {
        
            var note = $("#editnote").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }
            
         $.ajax({

        url: 'add_enquiry_notes.php',
        method:'POST',
        data:{

          'note': note,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>