<?php

include 'connection.php';

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
$sql      = "SELECT * FROM quotation_has_items WHERE id = '$id'";
    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $sql      = "select id, name, category_code from category";
$result           = $con->query($sql);
$catList = array();
while ($row = $result->fetch_assoc()) {
    array_push($catList, $row);
}

$sql      = "select id, name, sub_cat_code from sub_category";
$result           = $con->query($sql);
$subList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subList, $row);
}

 $table =   "<div class='row'>
                <div class='col-sm-6'>
                    <div class='form-group'>
                        <label>Category Name<span class='error'>*</span></label>
                            <select name='id_category' class='form-control selitemIcon' onchange='getSubcategory()' id='id_category' style='width: 260px;'>
                                <option value=''>SELECT</option>";
                                for($i=0; $i<count($catList); $i++) {
                                   $id = $catList[$i]['id'];
                                   $sub = $catList[$i]['name']."-".$catList[$i]['category_code'];
                             $table.= "<option value='$id'"; if($id==$item['id_category'])
                                    {
                                        $table.= "selected=selected";
                                    }

                              $table.= "> $sub";

                           

                             
                              $table.= '</option>';
                              }
                            $table.= "</select>

                        </div>
                    </div>
                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label>Sub Category Name<span class='error'>*</span></label>
                            <select name='id_subcategory' class='form-control selitemIcon' id='id_subcategory' onchange='getItem()' style='width: 260px;'>
                                <option value=''>SELECT</option>";
                                for($i=0; $i<count($subList); $i++) {
                                   $id = $subList[$i]['id'];
                                   $sub = $subList[$i]['name']."-".$subList[$i]['sub_cat_code'];
                             $table.= "<option value='$id'"; if($id==$item['id_subcategory'])
                                    {
                                        $table.='selected=selected';
                                    }

                              $table.= "> $sub";
                             
                              $table.= "</option>";
                              }
                            $table.="</select>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label>Item Name<span class='error'>*</span></label>
                            <select name='id_item' class='form-control selitemIcon' id='id_item' style='width: 260px;' onchange='getPrice()'>
                                <option value=''>SELECT</option>";
                                for($i=0; $i<count($subList); $i++) {
                                   $id = $subList[$i]['id'];
                                   $sub = $subList[$i]['name']."-".$subList[$i]['sub_cat_code'];
                             $table.= "<option value='$id'"; if($id==$item['id_subcategory'])
                                    {
                                        $table.= "selected=selected";
                                    }

                              $table.= "> $sub";
                             
                              $table.= '</option>';
                              }
                           $table.=" </select>
                        </div>
                    </div>
                </div>

         <div class='row'>
             <div class='col-sm-6'>
                <label>Unit Price</label>
                <input type='text' class='form-control' name='unit' id='unit' autocomplete='off' value='".$item['unit_price']."' onkeyup='getTotal()'>
            </div>
            <div class='col-sm-6'>
                <label>Quantity</label>
                <input type='text' class='form-control' name='totqnty' id='totqnty' autocomplete='off' value='".$item['quantity']."' onkeyup='getTotal()'>
            </div>
            <div class='col-sm-6'>
                    <label>Total Value</label>
                    <input type='text' class='form-control' id='qtn_value' name='qtn_value' value='".$item['total']."'>
            </div>
            </div>";

echo $table;

}

?>
