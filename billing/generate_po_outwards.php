<?php
include 'connection.php';
session_start();
$sid = session_id();
$eid = $_SESSION['employee']['id'];
$empId = $_SESSION['employee']['employee_id'];

if (isset($_GET['id']))
{
    $id = $_GET['id'];
    $sql = "select * from purchase_order where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $sql = "select * from po_outwards_clarification where id_po_outwards = $id";
    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $reviewList[$i]['id'] = $row['id'];
      $reviewList[$i]['clarification'] = $row['clarification'];
      $reviewList[$i]['date'] = date("d/m/Y", strtotime($row['date']));
      $i++;
    }
}

$sql="SELECT other_charges FROM po_outwards_items WHERE id_poout ='$id' AND other_charges >='0' ORDER BY id DESC LIMIT 0,1";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $otherCharges = $row['other_charges'];
  if ($otherCharges == "") {
    $otherCharges = "0.00";
  }
}

$viewquery = "SELECT a.*, b.name as catName, b.category_code, c.name as subcatName, c.sub_cat_code, d.name as itemName, d.code FROM po_outwards_items as a INNER JOIN category as b ON a.id_category=b.id INNER JOIN sub_category as c ON a.id_subcategory=c.id INNER JOIN item as d ON a.id_item=d.id WHERE a.id_poout ='$id' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['item'] = $row['itemName']."-".$row['code'];
      $career[$i]['category'] = $row['catName']."-".$row['category_code'];
      $career[$i]['subcat'] = $row['subcatName']."-".$row['sub_cat_code'];
      $career[$i]['quantity'] = $row['quantity'];
      $career[$i]['unit_price'] = $row['unit_price'];
      $career[$i]['total'] = $row['total'];
      $career[$i]['discount'] = $row['discount'];
      if($career[$i]['discount'] ==""){
        $career[$i]['discount'] = "0";
      }
      $career[$i]['id'] = $row['id'];
      $i++;
    }

    if(isset($_POST['send'])){

      $rname = $_POST['rname'];
    $rphone = $_POST['rphone'];
    $rdesig = $_POST['rdesig'];
    $billAddress = $_POST['billing_address'];
    
    $updatequery = "update purchase_order set regard_name = '$rname', regard_phone='$rphone', regard_designation='$rdesig', billing_address='$billAddress' where id = $id";

    $res=$con->query($updatequery);

      $query = "select po.*, v.vendor_name, v.email from purchase_order AS po INNER JOIN vendor AS v ON po.id_vendor=v.id where po.id = $id";
     
    $result1 = $con->query($query);
    $poVendor = $result1->fetch_assoc();

      $poNo = $poVendor['po_no'];
      $vendorEmail = $poVendor['email'];
    $vendorName = strtoupper($poVendor['vendor_name']);
    $regardName = ucwords($poVendor['regard_name']);
      $regardPhone = $poVendor['regard_phone'];
      $billingAddress = $poVendor['billing_address'];
      $regardDesignation = ucwords($poVendor['regard_designation']);
    $date = date("d/m/Y");

    $mailType = $_POST['mail_type'];

$to = $vendorEmail;
if ($mailType == "1") {
    $subject = "SAI ENTERPRISES- Purchase Order Sending";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR SIR/MADAM, </th></tr>
<tr>
<th style='text-align: center'>REFF: PO NO: $poNo Date: $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
PLEASE FIND THE ATTACHED PO AND DO THE NEEDFUL. ACKNOWLEDGE THE SAME.

<br>
</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
$regardName <br>
Mob: $regardPhone <br>
$regardDesignation

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "2") {
    
    $subject = "SAI ENTERPRISES- Purchase Order FallUp";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR SIR/MADAM, </th></tr>
<tr>
<th style='text-align: center'>REFF: PO NO: $poNo Date: $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
<br>
PLEASE CONFIRM THE READINESS OF THE ABOVE PO MATERIAL.

</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
$regardName <br>
Mob: $regardPhone <br>
$regardDesignation

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "3") {

$subject = "SAI ENTERPRISES- DRG FALLUP";

$message = "<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR SIR/MADAM, </th></tr>
<tr>
<th style='text-align: center'>REFF: PO NO: $poNo Date: $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
<br>
PLEASE SEND THE DRG FOR ABOVE PURCHASE ORDER IMMEDIATELY.

</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
$regardName <br>
Mob: $regardPhone <br>
$regardDesignation

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "4") {
    $subject = "SAI ENTERPRISES- Sending Dispatch Instruction";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR SIR/MADAM, </th></tr>
<tr>
<th style='text-align: center'>REFF PO NO. $poNo Date: $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
PLEASE DISPATCH ABOVE PO MATERIAL AS PER DI ATTACHED.
<br>
<a href='https://www.saienpl.com/billing_system/dispatch_instruction.php?id=$id' target='_blank'>DOWNLOAD DISPATCH INSTRUCTION</a>

<br
</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
$regardName <br>
Mob: $regardPhone <br>
$regardDesignation

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <mktg@saienpl.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";

mail($to,$subject,$message,$headers);

echo "<script>alert('Email Sent Succesfully')</script>";
echo "<script>parent.location='generate_po_outwards.php?id=$id'</script>";
}

  if(isset($_POST['send1'])){

    $rname = $_POST['rname'];
    $rphone = $_POST['rphone'];
    $rdesig = $_POST['rdesig'];
    $rdesig = $_POST['bill'];
      
     $query = "select po.id, po.po_no, po.po_date, v.vendor_name, v.email from purchase_order AS po INNER JOIN vendor AS v ON po.id_vendor=v.id where po.id = $id";
     
    $result1 = $con->query($query);
    $poVendor = $result1->fetch_assoc();

      $poNo = $poVendor['po_no'];
      $vendorEmail = $poVendor['email'];
    $vendorName = strtoupper($poVendor['vendor_name']);
    $date = date("d/m/Y");

$to = $vendorEmail;

$subject = "SAI ENTERPRISES- PO: $poNo";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR MR. </th></tr>
<tr>
<th style='text-align: left'>PO: $poNo</th>
<th style='text-align: center'>DATE: $date</th>
<th style='text-align: right; border: 1'><a href='https://www.atninstruments.com/billing_system/purchase_order_outwards_invoice.php?id=$id' target='_blank'>Click Here To Download PO</a> </th>
</tr>
</table>
<br>
<table>
<tr>
<th style='text-align: left'>
Enclosed is the PDF copy of SAI ENTERPRISES PO, Kindly acknowledge the <br>
receipt with Order Acceptance, confirming the delivery schedule and oblige.
<br><br>
Line of confirmation on receipt of this PO is highly appreciated. Do reply to <br>
the email without changing the SUBJECT line.
</th>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Anitha <br>
Purchase Officer <br>
Mob: 9379794006

<br><br>
For Technical details <br>
call : Bhagya Lakshmi <br>
Mob: 9379794006 <br><br>
M/s. SAI ENTERPRISES";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <info@atninstruments.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";

mail($to,$subject,$message,$headers);

echo "<script>alert('Email Sent succesfully')</script>";
echo "<script>parent.location='generate_po_outwards.php?id=$id'</script>";
}

if (isset($_POST['update']))
{
    $id = $_GET['id'];
        echo "<script>parent.location='purchase_order_outwards_invoice.php?id=$id'</script>";
}


$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, terms_condtion, terms_name FROM terms_conditions";
$result = $con->query($sql);
$termsList = array();
while ($row = $result->fetch_assoc()) {
    array_push($termsList, $row);
  }
  
  $sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_quotation_has_items");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> PO Outwards</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<!--<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">


</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Generate PO (Outwards)</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO Number<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="qtn_no" id="qtn_no" maxlength="50" autocomplete="off" value="<?php echo $item['po_no']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vendor <span class="error"> *</span></label>
                                <select class="form-control selitemIcon" name="enquiry" id="enquiry" disabled>
                                    <option value="">select</option>
                                    <?php for($i=0; $i<count($vendorList); $i++){?>
                                    <option value="<?php echo $vendorList[$i]['id']; ?>" <?php if($vendorList[$i]['id']==$item['id_vendor']){ echo "selected=selected"; } ?>><?php echo $vendorList[$i]['vendor_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Fallup Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="qtn_date" id="qtn_date" autocomplete="off" value="<?php echo $item['po_date']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error">*</span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>" readonly>
                            </div>
                        </div>
                        <!--<div class="col-sm-4">-->
                        <!--    <div class="form-group">-->
                        <!--        <label>Attachment<span class="error">*</span></label>-->
                        <!--        <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off" value="<?php echo $item['attachment']; ?>" readonly>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <br>
                                <label>Status <span class="error"> *</span></label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" id="status1" value="pending" <?php if($item['status']=='pending'){ echo "checked"; }?> disabled><span class="check-radio"></span> Pending
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="status" id="status2" value="approve"  <?php if($item['status']=='approve'){ echo "checked"; }?> disabled><span class="check-radio"></span> Approve
                                    </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mail Type <span class="error"> *</span></label>
                                <select name="mail_type" id="mail_type" class="form-control selitemIcon" onchange="getRegardField()">
                                    <option value="">SELECT</option>
                                    <option value="1">1. PURCHASE ORDER SENDING MAIL</option>
                                    <option value="2">2. PURCHASE ORDER FALLUP MAIL</option>
                                    <option value="3">3. DRG FALLUP MAIL</option>
                                    <option value="4">4. SENDING DI</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="row bill_address" style="display: none">
                  <div class="col-sm-6">
                    <label>Billing Address</label>
                    <textarea name="billing_address" id="billing_address" class="form-control" rows="3" ><?php echo $item['billing_address']; ?></textarea>
                  </div>
                </div>
                <br>
                <div class="row regards" style="display: none">
                  <div class="col-sm-4">
                    <h4>Regards :</h4>
                  </div>
                </div>
                <div class="row regards" style="display: none;">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" name="rname" id="rname" class="form-control" value="<?php echo $item['regard_name']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="text" name="rphone" id="rphone" class="form-control" value="<?php echo $item['regard_phone']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Designation</label>
                      <input type="text" name="rdesig" id="rdesig" class="form-control" value="<?php echo $item['regard_designation']; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 260px;">Add Items</button>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal1" class="btn btn-danger" data-toggle="modal" data-target="#myModal1" style="width: 260px;">Add Other Charges</button>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="submit" name="send" id="send" class="btn btn-success" style="width: 160px;">Send Mail To Vendor</button>
                            </div>
                        </div>
                </div>
                <div id="previous"></div>
                <div>
                        <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                          <th>Item Name</th>
                          <th>Quantity</th>
                          <th>Unit Price</th>
                          <th>Discount</th>
                          <th>Total</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {?>
                        <tr>
                          <td><?php echo $career[$i]['item']; ?></td>
                          <td><?php echo $career[$i]['quantity']; ?></td>
                          <td><?php echo $career[$i]['unit_price']; ?></td>
                          <td><?php echo $career[$i]['discount']."%"; ?></td>
                          <td><?php echo $career[$i]['total']; ?></td>
                          <td><a href="delete_po_out_item.php?id=<?php echo $career[$i]['id']; ?>&poid=<?php echo $id; ?>">DELETE</a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                    </div>
                    
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="purchase_order.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "ADD";} else {echo "Save";}?></button>
                   </div>
                </div>

                <div class="page-title clearfix col-sm-12">
                    <h3>Clarification Points</h3>
                </div>
                <div class="page-container">
                <div class="row add">
                        <div class="col-sm-4">
                          <label>Date</label>
                          <input type="text" name="clarf_date" id="clarf_date" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <label>Add New Clarification<span class="error">*</span></label>
                                <textarea  class="form-control" name="clarification" id="clarification" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Add Clarification" onclick="saveReview()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="edit_note"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example2">
                        <thead>
                            <tr>
                              <th>Date</th>
                          <th>Clarification</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {
                              $cid = $reviewList[$i]['id'];
                          ?>
                        <tr>
                          <td><?php echo $reviewList[$i]['date']; ?></td>
                          <td><?php echo $reviewList[$i]['clarification']; ?></td>
                        <td><a href="javascript:Onedit(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       <div id="addfiles"></div>
                   </div>

                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getPrice()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Remarks </label>
                            <input type="text" name="remark" id="remark" class="form-control">
                        </div>
                    </div>
                </div>
                    <div id="qtnfields"></div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Other Charges</h4>
        </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label> Other Charges </label>
                            <input type="text" name="other" id="other" class="form-control" value="<?php echo $otherCharges; ?>">
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="addOther" id="addOther" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <script>
  $( function() {
    // $( "#po_date" ).datepicker();
    $("#clarf_date").datepicker();
  } );
  </script>
  
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            quotation : "required",
            customer_name:"required",
            customer_mobile:"required",
            customer_email : "required",
            qtn_model : "required",
            qtn_date : "required",
            esi_code : "required",
            address : "required",
            email : "required",

            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            quotation : "<span>Enter quotation Number</span>",
            customer_name:"<span>Enter Customer Name</span>",
            customer_mobile:"<span>Enter Customer Contact number</span>",
            customer_email:"<span>Enter Customer Email</span>",
            qtn_model:"<span>Enter quotation Model</span>",
            qtn_date : "<span>enter quotation date</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
        function getSubcategory(){
          var id = $("#id_category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }

        function getRegardField(){
          var id = $("#mail_type").val();
          if (id=="1" || id=="2" || id=="3") {
            $(".regards").show();
            $(".bill_address").hide();
          }
          else
            if (id=="4") {
            $(".regards").show();
            $(".bill_address").show();
          }
          else{
            $(".regards").hide();
            $(".bill_address").hide();        
          }
        }
        
        function getPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_items_fields.php?id="+id, success: function(result){
            $("#qtnfields").html(result);
          }
        });
        }

    // $(document).on("keypress", "textarea", function(e){
    //     if(e.which == 13){
    //         var inputVal = $(this).val();
    //         alert("You've entered: " + inputVal);
    //         var startPos = inputVal.selectionStart;
    //         var endPos = inputVal.selectionEnd;
    //     }
    // });

    function typeInTextarea(newText, el = document.activeElement) {
    const start = el.selectionStart
    const end = el.selectionEnd
    const text = el.value
    const before = text.substring(0, start)
    const after  = text.substring(end, text.length)
    el.value = (before + newText + after)
    el.selectionStart = el.selectionEnd = start + newText.length
    el.focus()
  }

  document.getElementById("billing_address").onkeydown = e => {
    if (e.key === "Enter") typeInTextarea("<br>");
  }


    function getCalculate(){
        var unit= parseInt($('#unit').val());
            var totqnty = $("#totqnty").val();
            var purchase_cost = parseInt(unit * totqnty);
            // alert(purchase_cost);
            // console.log(purchase_cost);

        var per= $("#discount").val();
        var gst= $("#gst").val();
        
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var discountnum = (purchase_cost - pernum);
        var totnum = purchase_cost - discountnum;
        
        var gstpernum = (purchase_cost * 0.01 * (100 - gst));
        var gstnum = (purchase_cost - gstpernum);
        var totamt = totnum + gstnum;

        $('#total').val(totamt.toFixed(2));
    }
            
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var unit = $("#unit").val();
    if(unit == ""){
        alert('Enter Unit Price');
        return false;
    }
    var totqnty = $("#totqnty").val();
    if(totqnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var total = $("#total").val();
    if(total == ""){
        alert('Enter Total');
        return false;
    }
    var discount = $("#discount").val();
    if(discount == ""){
        alert('Enter Discount');
        return false;
    }
    
    var gst = $("#gst").val();
    if(gst == ""){
        alert('Enter GST Rate');
        return false;
    }
    var remark = $("#remark").val();
    
      $.ajax({

        url: 'add_item_to_po_outwards.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'unit': unit,
            'totqnty': totqnty,
            'discount': discount,
            'gst': gst,
            'total': total,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    $("#addOther").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var other = $("#other").val();
    if(other == ""){
        alert('Enter Other Charges');
        return false;
    }
    
      $.ajax({

        url: 'add_item_to_po_outwards.php',
        data:{

          'sid': sid,
            'other': other,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
        
        var id = $("#enquiry").val();
          console.log(id);

          $.ajax({url: "get_enquiry_fields.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
</script>
<script type="text/javascript">
    function Onedit(cid)
    {
      $.ajax({url: "get_po_outwards_clarification.php?id="+cid, success: function(result){
        $("#edit_note").html(result);
        $(".add").hide();
      }
    });
    }

function saveReview() {
        
            var note = $("#clarification").val();
            if(note== ""){
                alert('Enter Clarification');
                exit;
            }
            var date = $("#clarf_date").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }
            
            var enq_id='<?php echo $id?>';            
            $.ajax({

        url: 'add_po_outwards_clarification.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'enqId': enq_id
        },
        success: function(result){
            location.reload();
      }
        });
            
        }

        function editReview(id) {
        
            var note = $("#editclarification").val();
            if(note== ""){
                alert('Enter Clarification');
                exit;
            }
            var date = $("#edit_date").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }
            
         $.ajax({

        url: 'add_po_outwards_clarification.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
</html>