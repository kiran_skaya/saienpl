<?php

include 'connection.php';

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "SELECT a.id, a.rack_alloted_for_item, a.self_alloted_for_item, a.no_items_self, d.total_amount, d.rate, d.original_cost FROM design_sub_store as a INNER JOIN item as d ON a.id_item=d.id WHERE d.id = '$id'";
    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $sql1    = "SELECT SUM(qty_recevied) as qnty FROM sub_store_has_items WHERE id_item = '$id'";
    $result1 = $con->query($sql1);
    $item1 = $result1->fetch_assoc();
    
    if($item1['qnty']==''){
        $item1['qnty']=0;
    }
    
    $available = $item['no_items_self'] - $item1['qnty'];

 $table =   "<div class='row'>
                <div class='col-sm-4'>
                 <div class='form-group'>
                    <label>Purchase Price</label>
                    <input type='text' class='form-control' name='uprice' id='uprice' autocomplete='off' value='".$item['original_cost']."' readonly>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>GST Rate</label>
                    <input type='text' class='form-control' name='rate' id='rate' autocomplete='off' value='".$item['rate']."%"."' readonly>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Total Price</label>
                    <input type='text' class='form-control' name='total' id='total' autocomplete='off' value='".$item['total_amount']."' readonly>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Rack Number</label>
                    <input type='text' class='form-control' name='rnum' id='rnum' autocomplete='off' value='".$item['rack_alloted_for_item']."'readonly >
                </div>
            </div>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Self Number</label>
                    <input type='text' class='form-control' name='snum' id='snum' autocomplete='off' value='".$item['self_alloted_for_item']."' readonly>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Max Quantity</label>
                    <input type='text' class='form-control' name='totqnty' id='totqnty' autocomplete='off' value='".$item['no_items_self']."' readonly>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Current Quantity</label>
                    <input type='text' class='form-control' name='curqnty' id='curqnty' autocomplete='off' value='".$item1['qnty']."' readonly>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class='form-group'>
                    <label>Available Quantity</label>
                    <input type='text' class='form-control' name='avilable' id='avilable' autocomplete='off' value='".$available."' readonly>
                </div>
            </div>
            
        </div>

                <input type='hidden' class='form-control' name='design' id='design' autocomplete='off' value='".$item['id']."' >";

echo $table;

}

?>
