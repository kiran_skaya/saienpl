<?php
include 'connection.php';

if (isset($_GET['id']))
{
    $id = $_GET['id'];
    $sql = "select * from purchase_bill where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$sql      = "select * from purchase_bill";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currYear."-".$nxtYear;

if (isset($_POST['save']))
{
    $vendorId =$_POST['id_vendor'];
    $grn_no =$_POST['grn_no'];
    $grnDate =date("Y-m-d", strtotime($_POST['grn_date']));
    $purchase_bill_no =$_POST['po_no'];
    $total_value ='0';
    $balance ='0';
    $payStatus = $_POST['PENDING']."($total_value)";
    $remarks = $_POST['remarks'];
    $date = date("Y-m-d", strtotime($_POST['date']));
   
    $sql="INSERT INTO purchase_bill(id_vendor, grn_no, grn_date, po_no_id, total_value, balance, payment_status, remarks, date) VALUES('$vendorId', '$grn_no', '$grnDate', '$purchase_bill_no', '$total_value', '$balance', '$payStatus', '$remarks', '$date')";

    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: purchase_bill.php");
}

if (isset($_POST['update']))
{

    $vendorId =$_POST['id_vendor'];
    $grn_no =$_POST['grn_no'];
    $grnDate =date("Y-m-d", strtotime($_POST['grn_date']));
    $purchase_bill_no =$_POST['po_no'];
    $total_value =$_POST['total_value'];
    $remarks = $_POST['remarks'];
    $date = date("Y-m-d", strtotime($_POST['date']));
    
    $id  = $item['id'];
    $updatequery = "update purchase_bill set id_vendor = '$vendorId', grn_no = '$grn_no', po_no_id = '$purchase_bill_no', total_value = '$total_value', remarks='$remarks', date='$date' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="purchase_bill.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Bill</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />


</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Bill</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>GRN No<span class="error">*</span></label>
                                <input type="text" class="form-control" name="grn_no" id="grn_no" autocomplete="off" value="<?php if($item['grn_no']){ echo $item['grn_no']; }else{ echo $serialNo; } ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>GRN Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="grn_date" id="grn_date" autocomplete="off" value="<?php echo $item['grn_date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vendor Name<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="id_vendor" id="id_vendor" onchange="getpoNo()">
                                    <option value="">select</option>
                                    <?php for($i=0; $i<count($companyList); $i++){?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['id_vendor']){ echo "selected=selected"; } ?>><?php echo $companyList[$i]['vendor_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO NO<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="po_no" id="po_no" onchange="getPoItems()">
                                    <option value="">SELECT</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error">*</span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" autocomplete="off" value="<?php echo $item['date']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <div id="po_items"></div>

                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="purchase_bill.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_vendor:"required",
                remarks:"required",
                date : "required"
            },
            messages:{
                id_vendor:"<span>Select Vendor Name</span>",
                remarks:"<span>Enter Remarks</span>",
                date:"<span>select Date</span>"
            },
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $( "#grn_date" ).datepicker();
  } );
  </script>
  <script type="text/javascript">
        function getpoNo(){
          var id = $("#id_vendor").val();
          console.log(id);

          $.ajax({url: "get_po_out_number.php?id="+id, success: function(result){
            $("#po_no").html(result);
          }
        });
        }

        function getPoItems(){
            var poid = $("#po_no").val();
            $.ajax({url: "get_po_out_items.php?id="+poid, success: function(result){
            $("#po_items").html(result);
          }
        });
        }
    </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>