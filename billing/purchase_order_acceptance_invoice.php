<?php
include('connection.php');

date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d/m/Y');

$id = $_GET['id'];

$query = "select po.*, v.customer_name, v.email from purchase_order_inwards AS po INNER JOIN customer AS v ON v.id=po.id_customer where po.id = $id";

    $result1 = $con->query($query);
    $poCustomer = $result1->fetch_assoc();
    
    $customerName = $poCustomer['customer_name'];
    $poNo = $poCustomer['customer_po_no'];
    $oaNo = $poCustomer['oa_no'];
    $poDate = $poCustomer['date'];


$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');

        $file_data = $file_data ."
        <div width='100%' style='background-color: #3639a4; height: 100px; text-align: right;'>
    <h2><b style='font-size: 50px; color: orange;'>SAI</b> <b style='font-size: 50px; color: white;'>Enterprises</b> &emsp;
      <br><i style='position: relative; color: white; font-size: 15px;'>LEADING AUTOMATION DISTRIBUTER</i> &emsp;</h2>
      
    </div>
    <br>
      <table cellspacing='0' cellpadding='0' style='width: 100%'>

          <tr>
            <th style='text-align: left'>To,</th>
            <th></th>
          </tr>
          <tr>
            <th style='text-align:left'>$customerName </th>
            <td style='text-align:right'>OA NO : $oaNo DATE :  $date</td>
          </tr>
          <tr>
            <th style='text-align:left'>BANGALORE - 560072</th>
            <td></td>
          </tr>
      </table>
      <br>
      <b>Sub: Purchase Order Acceptance.<br><br>
      
      </b>
      <div>
        Dear Sir/Madam, <br>
      </div>
      <div>
        We are acknowledge with thanks receipt of your above mentioned confirmation of Purchase Order, Which has been Registered. To enable us to process your order, Please effect following amendments /clarifications to your order.<br>
        <br>
        <b>Following clarifications are required immediately per return to manufacture ordered goods:</b> <br>";

        $sql = "select * from oa_clarifications where id_inwards = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $reviewList[$i]['clarification'] = $row['clarification'];
      $i++;
    }
$file_data .="
    <table>";
        for ($i=0; $i<count($reviewList); $i++){
          $sl = $i+1;
          $clarification = $reviewList[$i]['clarification'];
          $file_data .="
      <tr>
        <td>$sl. $clarification</td>
      
      </tr>";
    }
    $file_data .="

    </table>

        
      </div>";

      $file_data .="<br>
      <b style='color:green'>BARRING UNFORESEEN CIRCUMSTANCES, THE GOODS WILL BE READY BY YOUR SCHEDULE</b> <br><br>
      In case we do receive your reply to the above deviations within 3 days, then we shall assume that the above terms are acceptable to you and shall dispatch the material. <br><br>
      <b>BANK DETAILS</b> <br><br>
      A/c Name: SAI ENTERPRISES <br>
      A/c No: 146201007427 <br>
      A/c Type: Current A/c <br>
      RTGS/IFSC Code: CNRB 0001146 <br>
      MICR Code: 560015062 <br>
      Bank: Canara Bank <br>
      Branch: Vijaynagar <br>
      City: Bangalore-560040 <br>
      state: Karnataka"; 

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetFooter('<div style="text-align: center">No. 107, 1st Floor, MEI Colony, Laggere main Road, Peenya Industrial Area (Wd.), Bengaluru 560058 <br>
  Ph: 080 - 2839 8596, Mobile: +91 93797 94006, E-mail: saientp_ajit@hotmail.com, Website: saienpl.com</div>');
$mpdf->WriteHTML($file_data);
$filename = "OA"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'D');
exit;