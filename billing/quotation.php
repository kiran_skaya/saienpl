<?php

include 'connection.php';

$viewquery = "SELECT a.*, b.enq_no, c.customer_name, c.address, c.mobile FROM quotation as a INNER JOIN enquiry as b ON a.enquiry_id=b.id INNER JOIN customer as c ON a.customer_id=c.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['qtn_no'] = $row['qtn_no'];
  $address = wordwrap($row['address'], 20, "<br/>");
  $career[$i]['customer_name'] = $row['customer_name']."<br>".$address."<br>".$row['mobile'];
  $career[$i]['enq_no'] = $row['enq_no'];
  $career[$i]['status'] = $row['status'];
  $career[$i]['qtn_date']= $row['qtn_date'];
  $career[$i]['qtn_date']= $row['qtn_date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quotation</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?qtn_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Quotations</h3>
                     <a href="quotation_add.php" class="btn btn-primary">+ Create Quotation</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                            <th>Quotation No</th>
                            <th>Enquiry No</th>
                            <th>Amount</th>
                          <th>Customer</th>
                          <th>Status</th>
                          <th>Revision</th>

                          <th>Qtn Generate</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                              
                                $id = $career[$i]['id'];
                                $viewquery = "SELECT a.*  FROM quotation_has_items as a   WHERE a.id_quotation ='$id' ";
                            $viewqueryresult = mysqli_query($con,$viewquery);
                            $qtnamount = 0;
                            while ($row = mysqli_fetch_array($viewqueryresult))
                            {
                              $qtnamount = (float)$qtnamount+(float)$row['total'];
                              
                            }
                            
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['qtn_no']; ?></td>
                          <td><?php echo $career[$i]['enq_no']; ?></td>
                                                    <td><?php echo $qtnamount; ?></td>

                          <td><?php echo $career[$i]['customer_name']; ?></td>
                          <td><?php echo $career[$i]['status']; ?></td>
                            <td><a href="quotation_revision.php?idquotation=<?php echo $career[$i]['id']; ?>">Quotation Revision</a></td>
                            
                          <td><a href="generate_quotation.php?id=<?php echo $career[$i]['id']; ?>" class="btn btn-primary">Generate</a></td>
                          <td><a href="quotation_add.php?id=<?php echo $career[$i]['id']; ?>"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>