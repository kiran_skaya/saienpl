<?php
include 'connection.php';

$id = $_GET['id'];
$sql = "SELECT a.*, b.enq_no, c.customer_name, c.address, c.mobile, c.email FROM quotation as a INNER JOIN enquiry as b ON a.enquiry_id=b.id INNER JOIN customer as c ON a.customer_id=c.id where a.id='$id' ";
$result = $con->query($sql) or die($con->error);
while ($row = mysqli_fetch_array($result))
{
	$customer_Name = $row['customer_name'];
	$address = wordwrap($row['address'], 20, "<\n>");
	$phone = $row['mobile'];
	$enqNo = $row['enq_no'];
	$qtnNo = $row['qtn_no'];
	$email = $row['email'];
	$time = date("H:i:sa");
	$ino = rand();
}

$viewquery = "SELECT b.* FROM quotation_has_items as a INNER JOIN item as b ON a.id_item=b.id where a.id_quotation='$id'";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['code'] = $row['code'];
  $career[$i]['description'] = $row['description'];
  $career[$i]['technical_detail'] = $row['technical_detail'];
  $career[$i]['unit_price']= $row['unit_price'];
  $career[$i]['quantity']= $row['quantity'];
  $career[$i]['total']= $row['total'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en" >

<head><meta charset="euc-kr">
  
  <title>Quotation | Sai Enterprises</title>

      <link rel="stylesheet" href="invoices/css/invoice.css">
    <script type="text/javascript" src="invoices/js/jquery.min.js"></script>


<style type="text/css">
	#heading{
		background-color: #3639a4;
		height: 100px;
		text-align: right;
	}
	#head{
		background-color: #3639a4;
	}
	#company{
		font-size: 50px;
		color: orange;
	}
	#tag{
		font-size: 50px;
		color: white;
	}
	i{
		position: relative;
		color: white;
		font-size: 15px;
	}
	#headline{
		text-align: center;
		color: #3639a4;
		font-size: 25px;

	}

</style>
</head>
<body>
	
	<div id="page-wrap">
		<div width="100%">
		<h2 id="heading"><b id="company">SAI</b> <b id="tag">Enterprises</b> &emsp;
			<br><i>LEADING AUTOMATION DISTRIBUTER</i> &emsp;</h2>
			
		</div>
		<br>
		<div id="headline">
			<b >Quotation</b>
		</div>
			
	<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">

          <tr>
            <td style="text-align: left;">
            	<b>To,</b><br>
              <?php echo $customer_Name; ?> <br>
              <?php echo $address; ?> <br>
              MOB: <?php echo $phone; ?> <br>
              EMAIL : <?php echo $email; ?>
            </td>
            <td style="text-align: right;">
            	<b>SAI ENTERPRISES</b><br>
              NO 107, 1<sup>ST</sup> FLOOR, MEI COLONY, <br>
              LAGGERE MAIN ROAD, <br>
              PEENYA INDUSTRIAL AREA, <br>
              BENGALURU- 560058. <br>
              Tel.: +91-80-28398596 <br>
              E-Mail: saientp_ajit@hotmail.com <br>
              Visit us @ www.saienpl.com
            </td>
          </tr>
      </table>
      <br>

      <table cellspacing="0" cellpadding="0" style="width: 100%">

          <tr>
            <th>Vendor Code:</th>
            <td></td>
          </tr>
          <tr>
            <th>Our Ref: </th>
            <td>0122R10IE /17-18</td>
          </tr>
          <tr>
            <th>Your Ref: </th>
            <td>020097/17-18I Dt 15/05/2017</td>
          </tr>
      </table>
      <br>
      <b>Sub: Quotation for the References mentioned above.<br><br></b>
      <div>
      	Dear Sir, <br>
      </div>
      <div>
      	Good day to you. At the outset we team SAI ENTERPRISES thank you very much for the support extended.<br>
      	Further to the inquiry, we are pleased to forward our best offer for the same.<br>
      </div>
<br>
<?php if(!empty($career)){
            
                  for ($i=0; $i<count($career); $i++){
                      $itemname = $career[$i]['name'];
                      $itemcode = $career[$i]['code'];
                      $itemdesc = $career[$i]['description'];
                      $itemtech = $career[$i]['technical_detail'];
                      $itemprice = $career[$i]['unit_price'];
                      $itemqnty = $career[$i]['quantity'];
                      $itemtotal = $career[$i]['total'];
            ?>
      <table border="1" cellspacing="0" cellpadding="0" style="width: 100%">

          <tr>
            <td style="text-align: left;" colspan="2">
            	Item #: <?php echo $i+1; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">ITEM Name:</td>
            <td style="text-align: right;">- <?php echo $itemname; ?></td>
         </tr>
        <tr>
            <td style="text-align: left;">ITEM CODE:</td>
            <td style="text-align: right;">- <?php echo $itemcode; ?></td>
         </tr>
         <tr>
            <td style="text-align: left;">ITEM DESCRIPTION:</td>
            <td style="text-align: right;">- <?php echo $itemdesc; ?></td>
         </tr>
      </table>
      <table cellspacing="0" cellpadding="0" style="width: 100%;" border="1">
      	<tr>
      	    <td><?php echo $itemtech; ?></td>
      	</tr>
        <tr>
          <h4>All other specifications as per enclosed technical leaflet</h4>
        </tr>
      </table>
      <br><br>
      <?php } 
          } ?>
      
		
		</div>

		
		<div style="clear:both"></div>
		
		<br><br><br><br>
		<div id="terms">
		  <h5>SAI ENTERPRISES</h5>
		  Contact us at www.saienpl.com
		</div>
		<div id="terms">
			<button id="button"><font size="4px;"><<- Print ->></font></button>
		</div>
		
	</div>
	
<script type="text/javascript">

    function printPdf()
    {
        window.print();
    }
    $('button').on('click',function(){
        printPdf();
    })
</script>
</body>

</html>