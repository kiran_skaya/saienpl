<?php

include 'connection.php';

$viewquery = "Select * from sales";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['enquiry'] = $row['enquiry'];
  $career[$i]['quotation'] = $row['quotation'];
  $career[$i]['sales_bill'] = $row['sales_bill'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sales</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    
  <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?mstore_id='+id;
      }
    }
  </script>

<style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   #page_list li
   {
    padding:16px;
    background-color:#f9f9f9;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   #page_list li.ui-state-highlight
   {
    padding:24px;
    background-color:#ffffcc;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   .btn-primary{
       color: #fff;
       background-color:#337ab7;
       border-color: #2e6da4;
   }
   .btn-primary:hover{
       color: #fff;
       background-color:#0057a1;
       border-color: #337ab7;
   }
  </style>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Sales</h3>
                        <a href="sales_add.php" class="btn btn-primary">+ Add Sales</a>
                    </div>
                    <div class="header">
                      <table class="table table-striped" id="example">
                        <tr>
                          <th>Enquiry</th>
                          <th>Quotation</th>
                          <th>Sales Bill Number</th>
                          <!--<th>Edit&nbsp; | &nbsp;Delete</th>-->
                        </tr>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = $career[$i]['enquiry'];
                            $quote = $career[$i]['quotation'];
                            $bill = $career[$i]['sales_bill'];
                            ?>
                        <tr>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $quote; ?></td>
                          <td><?php echo $bill; ?></td>
                          <!--<td><a href="sales_add.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a></td>-->
                        </tr>
                          <?php
                          }
                          ?>

                        
                      </table>
                    </div>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <script src="../js/jquery-3.3.1.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
       <script src="../js/dataTables.jqueryui.min.js"></script>
       <script>
            $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
  
</body>

</html>