<?php

include 'connection.php';

$viewquery = "Select s.*, m.name as mainstore from sub_store s inner join main_store m on m.id=s.id_main_store";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['mainstore'] = $row['mainstore'];
  $career[$i]['name'] = $row['name'];
  $career[$i]['email'] = $row['email'];
  $career[$i]['mobile'] = $row['mobile'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

if (isset($_POST['save']))
{
    $mainStrId = $_POST['id_mainstore'];
    $subStrId = $_POST['id_substore'];
     $categoryId = $_POST['id_category'];
     $subCategoryId = $_POST['id_subcategory'];
     $itemId = $_POST['id_item'];
	$rnum = $_POST['rnum'];
	$snum = $_POST['snum'];
	$uprice = $_POST['uprice'];
	$rate = $_POST['rate'];
	$total = $_POST['total'];
	$qnty = $_POST['qnty'];
	$grn = $_POST['grn'];
	$vendor = $_POST['vendor'];
	$totqnty = $_POST['totqnty'];
	$billno = $_POST['billno'];
	$bill_date = date("Y-m-d", strtotime($_POST['bill_date']));
	
	$check ="SELECT SUM(qty_recevied) as qtyRecevied FROM sub_store_has_items WHERE rack_no='$rnum' AND self_no = '$snum' AND id_sub_store='$subStrId'";
  $checkresult = mysqli_query($con,$check);

    $subStoreItem = $checkresult->fetch_assoc();
    $final = $subStoreItem['qtyRecevied']+$qnty;
    
  if($final <= $totqnty){
	
$sql = "INSERT into sub_store_has_items(id_sub_store, id_category, id_subcategory, id_item, rack_no, self_no, unit_price, total_quantity, gst_rate, total_value, grn, invoice_no, date, vendor_id, qty_recevied) VALUES('$subStrId', '$categoryId', '$subCategoryId', '$itemId', '$rnum', '$snum', '$uprice', '$totqnty', '$rate', '$total', '$grn', '$billno', '$bill_date', '$vendor', '$qnty')";

	$result = $con->query($sql);
	echo "<script>alert('Items Added Succesfully.')</script>";
    echo "<script>parent.location='history.php?strId=$subStrId&rno=$rnum&sno=$snum'</script>";
}
  else{
      echo "<script>alert('self is full!! Cannot add items anymore.')</script>";
  }
 
}

$sql = "SELECT id, name FROM main_store";
$result = $con->query($sql);
$mainStoreList = array();
while ($row = $result->fetch_assoc()) {
    array_push($mainStoreList, $row);
  }
  
  $sql = "SELECT id, name FROM sub_store";
$result = $con->query($sql);
$subStoreList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subStoreList, $row);
  }
  
$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  $sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sub Store List</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?comp_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
        <form method="POST" action="" enctype="multipart/form-data" id="form">
            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Store Transfer Note(Stock Addition)</h3>
                </div>
                <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Main Store<span class="error">*</span></label>
                            <select name="id_mainstore" class="form-control selitemIcon" onchange="getSubStore()" id="id_mainstore" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($mainStoreList); $i++)
                                    { 
                                        $value=$mainStoreList[$i]['id'];
                                        $label=$mainStoreList[$i]['name'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub Store<span class="error">*</span></label>
                            <select name="id_substore" class="form-control selitemIcon" id="id_substore" style="width: 300px;">
                                <option value="">SELECT</option>
                            </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 300px;" onchange="getDesign()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" ><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                    <div id="result"></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN</label>
                        <input type="text" name="grn" id="grn" class="form-control">
                        </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Invoice No</label>
                        <input type="text" name="billno" id="billno" class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date</label>
                        <input type="text" name="bill_date" id="bill_date" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor</label>
                        <select name="vendor" id="vendor" class="form-control selitemIcon" style="width: 300px;">
                        <option value="">SELECT</option>
                        <?php 
                        for($i=0; $i<count($vendorList); $i++){?>
                        <option value="<?php echo $vendorList[$i]['id']; ?>"><?php echo $vendorList[$i]['vendor_name']; ?></option>
                        <?php
                        }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quantity Received</label>
                        <input type="text" name="qnty" id="qnty" class="form-control">
                        </div>
                </div>
            </div>
              <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="sub_store_list.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="save">Save</button>
                   </div>
                </div>
            </div>
        </form>
        </div>
        <div id="previous"></div>
        <input type="hidden" name="store" id="store" >
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Main Store<span class="error">*</span></label>
                            <select name="id_mainstore" class="form-control selitemIcon" onchange="getSubStore()" id="id_mainstore" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i <count($mainStoreList); $i++)
                                    { 
                                        $value=$mainStoreList[$i]['id'];
                                        $label=$mainStoreList[$i]['name'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub Store<span class="error">*</span></label>
                            <select name="id_substore" class="form-control selitemIcon" id="id_substore" style="width: 260px;">
                                <option value="">SELECT</option>
                            </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getDesign()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" ><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="result"></div>
                <div class="col-sm-4">
                    <label>GRN</label>
                    <input type="text" name="grn" id="grn" class="form-control">
                </div>
                <div class="col-sm-4">
                    <label>Invoice No</label>
                    <input type="text" name="billno" id="billno" class="form-control">
                </div>
                <div class="col-sm-4">
                    <label>Date</label>
                    <input type="text" name="bill_date" id="bill_date" class="form-control">
                </div>
                <div class="col-sm-4">
                    <label>Vendor</label>
                    <select name="vendor" id="vendor" class="form-control selitemIcon" style="width: 280px;">
                        <option value="">SELECT</option>
                        <?php 
                        for($i=0; $i<count($vendorList); $i++){?>
                        <option value="<?php echo $vendorList[$i]['id']; ?>"><?php echo $vendorList[$i]['vendor_name']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-sm-4">
                    <label>Quantity Received</label>
                    <input type="text" name="qnty" id="qnty" class="form-control">
                </div>
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
        </div>
      </div>
      
    </div>
  </div>
  
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
       <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#bill_date" ).datepicker();
  } );
  </script>
<script type="text/javascript">
        function getPrice(){
        //   var id = $("#item").val();
          var id = $("#store").val();
          console.log(id);

          $.ajax({url: "get_price.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
        }
        
        function getDesign(){
          var idItem = $("#id_item").val();
          var idStr = $("#id_substore").val();
          console.log(idItem);

          $.ajax({url: "get_StoreDesign.php?id="+idItem, 
          success: function(result){
            $("#result").html(result);
          }
        });
        }
</script>
<script type="text/javascript">
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#store").val(strid);
      
      
      var id = $("#store").val();
          console.log(id);

          $.ajax({url: "get_price.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
    }
        
    $("#add").on('click',function(){
    var id = $("#design").val();
     var strid = $("#id_substore").val();
    var qnty = $("#qnty").val();
    if(qnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var billno = $("#billno").val();
    if(billno == ""){
        alert('Enter Invoice Number');
        return false;
    }
    var bill_date = $("#bill_date").val();
    if(bill_date == ""){
        alert('Select Date');
        return false;
    }
    var vendor = $("#vendor").val();
    if(vendor == ""){
        alert('Select Vendor Name');
        return false;
    }
    var grn = $("#grn").val();
    if(grn == ""){
        alert('Select GRN');
        return false;
    }
    var uprice = $("#uprice").val();
    var rate = $("#rate").val();
    var total = $("#total").val();
    var rnum = $("#rnum").val();
    var snum = $("#snum").val();
    var totqnty = $("#totqnty").val();
    
      $.ajax({

        url: 'add_items_to_store.php',
        data:{

          'id': id,
          'strid': strid,
           'qnty': qnty,
            'billno': billno,
            'uprice' : uprice,
            'bill_date': bill_date,
            'vendor': vendor,
            'grn': grn,
            'rate' : rate,
            'total' : total,
            'rnum' : rnum,
            'snum' : snum,
            'totqnty' : totqnty,
            
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }
    
    function getSubStore(){
      var id = $("#id_mainstore").val();
      console.log(id);

      $.ajax({url: "get_sub_store.php?id="+id, success: function(result){
        $("#id_substore").html(result);
      }
    });
    }
</script>
<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
</body>

</html>