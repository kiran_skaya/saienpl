<?php
include 'connection.php';
session_start();
$sid = session_id();
$id = $_GET['id'];
    $sql = "select * from terms_conditions where id = '$id'";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

if (isset($_POST['save']))
{
    // $terms_name = $_POST['terms_name'];
    $terms_condtion = $_POST['terms_condtion'];
    
    $updatequery = "INSERT INTO terms_conditions(terms_name, terms_condtion)VALUES ('$terms_name', '$terms_condtion')";
 
    $res=$con->query($updatequery);
    if($res){
        echo "<script>parent.location='terms_conditions_list.php'</script>";
    }
    
}
if (isset($_POST['update']))
{
    $id  = $_GET['id'];

    // $terms_name = $_POST['terms_name'];
    $terms_condtion = $_POST['terms_condtion'];
    
    $updatequery = "update terms_conditions set terms_name='$terms_name', terms_condtion='$terms_condtion'  where id = $id";
 
    $res=$con->query($updatequery);
    if($res){
        echo "<script>parent.location='terms_conditions_list.php'</script>";
    }
    
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Terms</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    #item_name{
        text-transform: UPPERCASE;
    }
    #file{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Terms & Conditions</h3>
                    </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="terms_condtion" id="terms_condtion" value="<?php if (!empty($item['terms_condtion'])) {echo $item['terms_condtion'];}?>">
                                </div>
                            </div>
                        </div>
                        
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="terms_conditions_list.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                 <!-- Modal -->
  
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
</html>