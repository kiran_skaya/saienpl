<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from vendor where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $viewquery = "Select * from vendor_contact_details where vendor_id='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['contact_name'] = $row['contact_name'];
      $career[$i]['contact_mobile'] = $row['contact_mobile'];
      $career[$i]['contact_email'] = $row['contact_email'];
      $career[$i]['department'] = $row['department'];
      $career[$i]['contact_landline'] = $row['contact_landline'];
      $i++;
    }
}

if (isset($_POST['save']))
{
    $vendorName = $_POST['vendor_name'];
    $address=$_POST['address'];
    $supplierCode=$_POST['supplier_code'];
    $balance= $_POST['balance'];
    $scope= $_POST['scope'];
    $contactPerson = $_POST['contact_person'];
    $bType = $_POST['btype'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $gst = $_POST['gst'];
   
    $sql="INSERT INTO vendor(vendor_name, address, supplier_code, balance, contact_person, scope, business_type, mobile, email, gst) VALUES('$vendorName','$address','$supplierCode','$balance','$scope','$contactPerson','$bType','$mobile','$email', '$gst')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_vendor_contact_details WHERE session_id='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['contact_name'] = $row['contact_name'];
    $fetch[$i]['contact_mobile'] = $row['contact_mobile'];
    $fetch[$i]['contact_email'] = $row['contact_email'];
    $fetch[$i]['department'] = $row['department'];
    $fetch[$i]['contact_landline'] = $row['contact_landline'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['contact_name'];
    $cmobile = $fetch[$i]['contact_mobile'];
    $cmail= $fetch[$i]['contact_email'];
    $dept= $fetch[$i]['department'];
    $ladline= $fetch[$i]['contact_landline'];

   $insertbill = "INSERT INTO vendor_contact_details(vendor_id, contact_name,department, contact_landline, contact_mobile, contact_email) VALUES ('$id', '$cname', '$dept', '$ladline', '$cmobile', '$cmail')";
    $result = mysqli_query($con,$insertbill);
    }

    header("location: vendor.php");
}

if (isset($_POST['update']))
{

    $vendorName = $_POST['vendor_name'];
    $address=$_POST['address'];
    $supplierCode=$_POST['supplier_code'];
    $balance= $_POST['balance'];
    $scope= $_POST['scope'];
    $contactPerson = $_POST['contact_person'];
    $bType = $_POST['btype'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $gst = $_POST['gst'];
    
    $id  = $item['id'];
    $updatequery = "update vendor set vendor_name = '$vendorName', address = '$address', supplier_code='$supplierCode', balance='$balance', contact_person='$contactPerson', scope='$scope', business_type='$bType', mobile='$mobile', email='$email', gst='$gst' where id = $id";
    
    $tempsql = mysqli_query($con,"SELECT * FROM temp_vendor_contact_details WHERE session_id='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['contact_name'] = $row['contact_name'];
    $fetch[$i]['contact_mobile'] = $row['contact_mobile'];
    $fetch[$i]['contact_email'] = $row['contact_email'];
    $fetch[$i]['department'] = $row['department'];
    $fetch[$i]['contact_landline'] = $row['contact_landline'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['contact_name'];
    $cmobile = $fetch[$i]['contact_mobile'];
    $cmail= $fetch[$i]['contact_email'];
    $dept= $fetch[$i]['department'];
    $ladline= $fetch[$i]['contact_landline'];

   $insertbill = "INSERT INTO vendor_contact_details(vendor_id, contact_name,department, contact_landline, contact_mobile, contact_email) VALUES ('$id', '$cname', '$dept', '$ladline', '$cmobile', '$cmail')";

    $result = mysqli_query($con,$insertbill);
    }

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="vendor.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_vendor_contact_details");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Vendor</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Vendor</h3>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2">Vendor Name<span class="error">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="vendor_name" id="vendor_name" maxlength="25" autocomplete="off" value="<?php echo $item['vendor_name']; ?>">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2">Address<span class="error">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="address" id="address" maxlength="250" autocomplete="off" rows="4"><?php echo $item['address']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="col-sm-2">Supplier Code<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="supplier_code" id="supplier_code" maxlength="50" autocomplete="off" value="<?php echo $item['supplier_code']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Opening Balance<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="balance" id="balance" maxlength="50" autocomplete="off" value="<?php echo $item['balance']; ?>">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="col-sm-2">Scope of Supply<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <textarea class="form-control" name="scope" id="scope" maxlength="250" autocomplete="off"><?php echo $item['scope']; ?></textarea>
                            </div>
                        </div>
                        <label class="col-sm-1">Contact Person<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="contact_person" id="contact_person" maxlength="50" autocomplete="off" value="<?php echo $item['contact_person']; ?>">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="col-sm-2">Business Type<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control selitemIcon" name="btype">
                                    <option value="">SELECT</option>
                                    <option value="service" <?php if($item['business_type']=='service'){ echo "selected=selected"; }?>>Service Business</option>
                                    <option value="corporation" <?php if($item['business_type']=='corporation'){ echo "selected=selected"; }?>>Corporation</option>
                                    <option value="merchandising" <?php if($item['business_type']=='merchandising'){ echo "selected=selected"; }?>>Merchandising Business</option>
                                    <option value="manufacturing" <?php if($item['business_type']=='manufacturing'){ echo "selected=selected"; }?>>Manufacturing Business</option>
                                    <option value="hybrid" <?php if($item['business_type']=='hybrid'){ echo "selected=selected"; }?>>Hybrid Business</option>
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-1">Phone<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="mobile" id="mobile" maxlength="50" autocomplete="off" value="<?php echo $item['mobile']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Email Id<span class="error">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" maxlength="30" autocomplete="off" value="<?php echo $item['email']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Gst No<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="gst" id="gst" maxlength="50" autocomplete="off" value="<?php echo $item['gst']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add More Contacts</button>
                            </div>
                        </div>
                    </div>
                    <div id="previous"></div>
                            
                            <div style="<?php if($id==''){echo "display:none";}?>">
                                <table class='table'>
                                   <thead>
                            		<tr>
                                		<th>Contact Name</th>
                                		<th>Contact Number</th>
                                		<th>Contact Email</th>
                                		<th>Department</th>
                                		<th>Land Line</th>
                            		</tr>
                            		</thead>
                            		<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){?>
                            		    <tr>
                            		        <td><?php echo $career[$i]['contact_name']; ?></td>
                            		        <td><?php echo $career[$i]['contact_mobile']; ?></td>
                            		        <td><?php echo $career[$i]['contact_email']; ?></td>
                            		        <td><?php echo $career[$i]['department']; ?></td>
                            		        <td><?php echo $career[$i]['contact_landline']; ?></td>
                            		    </tr>
                            		    <?php } ?>
                            		</tbody>
                        		</table>
                            </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="vendor.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
            <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Contacts</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-4">
                    <label>Contact Name</label>
                    <input type="text" class="form-control" name="cname" id="cname" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Department</label>
                    <input type="text" class="form-control" name="department" id="department" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Land Line Number</label>
                    <input type="text" class="form-control" name="lnum" id="lnum" autocomplete="off" maxlength="10">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <label>Contact Mobile Number</label>
                    <input type="text" class="form-control" name="cnum" id="cnum" autocomplete="off" maxlength="10">
                </div>
                <div class="col-sm-6">
                    <label>Contact Email</label>
                    <input type="text" class="form-control" name="cmail" id="cmail" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            vendor_name : "required",
            address:"required",
            supplier_code:"required",
            balance : "required",
            scope : "required",
            contact_person : "required",
            btype : "required",
            email : "required",
            gst : "required",
            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            
            vendor_name :"<span>Enter Vendor name</span>",
            address:"<span>Enter Address</span>",
            supplier_code:"<span>Enter supplier code</span>",
            balance:"<span>Enter balance in rupees</span>",
            email:"<span>Enter Valid Email Id</span>",
            scope : "<span>enter scope of supply</span>",
            contact_person : "<span>enter name of contact person</span>",
            btype : "<span>Select business type</span>",
            gst : "<span>Enter GST Number</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    $('#address').keyup(function () {
       var new_stuff = $(this).val();
           new_stuff = new_stuff.replace(/[\n\r]+/g,""); // clean out newlines, so we dont get dups!
       var test = new_stuff.replace(/(.{63})/g,"$1\n");
       $(this).val(test);
    });
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var cname = $("#cname").val();
    if(cname == ''){
        alert('Enter Contact Person Name');
        return false;
    }
    var cnum = $("#cnum").val();
    if(cnum == ''){
        alert('Enter Contact Number');
        return false;
    }
    var cmail = $("#cmail").val();
    if(cmail == ''){
        alert('Enter Contact Email');
        return false;
    }
    var dept = $("#department").val();
    if(dept == ''){
        alert('Enter Department Name');
        return false;
    }
    var lnum = $("#lnum").val();
    
      $.ajax({

        url: 'add_vendor_contact.php',
        data:{

          'sid': sid,
          'cname': cname,
           'cnum': cnum,
            'cmail': cmail,
            'dept': dept,
            'lnum': lnum,
        },
        success: function(result){
        $("#previous").html(result);
        $('#cname').val('');
        $('#cnum').val('');
        $('#cmail').val('');
        $('#department').val('');
        $('#lnum').val('');
      }
        });
      });
    </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>