<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from office_expense where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{
    $date = date("Y-m-d", strtotime($_POST['date']));
    $name = $_POST['id_expense_type'];
    $amount = $_POST['amount'];
    $remark = $_POST['remark'];
    
    $bill = $_FILES['bill_attach']['name'];
    $temp = $_FILES['bill_attach']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$bill);

    $sql = "insert into office_expense(id_expense_type, date, amount, remark, bill_attach) values('$name', '$date', '$amount', '$remark', '$bill') ";
    $con->query($sql) or die(mysqli_error($con));
      

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: office_expense.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];
    
    $date = date("Y-m-d", strtotime($_POST['date']));
    $name = $_POST['id_expense_type'];
    $amount = $_POST['amount'];
    $remark = $_POST['remark'];
    
    $bill = $_FILES['bill_attach']['name'];
    if($bill == ""){
        $bill = $item['bill_attach'];
    }
    else{
        $bill = $_FILES['bill_attach']['name'];
        $temp = $_FILES['bill_attach']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$bill);
    }

  $updatequery = "update office_expense set date = '$date', id_expense_type = '$name', amount='$amount', remark='$remark', bill_attach='$bill' where id = $id";
  
    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Updated successfully")</script>';
        header("location: office_expense.php");
        
    }
    header("location: office_expense.php");
}

$sql = "SELECT id, type_name FROM expense_type";
$result = $con->query($sql);
$expenseTypeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($expenseTypeList, $row);
  }


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Office Expense</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Office Expense</h3>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Expense Type</label>
                                <select name="id_expense_type" class="form-control selitemIcon">
                                    <option value="">SELECT</option>
                                    <?php
                                    for($i=0; $i<count($expenseTypeList); $i++){?>
                                    <option value="<?php echo $expenseTypeList[$i]['id']; ?>" <?php if($expenseTypeList[$i]['id']==$item['id_expense_type']){ echo "selected"; } ?>><?php echo $expenseTypeList[$i]['type_name']; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Date</label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($item['date'])) {echo $item['date'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" value="<?php if (!empty($item['amount'])) {echo $item['amount'];}?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Remark</label>
                                <input type="text" class="form-control" name="remark" value="<?php if (!empty($item['remark'])) {echo $item['remark'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Bill Without Commercial Attach</label>
                                <input type="file" class="form-control" name="bill_attach" value="<?php if (!empty($item['bill_attach'])) {echo $item['bill_attach'];}?>">
                            </div>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="office_expense.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_bank:"required",
                id_transaction_type:"required",
                amount : "required",
                date : "required",
                hsn_code : "required"
            },
            messages:{
                id_bank:"<span>*Select Bank Name</span>",
                id_transaction_type:"<span>*Select Transaction Type</span>",
                amount:"<span>*Enter Amount</span>",
                date:"<span>*Select Date</span>",
                hsn_code:"<span>*Enter HSN Code</span>"
            },
        })
    })
</script>
<script>
    function getGST(){
        var org_cost= $("#orgrate").val();
        var n_cost= $("#rate").val();
        var tot = (((n_cost - org_cost) *  100) / org_cost);
        var value = tot.toFixed(2);
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
    }
    
    var org_cost= $("#orgrate").val();
        var n_cost= $("#rate").val();
        var tot = (((n_cost - org_cost) *  100) / org_cost);
        $('#cgst_rate').val(tot +' %');
        $('#sgst_rate').val(tot +' %');
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>