<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from salary where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{


    $year = $_POST['year'];
    $month =$_POST['month'];
    $store = $_POST['store'];
    $employee = $_POST['employee'];
    $workingDays = $_POST['working_days'];
    $leaveBalance = $_POST['leave_balance'];
    $basicSalary = $_POST['basic_salary'];
    $deduction = $_POST['deduction'];
    $totalSalary = $_POST['total_salary'];
    $salaryPaid = $_POST['salary_paid'];
          
    $sql="INSERT INTO salary(year, month, store, employee_id, working_days, leave_balance, basic_salary, deduction, total_salary, salary_paid) VALUES('$year', '$month','$store','$employee','$workingDays','$leaveBalance','$basicSalary','$deduction','$totalSalary','$salaryPaid')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: salary_list.php");
}

if (isset($_POST['update']))
{

    $year = $_POST['year'];
    $month =$_POST['month'];
    $store = $_POST['store'];
    $employee = $_POST['employee'];
    $workingDays = $_POST['working_days'];
    $leaveBalance = $_POST['leave_balance'];
    $basicSalary = $_POST['basic_salary'];
    $deduction = $_POST['deduction'];
    $totalSalary = $_POST['total_salary'];
    $salaryPaid = $_POST['salary_paid'];
    
    $id  = $item['id'];
    $updatequery = "update salary set year = '$year', month = '$month', store='$store', employee_id='$employee', working_days='$workingDays', leave_balance='$leaveBalance', basic_salary='$basicSalary', deduction='$deduction', total_salary='$totalSalary', salary_paid='$salaryPaid' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="salary_list.php"</script>';
}

$sql = "SELECT id, company_name FROM company";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, employee_id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Salary</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Salary</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Year <span class="error">*</span></label>
                                <input type="text" class="form-control" name="year" id="year" maxlength="4" autocomplete="off" value="<?php echo $item['year']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Month <span class="error">*</span></label>
                                <input type="text" class="form-control" name="month" id="month" maxlength="20" autocomplete="off" value="<?php echo $item['month']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Store <span class="error">*</span></label>
                                <input type="text" class="form-control" name="store" id="store" maxlength="20" autocomplete="off" value="<?php echo $item['store']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Employee <span class="error">*</span></label>
                                <select name="employee" class="form-control selitemIcon">
                                <option value=""> --SELECT-- </option>
                                <?php
                                for($i=0; $i<count($employeeList); $i++){
                                    ?>
                                    <option value="<?php echo $employeeList[$i]['id']; ?>" <?php if($employeeList[$i]['id']==$item['employee_id']){ echo "selected";} ?>><?php echo $employeeList[$i]['employee_name']; ?></option>
                                    <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Working Days<span class="error">*</span></label>
                                <input type="text" class="form-control" name="working_days" id="working_days" maxlength="20" autocomplete="off" value="<?php echo $item['working_days']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Leave Balance<span class="error">*</span></label>
                                <input type="text" class="form-control" name="leave_balance" id="leave_balance" maxlength="50" autocomplete="off" value="<?php echo $item['leave_balance']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Basic Salary<span class="error">*</span></label>
                                <input type="text" class="form-control" name="basic_salary" id="basic_salary" maxlength="50" autocomplete="off" value="<?php echo $item['basic_salary']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Any Deduction<span class="error">*</span></label>
                                <input type="text" class="form-control" name="deduction" id="deduction" maxlength="50" autocomplete="off" value="<?php echo $item['deduction']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Total Salary<span class="error">*</span></label>
                                <input type="text" class="form-control" name="total_salary" id="total_salary" autocomplete="off" value="<?php echo $item['total_salary']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Salary Paid <span class="error">*</span></label>
                                <input type="text" class="form-control" name="salary_paid" id="salary_paid" maxlength="50" autocomplete="off" value="<?php echo $item['salary_paid']; ?>">
                            </div>
                        </div>
                        
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="salary_list.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            year : "required",
            month : "required",
            store:"required",
            employee:"required",
            working_days : "required",
            leave_balance : "required",
            basic_salary : "required",
            deduction : "required",
            total_salary : "required",
            salary_paid : "required"
        },
        messages:{
            year : "<span>Enter Year</span>",
            month : "<span>Enter Month</span>",
            store:"<span>Enter Store</span>",
            employee:"<span>Select Employee</span>",
            working_days:"<span>Enter Working Days</span>",
            leave_balance:"<span>Enter Leave Balance</span>",
            basic_salary : "<span>Enter Basic Salary</span>",
            deduction : "<span>Enter Any Deduction</span>",
            total_salary : "<span>Enter Total Salary</span>",
            salary_paid : "<span>Enter Salary Paid</span>"
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>