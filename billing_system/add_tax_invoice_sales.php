<?php
include 'connection.php';

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from tax_invoice_sales where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$sql      = "select * from tax_invoice_sales";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currYear."-".$nxtYear;

if (isset($_POST['save']))
{
    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $poNoId = $_POST['po_no'];
    $total = $_POST['total'];
    $gstRate = $_POST['rate'];
    $gstType = $_POST['gst_type'];
    $sgst = $_POST['sgst_rate'];
    $cgst = $_POST['cgst_rate'];
    $igst = $_POST['igst_rate'];
    $totalAmount = $_POST['totalinclude'];
    $payStatus = "PENDING"."<br>"."($totalAmount)";
    $balance = $_POST['totalinclude'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];

   $sql="INSERT INTO tax_invoice_sales(invoice_no, date, id_customer, po_num_id, total_amount, balance, gst_rate, gst_no, sgst_rate, cgst_rate, igst_rate, tax_include_amount, payment_status, remarks, days_no, attachment) VALUES('$invoiceNo', '$date', '$customer_name', '$poNoId ', '$total', '$balance', '$gstRate', '$gstType', '$sgst', '$cgst', '$igst', '$totalAmount', '$payStatus', '$remarks', '$days', '$file')";
    
    $result = $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    $updatequery = "update purchase_order_inwards set tax_invoice_generated = 'Yes' where id = $last_id";

    $res=$con->query($updatequery);

    header("location: tax_invoice_sales.php");
}

// if (isset($_POST['generate']))
// {
// $poNoId = $_POST['po_no'];
//         echo "<script>parent.location='tax_invoice_sales_invoice.php?id=$poNoId'</script>";
// }

if (isset($_POST['update']))
{

    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $invValue = $_POST['invoice_value'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    if($file == ""){
        $file = $item['attachment'];
    }
    else{
        $file = $_FILES['attachment']['name'];
        $temp = $_FILES['attachment']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];
    
    $id  = $item['id'];
    $updatequery = "update tax_invoice_sales set id_customer = '$customer_name', invoice_no='$invoiceNo', invoice_value='$invValue', attachment='$file', remarks='$remarks', date='$date', days_no='$days' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="tax_invoice_sales.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Tax Invoice Sales </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
    
    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Tax Invoice Sale</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Invoice Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="invoice_no" id="invoice_no" autocomplete="off" value="<?php if($item['invoice_no']) { echo $item['invoice_no']; } else { echo $serialNo; } ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Date <span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" autocomplete="off" value="<?php echo $item['date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer Name<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="id_customer" id="id_customer" onchange="getpoNo()">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($companyList); $i++){?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['id_customer']){ echo "selected=selected"; } ?>><?php echo $companyList[$i]['customer_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO NO<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="po_no" id="po_no" onchange="getPoItems()">
                                    <option value="">SELECT</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No of Days<span class="error"></span></label>
                                <input type="text" class="form-control" name="days" id="days" autocomplete="off" value="<?php echo $item['days_no']; ?>" maxlength="3">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error"></span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Attachment<span class="error"></span></label>
                                <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off">
                            </div>
                            <br>
                            <?php if(!empty($item['attachment'])){?>
                            <img src="uploads/<?php echo $item['attachment']; ?>" width="100px" height="100px">
                            <?php } ?>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
                <br>
                <div id="po_items"></div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="tax_invoice_sales.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
  <script type="text/javascript">
        function getpoNo(){
          var id = $("#id_customer").val();
          console.log(id);

          $.ajax({url: "get_po_number.php?id="+id, success: function(result){
            $("#po_no").html(result);
          }
        });
        }
        
        function getPoItems(){
            var poid = $("#po_no").val();
            $.ajax({url: "get_po_items.php?id="+poid, success: function(result){
            $("#po_items").html(result);
          }
        });
        }
        
    
    function getGST(){
        var org_cost= parseInt($('#total').val());
        var per= $("#rate").val();
        var pernum = (org_cost * 0.01 * (100 - per));
        var gstnum = (org_cost - pernum);
        var totnum = org_cost + gstnum;
        
        var tot = (((totnum - org_cost) * 100) / org_cost);
        var value = tot.toFixed(2)/2;
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
        $('#igst_rate').val(per +' %');
        $('#totalinclude').val(totnum.toFixed(2));
    }
    
    function getGstRate1(){
        var type = $("#gst_type1").val();
        if(type == 'local'){
            $(".local").show();
            $("#inter").hide();
        }
    }
    
    function getGstRate2(){
        var type = $("#gst_type2").val();
        if(type == 'inter-state')
        {
            $(".local").hide();
            $("#inter").show();
        }
    }
    
    
    </script>
  <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_customer:"required",
                remarks:"required",
                date : "required",
                rate : "required",
                po_no : "required"
            },
            messages:{
                id_customer:"<span>Select customer Name</span>",
                remarks:"<span>Enter Remarks</span>",
                date:"<span>select Date</span>",
                rate:"<span>Select GST Rate</span>",
                po_no:"<span>select PO NO</span>"
            },
        });
    });
</script>

<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
</script>
</html>