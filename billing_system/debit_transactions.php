<?php

include 'connection.php';

$viewquery = "Select a.*, b.bank_name, b.account_name, c.vendor_name, c.address, c.mobile, d.type_name from debit_transaction a INNER JOIN vendor c ON a.id_vendor=c.id INNER JOIN banks b ON a.id_bank=b.id INNER JOIN transaction_type as d ON a.id_transaction_type=d.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['vendor_name'] = $row['vendor_name']."<br>".$row['address']."<br>".$row['mobile'];
  $career[$i]['bank_name'] = $row['bank_name']."<br>".$row['account_name'];
  $career[$i]['transaction_type'] = $row['type_name'];
  $career[$i]['paid'] = $row['paid'];
  $career[$i]['remarks'] = $row['remarks'];
  $career[$i]['value'] = $row['value'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transaction</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        // parent.location='delete_purchase_bill.php?pbill_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Payments</h3>
                     <a href="add_debit_transactions.php" class="btn btn-primary">+ Create Payment</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Transaction Type</th>
                                <th>Vendor Name</th>
                                <th>Date</th>
                                <th>Value</th>
                          <th>Remarks</th>
                          <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['bank_name']; ?></td>
                          <td><?php echo $career[$i]['transaction_type']; ?></td>
                          <td><?php echo $career[$i]['vendor_name']; ?></td>
                          <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['value']; ?></td>
                          <td><?php echo $career[$i]['remarks']; ?></td>
                          <td><a href="add_debit_transactions.php?id=<?php echo $id; ?>" title="EDIT"><i class="fa fa-edit fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>