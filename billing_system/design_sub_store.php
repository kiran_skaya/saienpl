<?php
include 'connection.php';
session_start();
$sid = session_id();


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from sub_store where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

 $viewquery = "SELECT a.*, b.name as item, c.name as category, sc.name as subcat FROM design_sub_store as a INNER JOIN item as b ON a.id_item=b.id INNER JOIN category as c ON a.id_category=c.id INNER JOIN sub_category as sc ON a.id_sub_category=sc.id WHERE a.id_sub_store ='$id' order by a.rack_alloted_for_item, a.self_alloted_for_item asc ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['item'] = $row['item'];
      $career[$i]['category'] = $row['category'];
      $career[$i]['subcat'] = $row['subcat'];
      $career[$i]['rack_no'] = $row['rack_alloted_for_item'];
      $career[$i]['self_no'] = $row['self_alloted_for_item'];
      $career[$i]['quantity'] = $row['no_items_self'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }


if (isset($_POST['update']))
{
//     $tempsql = "SELECT * FROM temp_design_sub_store WHERE id_sub_store='$id' ";
//     $tempexec = mysqli_query($con, $tempsql);
//   $i=0;
//   while ($row = mysqli_fetch_assoc($tempexec)) {
//     $fetch[$i]['id_category'] = $row['id_category'];
//     $fetch[$i]['id_sub_category'] = $row['id_sub_category'];
//     $fetch[$i]['id_item'] = $row['id_item'];
//     $fetch[$i]['rack_alloted_for_item'] = $row['rack_alloted_for_item'];
//     $fetch[$i]['self_alloted_for_item'] = $row['self_alloted_for_item'];
//     $fetch[$i]['no_items_self'] = $row['no_items_self'];
//     $i++;
//   }
//   for($i=0; $i<count($fetch); $i++){
//     $id_category = $fetch[$i]['id_category'];
//     $id_sub_category = $fetch[$i]['id_sub_category'];
//     $id_item = $fetch[$i]['id_item'];
//     $rnum = $fetch[$i]['rack_alloted_for_item'];
//     $snum = $fetch[$i]['self_alloted_for_item'];
//     $num = $fetch[$i]['no_items_self'];

//     $insertbill = "INSERT INTO design_sub_store(id_sub_store, id_category, id_sub_category, id_item, rack_alloted_for_item, self_alloted_for_item, no_items_self) VALUES ('$id', '$id_category', '$id_sub_category', '$id_item', '$rnum', '$snum', '$num')";

//     $result = mysqli_query($con,$insertbill);
//     }
    
        // echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="design_sub_store_list.php"</script>';
}

$sql = "SELECT id, name FROM main_store";
$result = $con->query($sql);
$mainstoreList = array();
while ($row = $result->fetch_assoc()) {
    array_push($mainstoreList, $row);
  }

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_design_sub_store");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sub Store</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    #name{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>

                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Design Sub Store</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Main Store<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="main" id="main" disabled>
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($mainstoreList); $i++){ ?>
                                    <option value="<?php echo $mainstoreList[$i]['id']; ?>" <?php if($mainstoreList[$i]['id']==$item['id_main_store']){echo "selected=selected";} ?>><?php echo strtoupper($mainstoreList[$i]['name']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sub Store Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" maxlength="50" autocomplete="off" value="<?php echo $item['name']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sub Store handling Person Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="person_name" id="person_name" maxlength="50" autocomplete="off" value="<?php echo $item['person_name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Contact Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="contact_no" id="contact_no" autocomplete="off" value="<?php echo $item['contact_no']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Racks allotted for sub store<span class="error">*</span></label>
                                <input type="text" class="form-control" name="total_racks" id="total_racks" maxlength="50" autocomplete="off" value="<?php echo $item['total_racks']; ?>" onkeyup="getRackSelect()" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Number of Selfs per Rack<span class="error">*</span></label>
                                <input type="text" class="form-control" name="selfs_per_rack" id="selfs_per_rack" maxlength="50" autocomplete="off" value="<?php echo $item['selfs_per_rack']; ?>" onkeyup="getSelfSelect()" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Items To Store</button>
                            </div>
                        </div>
                    </div>
                    <div id="previous"></div>
                    <div>
                        <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                            <th>Rack No</th>
                          <th>Self No</th>
                          <th>Category</th>
                    		<th>Sub Category</th>
                          <th>Item Name</th>
                          <th>Max Qnty</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {?>
                        <tr>
                            <td><?php echo $career[$i]['rack_no']; ?></td>
                          <td><?php echo $career[$i]['self_no']; ?></td>
                          <td><?php echo $career[$i]['category']; ?></td>
                          <td><?php echo $career[$i]['subcat']; ?></td>
                          <td><?php echo $career[$i]['item']; ?></td>
                          <td><?php echo $career[$i]['quantity']; ?></td>
                          <td><a href="delete_substore_item.php?id=<?php echo $career[$i]['id']; ?>&sid=<?php echo $id; ?>">DELETE</a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="sub_store.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Rack Alloted For Item<span class="error">*</span></label>
                            <select class="form-control selitemIcon" name="rack_alloted_for_item" id="rack_alloted_for_item" style="width: 260px;">
                                <option value=''>select</option>
                                    
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Self Alloted For Item<span class="error">*</span></label>
                            <select class="form-control selitemIcon" name="self_alloted_for_item" id="self_alloted_for_item" style="width: 260px;">
                                <option value="">select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Max Quantity<span class="error">*</span></label>
                            <input type="text" class="form-control" name="no_items_self" id="no_items_self" maxlength="50" autocomplete="off" >
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            name : "required",
            main:"required",
            person_name:"required",
            total_racks : "required",
            no_items_self : "required",
            selfs_per_rack : "required",
            id_category : "required",
            id_subcategory : "required",
            id_item : "required",
            rack_alloted_for_item : "required",
            self_alloted_for_item : "required",

            contact_no: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            name : "<span>Enter Sub store name</span>",
            main:"<span>Select main store</span>",
            person_name:"<span>Enter Person Name</span>",
            total_racks:"<span>Enter Total Number of Racks</span>",
            no_items_self:"<span>Enter Number of Items</span>",
            selfs_per_rack : "<span>Enter Number of Selfs</span>",
            id_category : "<span>Select Category</span>",
            id_subcategory : "<span>Select Sub Category</span>",
            id_item : "<span>Select Item </span>",
            rack_alloted_for_item : "<span>Select Rack </span>",
            self_alloted_for_item : "<span>Select Self </span>",
           contact_no:
           {
            required:"<span>Enter Contact Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
        function getSubcategory(){
          var id = $("#id_category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }
        
        function getRackSelect(){
          var value = $("#total_racks").val();
          console.log(value);

          $.ajax({url: "get_dropdown.php?value="+value, success: function(result){
            $("#rack_for_item").html(result);
          }
        });
        }
        
        function getSelfSelect(){
          var value = $("#selfs_per_rack").val();
          console.log(value);

          $.ajax({url: "get_dropdown2.php?value="+value, success: function(result){
            $("#self_for_item").html(result);
          }
        });
        }
        
        var value = $("#total_racks").val();
          console.log(value);

          $.ajax({url: "get_dropdown.php?value="+value, success: function(result){
            $("#rack_alloted_for_item").html(result);
          }
        });
        
        var value = $("#selfs_per_rack").val();
          console.log(value);

          $.ajax({url: "get_dropdown2.php?value="+value, success: function(result){
            $("#self_alloted_for_item").html(result);
          }
        });
    </script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var rnum = $("#rack_alloted_for_item").val();
    if(rnum == ""){
        alert('Enter Rack Number');
        return false;
    }
    var snum = $("#self_alloted_for_item").val();
    if(snum == ""){
        alert('Enter Self Number');
        return false;
    }
    
    var no_items_self = $("#no_items_self").val();
    if(no_items_self == ""){
        alert('Enter Max Quantity');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_sub_store.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'rnum': rnum,
            'snum': snum,
            'qty': no_items_self,
        },
        success: function(result){
            $("#previous").html(result);
        location.reload();
        
      }
        });
      });
    </script>
    <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
          var sid = '<?php echo $id; ?>';
          var option = 'delete';
      $.ajax({

        url: 'add_items_to_sub_store.php',
        data:{

          'sid': sid,
          'option': option,
           'id': id,
        },
        success: function(result){
        $("#previous").html(result);
      }
        });
      }
    }
  </script>

</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>