<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from employee where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$sql = "select * from employee";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.03d", $resnum);

if (isset($_POST['save']))
{

    $employeeId = $_POST['employee_id'];
    $mainStore = $_POST['main_store'];
    $employeeName =$_POST['employee_name'];
    $gender = $_POST['gender'];
    $designation = $_POST['designation'];
    $gaurdian = $_POST['gaurdian'];
    $department = $_POST['department'];
    $pfCode = $_POST['pf_code'];
    $esiCode = $_POST['esi_code'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $company = $_POST['id_company'];
    
    $salary = $_POST['salary'];
    $total_leave = $_POST['total_leave'];
    $password = $_POST['password'];
    
    
   
   $sql1 = "SELECT * FROM employee WHERE email = '$email' AND password='$password'";
      $result1  = $con->query($sql1);
      $resnum = mysqli_num_rows($result1);
      if($resnum < 1){
          
    $sql="INSERT INTO employee(employee_id, id_main_store, company_id, employee_name, gender, designation, gaurdian, id_department, pf_code, esi_code, address, phone, email, password, salary, total_leave) VALUES('$employeeId', '$mainStore', '$company','$employeeName','$gender','$designation','$gaurdian','$department','$pfCode','$esiCode','$address','$phone', '$email', '$password', '$salary', '$total_leave')";
    $con->query($sql) or die(mysqli_error($con));
      }
      else{
          echo "<script>alert('Employee Already Exists')</script>";
      }

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: employee.php");
}

if (isset($_POST['update']))
{
    if ($_POST['employee_id']=="") {
    }
    else{
    $employeeId = $_POST['employee_id'];
}
    $mainStore = $_POST['main_store'];
    $employeeName =$_POST['employee_name'];
    $gender = $_POST['gender'];
    $designation = $_POST['designation'];
    $gaurdian = $_POST['gaurdian'];
    $department = $_POST['department'];
    $pfCode = $_POST['pf_code'];
    $esiCode = $_POST['esi_code'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $company = $_POST['id_company'];
    $salary = $_POST['salary'];
    $total_leave = $_POST['total_leave'];
    $password = $_POST['password'];
    
    $id  = $item['id'];
    $updatequery = "update employee set id_main_store='$mainStore', employee_id = '$employeeId', employee_name = '$employeeName', company_id='$company', gender='$gender', designation='$designation', gaurdian='$gaurdian', id_department='$department', pf_code='$pfCode', esi_code='$esiCode', address='$address', phone='$phone', email='$email', password='$password', salary='$salary', total_leave='$total_leave' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="employee.php"</script>';
}

$sql = "SELECT id, company_name FROM company";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, name FROM department";
$result = $con->query($sql);
$departmentList = array();
while ($row = $result->fetch_assoc()) {
    array_push($departmentList, $row);
  }

$sql = "SELECT id, name FROM main_store";
$result = $con->query($sql);
$mainStoreList = array();
while ($row = $result->fetch_assoc()) {
    array_push($mainStoreList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Employee</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Employee</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Main Store<span class="error">*</span></label>
                                <select name="main_store" id="main_store" class="form-control selitemIcon" onchange="getMainStoreName()">
                                <option value=""> --SELECT-- </option>
                                <?php
                                for($i=0; $i<count($mainStoreList); $i++){
                                    ?>
                                    <option value="<?php echo $mainStoreList[$i]['id']; ?>" <?php if($mainStoreList[$i]['id']==$item['id_main_store']){ echo "selected";} ?>><?php echo $mainStoreList[$i]['name']; ?></option>
                                    <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Employee Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="employee_name" id="employee_name" maxlength="50" autocomplete="off" value="<?php echo $item['employee_name']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Gender<span class="error">*</span></label>
                                <select name="gender" class="form-control selitemIcon">
                                    <option value="">Select</option>
                                    <option value="Male" <?php if($item['gender']=='Male'){echo "selected=selected"; }?>>MALE</option>
                                    <option value="Female" <?php if($item['gender']=='Female'){echo "selected=selected"; }?>>FEMALE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Designation<span class="error">*</span></label>
                                <input type="text" class="form-control" name="designation" id="designation" maxlength="50" autocomplete="off" value="<?php echo $item['designation']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Father/Husband Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="gaurdian" id="gaurdian" maxlength="50" autocomplete="off" value="<?php echo $item['gaurdian']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Department<span class="error">*</span></label>
                                <select name="department" class="form-control selitemIcon">
                                <option value=""> --SELECT-- </option>
                                <?php
                                for($i=0; $i<count($departmentList); $i++){
                                    ?>
                                    <option value="<?php echo $departmentList[$i]['id']; ?>" <?php if($departmentList[$i]['id']==$item['id_department']){ echo "selected";} ?>><?php echo $departmentList[$i]['name']; ?></option>
                                    <?php } ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PF Code No<span class="error">*</span></label>
                                <input type="text" class="form-control" name="pf_code" id="pf_code" maxlength="50" autocomplete="off" value="<?php echo $item['pf_code']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>ESI Code No<span class="error">*</span></label>
                                <input type="text" class="form-control" name="esi_code" id="esi_code" maxlength="50" autocomplete="off" value="<?php echo $item['esi_code']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Address<span class="error">*</span></label>
                                <input type="text" class="form-control" name="address" id="address" autocomplete="off" value="<?php echo $item['address']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="phone" id="phone" maxlength="50" autocomplete="off" value="<?php echo $item['phone']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" autocomplete="off" value="<?php echo $item['email']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Password<span class="error">*</span></label>
                                <input type="password" class="form-control" name="password" id="password" autocomplete="off" value="<?php echo $item['password']; ?>">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-4">

                            <label>Company Name <span class="text-danger"> *</span></label>
                            <select name="id_company" class="form-control selitemIcon">
                                <option value=""> --SELECT-- </option>
                                <?php
                                for($i=0; $i<count($companyList); $i++){
                                    ?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['company_id']){ echo "selected";} ?>><?php echo $companyList[$i]['company_name']; ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Salary<span class="error">*</span></label>
                                <input type="text" class="form-control" name="salary" id="salary" autocomplete="off" value="<?php echo $item['salary']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Total Leave<span class="error">*</span></label>
                                <input type="text" class="form-control" name="total_leave" id="total_leave" autocomplete="off" value="<?php echo $item['total_leave']; ?>">
                            </div>
                        </div>
                        <div id="employeeId"></div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="employee.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>

    <script type="text/javascript">
        function getMainStoreName(){
            var id = $("#main_store").val();
        var no = '<?php echo $ref; ?>';

                $.ajax({url: "get_employee_id.php?id="+id+"&no="+no, 
                    success: function(result){
                    $("#employeeId").html(result);
              }
            });
        }
    </script>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            employee_id : "required",
            employee_name : "required",
            gender:"required",
            designation:"required",
            gaurdian : "required",
            department : "required",
            pf_code : "required",
            esi_code : "required",
            address : "required",
            email : "required",
            password: "required",
            id_company : "required",

            phone: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            employee_id : "<span>Enter Employee Id</span>",
            employee_name : "<span>Enter Employee name</span>",
            gender:"<span>Select Gender</span>",
            designation:"<span>Enter designation</span>",
            gaurdian:"<span>Enter Father/Husband Name</span>",
            department:"<span>Select Department</span>",
            pf_code : "<span>enter PF code number</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
            password : "<span>Enter Password</span>",
            id_company : "<span>Select Company Name</span>",
           phone:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>