<?php

include('connection.php');

if($_POST)
{
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "Select * from employee where email='$email' and password='$password'";
    $result  = $con->query($sql);
    $loginList = array();
    while ($row = $result->fetch_assoc())
    {
        $_SESSION['employee'] = $row;
        array_push($loginList, $row);
    }


        if(empty($loginList))
        {
            echo "<script>alert('Please enter the valid username and password');</script>";
            echo "<script>parent.location='login.php'</script>";
            exit;
        }
        else
        {
            echo "<script>parent.location='edit_profile.php'</script>";
            exit;
        }
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#form').validate({
                rules:{
                    email:"required",
                    password:"required"
                },
                messages:{
                    email:"<span>Enter email</span>",
                    password:"<span>Enter password</span>"
                },
            })
        })
    </script>

    <style type="text/css">
        .error{
            color: #a94442;
        }
    </style>
    
</head>

<body>     
    <div class="login-container">
    <form method="POST" action="" id="form" >
        <h3 class="login-title">Login</h3>
        <div class="form-group">
            <label>Email Address<span class="error">*</span> </label>
            <input type="text" id="email" name="email" class="form-control" title="Enter Email Id">
        </div> 
        <div class="form-group">
            <label>Password<span class="error">*</span></label>
            <input type="password" id="password" name="password" class="form-control" title="Enter Password">
        </div>
        
        <button type="submit" name="submit" class="btn btn-primary btn-block">Login</button>                       
        <div class="login-links">            
            <hr />
            <p>Don't have account? <a href="#">Register</a></p>
        </div>
        </form>
    </div>
<!--     Placed at the end of the document so the pages load faster
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>     -->
</body>

</html>