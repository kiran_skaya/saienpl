<?php
function numberTowords($num)
{

$ones = array(
0 =>"ZERO",
1 => "ONE",
2 => "TWO",
3 => "THREE",
4 => "FOUR",
5 => "FIVE",
6 => "SIX",
7 => "SEVEN",
8 => "EIGHT",
9 => "NINE",
10 => "TEN",
11 => "ELEVEN",
12 => "TWELVE",
13 => "THIRTEEN",
14 => "FOURTEEN",
15 => "FIFTEEN",
16 => "SIXTEEN",
17 => "SEVENTEEN",
18 => "EIGHTEEN",
19 => "NINETEEN",
"014" => "FOURTEEN"
);
$tens = array( 
0 => "ZERO",
1 => "TEN",
2 => "TWENTY",
3 => "THIRTY", 
4 => "FORTY", 
5 => "FIFTY", 
6 => "SIXTY", 
7 => "SEVENTY", 
8 => "EIGHTY", 
9 => "NINETY" 
); 
$hundreds = array( 
"HUNDRED", 
"THOUSAND", 
"MILLION", 
"BILLION", 
"TRILLION", 
"QUARDRILLION" 
); /*limit t quadrillion */
$num = number_format($num,2,".",","); 
$num_arr = explode(".",$num); 
$wholenum = $num_arr[0]; 
$decnum = $num_arr[1]; 
$whole_arr = array_reverse(explode(",",$wholenum)); 
krsort($whole_arr,1); 
$rettxt = ""; 
foreach($whole_arr as $key => $i){
  
while(substr($i,0,1)=="0")
    $i=substr($i,1,5);
if($i < 20){ 
/* echo "getting:".$i; */
$rettxt .= $ones[$i]; 
}elseif($i < 100){ 
if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
}else{ 
if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
} 
if($key > 0){ 
$rettxt .= " ".$hundreds[$key]." "; 
}
} 
if($decnum > 0){
$rettxt .= " and ";
if($decnum < 20){
$rettxt .= $ones[$decnum];
}elseif($decnum < 100){
$rettxt .= $tens[substr($decnum,0,1)];
$rettxt .= " ".$ones[substr($decnum,1,1)];
}
}
return $rettxt;
}

?>
<?php
include('connection.php');
date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d/m/Y');

$id = $_GET['id'];
$sql = "SELECT a.*, b.customer_name, b.address, b.mobile, b.gst_no FROM purchase_order_inwards as a INNER JOIN customer as b ON a.id_customer=b.id where a.id='$id' ";
$result = $con->query($sql) or die($con->error);
while ($row = mysqli_fetch_array($result))
{
	$vendor_name = $row['customer_name'];
	$address = wordwrap($row['address'], 35, "<br>", true);
  $phone = $row['mobile'];
	$gstin = $row['gst_no'];
	$poNo = "PI".$row['customer_po_no'];
	$refPoNo = $row['customer_po_no'];
  $poDate = date("d/m/Y", strtotime($row['po_date']));
  $poInvDate = date("d/m/Y");
	$email = $row['email'];
	$time = date("H:i:sa");
	$ino = rand();
}

$viewquery = "SELECT a.*, b.name, b.code, b.description, b.hsn_code, b.rate FROM po_inwards_items as a INNER JOIN item as b ON a.id_item=b.id where a.id_poin='$id'";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['code'] = $row['code'];
  $career[$i]['description'] = $row['description'];
  $career[$i]['unit_price']= $row['unit_price'];
  $career[$i]['quantity']= $row['quantity'];
  $career[$i]['total']= $row['total'];
  $career[$i]['hsn_code']= $row['hsn_code'];
  $career[$i]['discount']= $row['discount'];
  if($career[$i]['discount'] ==""){
        $career[$i]['discount'] = "0";
      }
  $career[$i]['rate']= $row['rate'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

$sql= "SELECT b.terms_condtion FROM purchase_order_inwards AS a INNER JOIN terms_conditions as b ON a.terms_name=b.id WHERE a.id='$id'";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $termsCondtion = $row['terms_condtion'];
}

$sql="SELECT sum(total) as totalAmount FROM po_inwards_items WHERE id_poin ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $totalAmount = $row['totalAmount'];
}

$sql="SELECT other_charges FROM po_inwards_items WHERE id_poout ='$id' AND other_charges >='0' ORDER BY id DESC LIMIT 0,1";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $otherCharges = $row['other_charges'];
  if ($otherCharges == "") {
    $otherCharges = "0.00";
  }
}

$totalPO = $totalAmount + $otherCharges;
  $amountInWords = ucwords(numberTowords($totalPO));

$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');
        

        $file_data = $file_data ."
		
		<table width='100%' border='1' style='border-collapse:collapse;'>
          <tr>
            <td style='text-align: left;' colspan='9'>
            	<b style='font-size: 36px; color: blue;'>SAI ENTERPRISES</b> <br>
              <i style='font-size: 12px'>(AN ISO 9001-2015 CERTIFIED COMPANY)</i> <br>
              #107, 1<sup>ST</sup> FLOOR, MEI COLONY, 
              LAGGERE MAIN ROAD,
              PEENYA INDUSTRIAL AREA(WA),
              BENGALURU- 560058. <br> <br>
              <b>GSTIN: 29AHRPA5654N1ZN</b>
            </td>
          </tr>
          <tr><td style='text-align: center; font-size:18px; background-color: #F4F4F4;' colspan='9'><b>PROFORMA INVOICE</b></td></tr>
          <tr><td style='font-size: 15px;' colspan='4'>Proforma Inv No. : $poNo </td> 
          <td style='text-align: right;' colspan='5'> 
            Proforma Inv Date : $poInvDate </td></tr>
          <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='9'><b>Details of Customer: </b></td></tr>
          <tr>
            <td style='text-align: left; font-size:16px;' colspan='4'><br> M/S. $vendor_name
            <br>
                $address
            <br>
            GSTIN : $gstin
            </td>
            <td style='text-align: left; font-size:16px;' colspan='5'><br> PO No: $refPoNo
            <br>
                PO Date : $poDate
            </td>
          </tr>
          <tr>
          <th style='background-color: #D5D3D3'>Sl. No</th>
          <th style='background-color: #D5D3D3'>Item Description</th>
          <th style='background-color: #D5D3D3'>HSN</th>
          <th style='background-color: #D5D3D3'>QTY</th>
          <th style='background-color: #D5D3D3'>UOM</th>
          <th style='background-color: #D5D3D3'>Unit Price</th>
          <th style='background-color: #D5D3D3'>Disc%</th>
          <th style='background-color: #D5D3D3'>Taxable Amount</th>
          <th style='background-color: #D5D3D3'>GST Rate</th>
          </tr>";

          for ($i=0; $i<count($career); $i++){

          $itemname = $career[$i]['name'];
          $itemcode = $career[$i]['code'];
          $itemdesc = $career[$i]['description'];
          $itemprice = $career[$i]['unit_price'];
          $itemqnty = $career[$i]['quantity'];
          $itemtotal = $career[$i]['total'];
          $hsncode = $career[$i]['hsn_code'];
          $discount = $career[$i]['discount'];
          $rate = $career[$i]['rate'];
          $no = $i+1;
       
         $file_data .="
         <tr>
         <td>$no</td>
         <td><b>$itemname</b> $itemdesc </td>
         <td>$hsncode</td>
         <td>$itemqnty</td>
         <td>Nos</td>
         <td>$itemprice</td>
         <td>$discount%</td>
         <td>$itemtotal</td>
         <td>$rate%</td>
         </tr>";
          }

        $file_data .="<tr><td colspan='7' style='text-align: right'> &emsp; Total : </td><td colspan='2'> $totalAmount </td></tr>
         <tr><td colspan='9'> &emsp; </td></tr>
         <tr><td colspan='6'>  </td><td colspan='2'>  Other Charges</td><td style='text-align: center'>  $otherCharges </td></tr>
         <tr><td colspan='6'> <b>Total PO Amount in Words:</b> <br> $amountInWords  </td><td colspan='2'>Total PO Value</td><td> $totalPO </td></tr>
         <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='9'><b>Back Account Details </b></td></tr>
         <tr><td colspan='9'>
         Bank Name: Canara Bank <br>
         Bank Branch: Vijaynagar <br>
         A/c No: 1146201007427 <br>
         A/c Type: Current a/c <br>
         Name: M/S. SAI ENTERPRISES <br>
         MICR Code: 560015062 <br>
         IFSC Code: CNRB 0001146 <br>

         </td></tr>
         <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='9'><b>Terms & Conditions </b></td></tr>
         <tr><td colspan='9'>
         $termsCondtion
         </td></tr>
         <tr><td style='text-align: right' colspan='9'><b>For SAI ENTERPRISES</b> <br> <br>  Authorised Signatory</td></tr>

      </table>";

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetFooter('<div style="text-align: center">This is Computer generated bill For SAI ENTERPRISES</div>');
$mpdf->WriteHTML($file_data);
$filename = "PO"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'I');
    echo "<script>parent.location='generate_po_inwards.php?id=$id'</script>";

exit;