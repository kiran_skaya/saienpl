<?php
include('connection.php');

date_default_timezone_set("Asia/Kolkata");
$date = date('d/m/Y');

$id = $_GET['id'];
$sql = "SELECT a.*, b.enq_no, c.customer_name, c.address,c.address2,c.city,c.zipcode, c.mobile, c.email FROM quotation as a INNER JOIN enquiry as b ON a.enquiry_id=b.id INNER JOIN customer as c ON a.customer_id=c.id where a.id='$id' ";
$result = $con->query($sql) or die($con->error);
while ($row = mysqli_fetch_array($result))
{
	$customer_Name = $row['customer_name'];
	$address = $row['address'];
	$address2 = $row['address2'];
	$city = $row['city'];
	$zipcode = $row['zipcode'];
	$phone = $row['mobile'];
	$enqNo = $row['enq_no'];
	$qtnNo = $row['qtn_no'];
	$email = $row['email'];
	$id_terms = $row['id_terms'];
		$project_name = $row['project_name'];
	$time = date("H:i:sa");
	$ino = rand();
}
if($id_terms!='') {

 $sql = "SELECT * FROM terms_conditions where id in ($id_terms)";
$result = $con->query($sql);
$quotationList = array();
while ($row = $result->fetch_assoc()) {
    array_push($quotationList, $row);
  }
}

 $viewquery = "SELECT a.*,b.name,b.hsn_code,b.code,b.description,b.technical_detail FROM quotation_has_items as a INNER JOIN item as b ON a.id_item=b.id where a.id_quotation='$id'";
$viewqueryresult = mysqli_query($con,$viewquery);
$career= array();
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['hsn_code'] = $row['hsn_code'];
  $career[$i]['code'] = $row['code'];
  $career[$i]['description'] = $row['description'];
  $career[$i]['technical_detail'] = $row['technical_detail'];
  $career[$i]['unit_price']= $row['unit_price'];
  $career[$i]['quantity']= $row['quantity'];
  $career[$i]['total']= $row['total'];
  $career[$i]['id'] = $row['id'];
  $i++;
}
$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');
        

        $file_data = $file_data ."
        <div width='100%' style='background-color: #3639a4; height: 100px; text-align: right;'>
		<h2><b style='font-size: 50px; color: orange;'>SAI</b> <b style='font-size: 50px; color: white;'>Enterprises</b> &emsp;
			<br><i style='position: relative; color: white; font-size: 15px;'>LEADING AUTOMATION DISTRIBUTER</i> &emsp;</h2>
			
		</div>
		<div style='text-align: center; color: #3639a4;font-size: 25px;'>
			<b>Quotation</b>
		</div>
		
		<table width='100%' border='1' style='border-collapse:collapse;'>
          <tr>
            <td style='text-align: left;'>
            	<b>To,</b><br>
              $customer_Name <br>
              $address <br>
              $address2 <br>
              $city <br>
              $zipcode <br>
              
              MOB: $phone <br>
              EMAIL : $email
            </td>
            <td style='text-align: right;'>
            	<b>SAI ENTERPRISES</b><br>
              NO 107, 1<sup>ST</sup> FLOOR, MEI COLONY, <br>
              LAGGERE MAIN ROAD, <br>
              PEENYA INDUSTRIAL AREA, <br>
              BENGALURU- 560058. <br>
              Tel.: +91-80-28398596 <br>
              E-Mail: saientp_ajit@hotmail.com <br>
              Visit us @ www.saienpl.com
            </td>
          </tr>
      </table>
       <br>
        <br>
      <table cellspacing='0' cellpadding='0' style='width: 100%'>

          <tr>
            <th style='text-align:left;width:120px;'>Vendor Code</th>
            <td>: </td>
          </tr>
          <tr>
            <th style='text-align:left;'>Our Ref </th>
            <td>: $qtnNo</td>
          </tr>
          <tr>
            <th style='text-align:left;'>Your Ref</th>
            <td>: $enqNo Dt $date</td>
          </tr>
          <tr>
            <th style='text-align:left;'>Project Name</th>
            <td>: $project_name</td>
          </tr>
      </table>
      <br>
      <b>Sub: Quotation for the References mentioned above.<br><br></b>
      <div>
      	Dear Sir, <br>
      </div>
      <div>
      	Good day to you. At the outset we team SAI ENTERPRISES thank you very much for the support extended.<br>
      	Further to the inquiry, we are pleased to forward our best offer for the same.<br>
      </div>";
      for ($i=0; $i<count($career); $i++){
          $hsn_code = $career[$i]['hsn_code'];
          
          $itemname = $career[$i]['name'];
          $itemcode = $career[$i]['code'];
          $itemdesc = $career[$i]['description'];
           $itemtech = $career[$i]['technical_detail'];
          $itemprice = $career[$i]['unit_price'];
          $itemqnty = $career[$i]['quantity'];
          $itemtotal =(int) $career[$i]['total'];
          $gstamount =(int)(($itemtotal/100)* 18);
          $no = $i+1;
          
         $file_data.= "<table width='100%' border='1' style='border-collapse:collapse;page-break-inside:avoid'>

          <tr>
            <td style='text-align: left;' colspan='2'>
            	<b style='color: red;'>ITEM #$no</b>
            </td>
        </tr>
        <tr>
            <td style='text-align: left;'>ITEM Name :</td>
            <td style='text-align: right;'>- $itemname</td>
         </tr>
        <tr>
            <td style='text-align: left;'>ITEM CODE :</td>
            <td style='text-align: right;'>- $itemcode </td>
         </tr>
         <tr>
            <td style='text-align: left;'>ITEM DESCRIPTION :</td>
            <td style='text-align: right;'>- $itemdesc</td>
         </tr>
         <tr>
         <td colspan='2'>
           <table width='100%' border='1' style='border-collapse:collapse;'>
               <tr>
               <th>HSN CODE</th>
               <th>Quantity</th>
               <th>Unit Price</th>
               <th>Total Value</th>
               <th>GST - 18%</th>
               </tr>
                <tr>
               <td>$hsn_code</td>
               <td>$itemqnty</td>
               <td>$itemprice</td>
               <td>$itemtotal</td>
               <td>$gstamount</td>
               </tr>
           </table>
         </td>
         </tr>
      </table>";
      
      $file_data.="<table width='100%' border='1' style='border-collapse:collapse;page-break-inside:avoid'><tr><td>$itemtech</td></tr></table>";
      $file_data.="<hr/><br/>";
      }
      
 
      

      
      $file_data.="<table width='100%'  style='border-collapse:collapse;page-break-inside:avoid'>
      <tr>
         <th style='text-align:left;'>Terms and Condition</th></tr>";
      
      for($m=0;$m<count($quotationList);$m++){
          $termsandcondition = $quotationList[$m]['terms_condtion'];
          $n = $m+1;
      	$file_data.="<tr>
      	    <td>$n. $termsandcondition</td>
      	</tr>";
       } 
      $file_data.="</table> <br>";

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->WriteHTML($file_data);
$filename = "QUO"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'I');
exit;