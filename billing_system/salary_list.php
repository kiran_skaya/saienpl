<?php

include 'connection.php';

$viewquery = "SELECT a.*, b.employee_name from salary as a INNER JOIN employee as b ON a.employee_id=b.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['year'] = $row['year'];
  $career[$i]['employee_name'] = $row['employee_name'];
  $career[$i]['month'] = $row['month'];
  $career[$i]['store'] = $row['store'];
  $career[$i]['working_days'] = $row['working_days'];
  $career[$i]['leave_balance'] = $row['leave_balance'];
  $career[$i]['total_salary'] = $row['total_salary'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Salary</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_salary.php?sid='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Salary</h3>
                     <a href="add_salary.php" class="btn btn-primary">+ Create Salary</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                              <th>Sl. No</th>
                                <th>Year</th>
                          <th>Month</th>
                          <th>Employee Name</th>
                          <th>Store</th>
                          <th>Working Days</th>
                          <th>Leave Balance</th>
                          <th>Total Salary</th>
                          <th>Edit&nbsp; | &nbsp;Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = ucwords($career[$i]['employee_name']);
                            $year = $career[$i]['year'];
                            $month = $career[$i]['month'];
                            $store = $career[$i]['store'];
                            $work = $career[$i]['working_days'];
                            $leave = $career[$i]['leave_balance'];
                            $total = $career[$i]['total_salary'];
                            $eid = $career[$i]['employee_id'];
                            ?>
                        <tr>
                        <td><?php echo $i+1; ?></td>
                          <td><?php echo $year; ?></td>
                          <td><?php echo $month; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $store; ?></td>
                          <td><?php echo $work; ?></td>
                          <td><?php echo $leave; ?></td>
                          <td><?php echo $total; ?></td>
                          <td><a href="add_salary.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>