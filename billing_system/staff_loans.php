<?php

include 'connection.php';

$viewquery = "SELECT a.*, b.employee_name, b.employee_id from staff_loans as a INNER JOIN employee as b ON a.employee_id=b.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['loan_no'] = $row['loan_no'];
  $career[$i]['employee_name'] = $row['employee_name']."-".$row['employee_id'];
  $career[$i]['loan_description'] = $row['loan_description'];
  $career[$i]['loan_amount'] = $row['loan_amount'];
  $career[$i]['loan_issue_date'] = date("Y-m-d", strtotime($row['loan_issue_date']));
  $career[$i]['no_months_recover'] = $row['no_months_recover'];
  $career[$i]['balance'] = $row['balance'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Staff Loans</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_salary.php?sid='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Staff Loans</h3>
                     <a href="add_staff_loan.php" class="btn btn-primary">+ Create Staff Loan</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                              <th>Sl. No</th>
                                <th>Loan No.</th>
                          <th>Employee Name</th>
                          <th>Loan Description</th>
                          <th>Loan Amount </th>
                          <th>No of Months</th>
                          <th>Issue Date</th>
                          <th>Loan Balance</th>
                          <th>Add Received Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = ucwords($career[$i]['employee_name']);
                            $loanno = $career[$i]['loan_no'];
                            $loanDescription = $career[$i]['loan_description'];
                            $loanAmount = $career[$i]['loan_amount'];
                            $date = $career[$i]['loan_issue_date'];
                            $months = $career[$i]['no_months_recover'];
                            $balance = $career[$i]['balance'];
                            $eid = $career[$i]['employee_id'];
                            ?>
                        <tr>
                        <td><?php echo $i+1; ?></td>
                          <td><?php echo $loanno; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $loanDescription; ?></td>
                          <td><?php echo $loanAmount; ?></td>
                          <td><?php echo $date; ?></td>
                          <td><?php echo $months; ?></td>
                          <td><?php echo $balance; ?></td>
                          <td><a href="add_received_amount.php?id=<?php echo $id; ?>" class="btn btn-primary">ADD Amount</a></td>
                          <!-- <td><a href="add_staff_loan.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a></td> -->
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>