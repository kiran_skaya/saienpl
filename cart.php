<?php

include('connection.php');
		$sessionid = session_id();

 $query = "SELECT  i.item_name, t.*, sc.sub_category_name, c.category_id, c.category_name FROM item AS i INNER JOIN sub_category AS sc ON i.id_category=sc.sub_category_id INNER JOIN category AS c ON sc.id_category=c.category_id INNER JOIN temp_cart AS t ON t.id_item=i.id  WHERE t.session_id='$sessionid'";
 $categorysubname = mysqli_query($con,$query);
 $itemDisplayNameDetails = array();

 foreach ($categorysubname as $rowsubcategorya) {
    array_push($itemDisplayNameDetails, $rowsubcategorya);
 }


?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Cart | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper">

	<?php include('header.php');?>

		

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container">

					<div class="table-responsive mb-5">
						<div id='checkouttable'>
						</div>
					</div>

					

				</div>
			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

	<script>

    function changequantity(id,assignment) {

    	var quantity = $("#qtyid"+id).val();
    	$.post("temptablecart.php",
	  		{
			    id_item: id,
			    assignment : assignment,
			    qty : quantity
			},
		    function(data, status){
		       $("#checkouttable").html(data);
		    });

    }
     function deleteitem(id) {

    	$.post("tempdeleteitem.php",
	  		{
			    idsession: id
			},
		    function(data, status){
		       changequantity();
		    });

    }

    $(document).ready(function(){
	  changequantity();
	});

	</script>
 
</body>
</html>