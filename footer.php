<footer id="footer" class="dark">
			<!-- Copyrights
			============================================= -->
			<div id="copyrights">
				<div class="container">

					<div class="row col-mb-30">

						<div class="col-md-6 text-center text-md-left">
							Copyrights &copy; 2020 All Rights Reserved by Sai Enterprises.<br>
							<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
						</div>

						<div class="col-md-6 text-center text-md-right">
														<div class="clear"></div>

							<i class="icon-envelope2"></i> info@saienpl.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91 93797 94006 <span class="middot">&middot;</span> 
						</div>

					</div>

				</div>
			</div><!-- #copyrights end -->
		</footer><!-- #footer end -->
