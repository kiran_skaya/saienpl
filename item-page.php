<?php
include('connection.php');
$id = $_GET['id'];

 $query = "SELECT  i.item_name, i.* , sc.sub_category_name, c.category_id, c.category_name FROM item AS i INNER JOIN sub_category AS sc ON i.id_category=sc.sub_category_id INNER JOIN category AS c ON sc.id_category=c.category_id WHERE i.id='$id'";
 $categorysubname = mysqli_query($con,$query);
 $itemDisplayNameDetails = mysqli_fetch_array($categorysubname,MYSQLI_ASSOC);



 $querySubCategory = "select * from item where id = $id order by product_order";
$sub_result = mysqli_query($con,$querySubCategory);
$itemDisplay = array();
foreach ($sub_result as $rowsubcategorya) {
    $itemDisplay =  $rowsubcategorya;
}


 $image_query = "select * from item where id=$id";
                $image_result = mysqli_query($con,$image_query);


$specification_query = "select * from item_has_specification where id_item=$id";
$result        = $con->query($specification_query);
$specificaion_result = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($specificaion_result, $row);
}


                 $specification_query = "select * from item_has_technical_specification where id_item=$id";
$result        = $con->query($specification_query);
$technical_specificaion_result = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($technical_specificaion_result, $row);
}


?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Shop Single | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<?php include('header.php');?>

		<!-- <section id="page-title">

			<div class="container clearfix">
				<h1>Pink Printed Dress</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Shop</a></li>
					<li class="breadcrumb-item active" aria-current="page">Shop Single</li>
				</ol>
			</div>

		</section>

 -->
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="single-product">
						<div class="product">
							<div class="row gutter-40">

								<div class="col-md-5">

									<!-- Product Single - Gallery
									============================================= -->
									<div class="product-image">
										<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
											<div class="flexslider">
												<div class="slider-wrap" data-lightbox="gallery">

												<?php foreach ($image_result as $image){ ?>

													<?php if($image['image_1']) { ?>
													<div class="slide" data-thumb="<?php echo 'admin/uploads/'.$image[image_1];?>"><a href="<?php echo 'admin/uploads/'.$image[image_1];?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo 'admin/uploads/'.$image[image_1];?>"></a></div>

												<?php } ?>

													<?php if($image['image_2']) { ?>
													<div class="slide" data-thumb="<?php echo 'admin/uploads/'.$image[image_2];?>"><a href="<?php echo 'admin/uploads/'.$image[image_2];?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo 'admin/uploads/'.$image[image_2];?>"></a></div>

												<?php } ?>

												<?php if($image['image_3']) { ?>
													<div class="slide" data-thumb="<?php echo 'admin/uploads/'.$image[image_3];?>"><a href="<?php echo 'admin/uploads/'.$image[image_3];?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo 'admin/uploads/'.$image[image_3];?>"></a></div>

												<?php } ?>


												<?php if($image['image_4']) { ?>
													<div class="slide" data-thumb="<?php echo 'admin/uploads/'.$image[image_4];?>"><a href="<?php echo 'admin/uploads/'.$image[image_4];?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo 'admin/uploads/'.$image[image_4];?>"></a></div>

												<?php } ?>


												<?php } ?>
													
												</div>
											</div>
										</div>
									</div><!-- Product Single - Gallery End -->

								</div>

								<div class="col-md-7 product-desc">
									<div class="d-flex align-items-center justify-content-between">

          
										<!-- Product Single - Price
										============================================= -->
										<div class="product-price"><ins><?php echo $itemDisplayNameDetails['category_name']; ?></ins> / 



<a href="ATNSubCategories.php?id=<?php echo $itemDisplayNameDetails['category_id']; ?> "><?php echo $itemDisplayNameDetails['sub_category_name']; ?></a>

										</div><!-- Product Single - Price End -->

										

									</div>


									<div class="d-flex align-items-center justify-content-between">

          
										<!-- Product Single - Price
										============================================= -->
										<div class="product-price"><ins><?php echo $itemDisplay['item_name'];?></ins></div><!-- Product Single - Price End -->

										

									</div>

									<div class="line"></div>


									<!-- Product Single - Short Description
									============================================= -->
									<p><?php echo $itemDisplay['description']; ?></p>
																		<div class="line"></div>
									<button class="button button-3d mt-0 float-right" onclick="addtocart(<?php echo $itemDisplay['id']; ?>)">Add to Cart</button>

									

								</div>

								

								<div class="w-10"></div>

								<div class="col-12 mt-1">

									<div class="tabs clearfix mb-0" id="tab-1">

										<ul class="tab-nav clearfix">
											<li><a href="#tabs-1"><span class="d-none d-md-inline-block"> Product Details</span></a></li>
											<li><a href="#tabs-2"><span class="d-none d-md-inline-block"> Specifications</span></a></li>
											<!--<li><a href="#tabs-3"><span class="d-none d-md-inline-block"> Catalogs</span></a></li>-->
											<li><a href="#tabs-4"><span class="d-none d-md-inline-block"> Manuals</span></a></li>
											<li><a href="#tabs-5"><span class="d-none d-md-inline-block"> Installation Video</span></a></li>
										</ul>

										<div class="tab-container">

											<div class="tab-content clearfix" id="tabs-1">
												 <p><?php echo $itemDisplay['details']; ?>
                  </p>
											</div>
											<div class="tab-content clearfix" id="tabs-2">
												<table class="table table-striped">
							                        <thead>
							                            <tr>
							                                <th colspan="2">Specifications</th>
							                            </tr>
							                        </thead>
							                        <tbody>
							                        <?php foreach ($specificaion_result as $category) { ?>

							                            <tr>
							                                <td><?php echo $category['label']; ?></td>
							                              
							                                 <td><?php echo $category['value']; ?></td>
							                            </tr>
							                        <?php }?>


							                        </tbody>
							                    </table>
											</div>
										
											<div class="tab-content clearfix" id="tabs-4">
												<embed src="<?php echo 'admin/uploads/'.$itemDisplay['brochure']; ?>" type="application/pdf" width="90%" height="500px" />
											</div>
											<div class="tab-content clearfix" id="tabs-5">
												 <iframe width="560" height="300" src="<?php echo $itemDisplay['video_name2']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
											</div>

										</div>

									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
	<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

    <script>
     function addtocart(id) {
     	$.post("dualtempcart.php",
	  		{
			    id_item: id
			},
		    function(data, status){
		       alert("Data: " + data + "\nStatus: " + status);
		    });
     }
    </script>

</body>
</html>