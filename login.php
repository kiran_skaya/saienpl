<?php
include('connection.php');

if($_POST) {

	if($_POST['login-form-submit']=='login') {

          $email = $_POST['login-form-username'];
          $password = $_POST['login-form-password'];


		 $careerqueryresult = mysqli_query($con,$sqlInsert);
		 $emailCheck = "select * from user where email = '$email' and password='$password' ";
		$sub_result = mysqli_query($con,$emailCheck);
		foreach ($sub_result as $rowsubcategorya) {
		    $_SESSION['user']= $rowsubcategorya;
		    echo "<script>parent.location='dashboard.php'</script>";
		    exit;

		}


	}
	if($_POST['register-form-submit']=='register') {

		$name = $_POST['register-form-name'];
		$email = $_POST['register-form-email'];
		$phone = $_POST['register-form-phone'];
		$password = $_POST['register-form-password'];
		$repassword = $_POST['register-form-repassword'];

        $emailCheck = "select * from user where email = $email ";
		$sub_result = mysqli_query($con,$emailCheck);
		foreach ($sub_result as $rowsubcategorya) {
		    echo "<script>alert('Email already exist, Please login ')</script>";
		    echo "<script>parent.location='login.php'</script>";
		    exit;
		}
		$date = date('Y-m-d');

		 $sqlInsert = "Insert into user(name,password,phone,email,created_date) values ('$name','$password','$phone','$email','$date') ";
		 $careerqueryresult = mysqli_query($con,$sqlInsert);
		 $emailCheck = "select * from user where email = '$email' ";
		$sub_result = mysqli_query($con,$emailCheck);
		foreach ($sub_result as $rowsubcategorya) {
		    $_SESSION['user']= $rowsubcategorya;
		}


	}
}


?>




<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Login - Layout 3 | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

			<?php include('header.php');?>
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="accordion accordion-lg mx-auto mb-0 clearfix" style="max-width: 550px;">

						<div class="accordion-header">
							<div class="accordion-icon">
								<i class="accordion-closed icon-lock3"></i>
								<i class="accordion-open icon-unlock"></i>
							</div>
							<div class="accordion-title">
								Login to your Account
							</div>
						</div>
						<div class="accordion-content clearfix">
							<form id="login-form" name="login-form" class="row mb-0"  method="post">
								<div class="col-12 form-group">
									<label for="login-form-username">Username:</label>
									<input type="text" id="login-form-username" name="login-form-username" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="login-form-password">Password:</label>
									<input type="password" id="login-form-password" name="login-form-password" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<button class="button button-3d button-black m-0" id="login-form-submit" name="login-form-submit" value="login">Login</button>
									<a href="#" class="float-right">Forgot Password?</a>
								</div>
							</form>
						</div>

						<div class="accordion-header">
							<div class="accordion-icon">
								<i class="accordion-closed icon-user4"></i>
								<i class="accordion-open icon-ok-sign"></i>
							</div>
							<div class="accordion-title">
								New Signup? Register for an Account
							</div>
						</div>
						<div class="accordion-content clearfix">
							<form id="register-form" name="register-form" class="row mb-0" action="" method="post">
								<div class="col-12 form-group">
									<label for="register-form-name">Name:</label>
									<input type="text" id="register-form-name" name="register-form-name" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="register-form-email">Email Address:</label>
									<input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="register-form-username">Choose a Username:</label>
									<input type="text" id="register-form-username" name="register-form-username" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="register-form-phone">Phone:</label>
									<input type="text" id="register-form-phone" name="register-form-phone" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="register-form-password">Choose Password:</label>
									<input type="password" id="register-form-password" name="register-form-password" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="register-form-repassword">Re-enter Password:</label>
									<input type="password" id="register-form-repassword" name="register-form-repassword" value="" class="form-control" />
								</div>

								<div class="col-12 form-group">
									<button class="button button-3d button-black m-0" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
								</div>
							</form>
						</div>

					</div>

				</div>
			</div>
		</section><!-- #content end -->

		<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>