<?php
include('connection.php');

$id_user = $_SESSION['user']['id'];
$invoice_amount = '0';

$invoice_number = "-";
$invoice_date = date('Y-m-d');
$id_shipping_address = $_SESSION['shippingId'];
$id_billing_address = $_SESSION['billingId'];


		$invoiceSql = "Insert into invoice(id_user,invoice_amount,invoice_number,invoice_date,id_shipping_address,id_billing_address) values
		 ('$id_user','$invoice_amount','$invoice_number','$invoice_date','$id_shipping_address','$id_billing_address')";

		 $careerqueryresult = mysqli_query($con,$invoiceSql);

		 $invoiceId = $con->insert_id;



	$sessionid = session_id();

 $query = "SELECT  i.item_name, t.*, sc.sub_category_name, c.category_id, c.category_name FROM item AS i INNER JOIN sub_category AS sc ON i.id_category=sc.sub_category_id INNER JOIN category AS c ON sc.id_category=c.category_id INNER JOIN temp_cart AS t ON t.id_item=i.id  WHERE t.session_id='$sessionid'";
 $categorysubname = mysqli_query($con,$query);
 $itemDisplayNameDetails = array();
$totalFinalPrice = 0; 
 foreach ($categorysubname as $rowsubcategorya) {
$item = $rowsubcategorya['id_item'];
$price = $rowsubcategorya['price'];
$qty = $rowsubcategorya['qty'];
$total_price = $rowsubcategorya['total_price'];
$totalFinalPrice = $totalFinalPrice + $total_price;

	$invoiceSql = "Insert into invoice_details(id_invoice,id_item,price,qty,total_price) values
		 ('$invoiceId','$item','$price','$qty','$total_price')";

		 $careerqueryresult = mysqli_query($con,$invoiceSql);

}

$invoicenumber = "INV".date('Y').$invoiceId;
$updateSql = "Update invoice set invoice_amount='$totalFinalPrice',invoice_number='$invoicenumber' where id='$invoiceId'";
mysqli_query($con,$updateSql);


?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Checkout | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

	<?php include('header.php');?>

	
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<form id="shipping-form" name="shipping-form" class="row mb-0" action="" method="post">
					    <div class="row">
						<div class="col-sm-12" style="text-align: center;">
							 <h4>Thank you for placing the order</h4>

						</div>
				    </form>

				</div>
			</div>
		</section><!-- #content end -->

	<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>