<?php
include('connection.php');
$sql = "Select * from project_categories";
$result = mysqli_query($con,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $proj[$i]['id'] = $row['id'];
  $proj[$i]['project_category_name'] = $row['project_category_name'];
  $proj[$i]['project_category_image'] = 'admin/uploads/'.$row['project_category_image'];
  $i++;
}


?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Portfolio Single - Gallery Full Width | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
	<?php include('header.php');?>


		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="row">
						<div class="heading-block fancy-title border-bottom-0 title-bottom-border">
								<h4>Our <span>Project List</span>.</h4>
							</div>

												

                    </div>

					<div class="row">

					



					<?php for($i=0;$i<count($proj);$i++) { ?>
					<div class="col-3">

						<div class="oc-item">
							<div class="portfolio-item">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="admin/uploads/1.jpg" alt="Open Imagination">
									</a>
									
								</div>
								<div class="portfolio-desc">
									<span  class="product-price textcolor"><?php echo $proj[$i]['project_category_name'];?></span>
								</div>
							</div>
						</div>
											</div>

					<?php } ?> 



					
					</div>


					

				</div>


			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
	<?php include('footer.php');?>
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>