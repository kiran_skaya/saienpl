<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Services | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
	<?php include('header.php');?>
		<!-- Page Title
		============================================= -->
		<!-- <section id="page-title"> -->

		<!-- 	<div class="container clearfix">
				<h1>Services</h1>
				<span>We provide Amazing Solutions</span>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Services</li>
				</ol>
			</div>

		</section> -->

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
						<div class="row">
						<div class="heading-block fancy-title border-bottom-0 title-bottom-border">
								<h4>Our <span>Services</span>.</h4>
							</div>

												

                    </div>



					<div class="row col-mb-50">

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-crop i-alt"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Quality<span class="subtitle">Quality Assured Manufacturing Inc.</span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-tint i-alt"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Integrity<span class="subtitle">16.7+ Million on your fingertips</span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-text-width i-alt"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Accountability<span class="subtitle">We believe in accepting responsibility</span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-ok"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Team Work<span class="subtitle">Team success takes precedence over individual achievement</span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-thumbs-up i-alt"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Improvement<span class="subtitle">We listen to customer and improve the things</span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-4">
							<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
								<div class="fbox-icon">
									<a href="#"><i class="icon-eye i-alt"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Relationship<span class="subtitle">Develop the long standing business relationships</span></h3>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

	<script>

	</script>

</body>
</html>