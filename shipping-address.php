<?php
include('connection.php');

		$sessionid = session_id();
 $query = "SELECT  i.item_name, t.*, sc.sub_category_name, c.category_id, c.category_name FROM item AS i INNER JOIN sub_category AS sc ON i.id_category=sc.sub_category_id INNER JOIN category AS c ON sc.id_category=c.category_id INNER JOIN temp_cart AS t ON t.id_item=i.id  WHERE t.session_id='$sessionid'";
 $categorysubname = mysqli_query($con,$query);
 $itemDisplayNameDetails = array();

 foreach ($categorysubname as $rowsubcategorya) {
    array_push($itemDisplayNameDetails, $rowsubcategorya);
}



if($_POST) {

	$iduser = $_SESSION['user']['id'];

	$billingFormName = $_POST['billing-form-name'];
    $billingCompanyName = $_POST['billing-form-companyname'];
    $billingAddress = $_POST['billing-form-address'];
    $billingAddress2 = $_POST['billing-form-adress'];
    $billingCity = $_POST['billing-form-city'];
    $billingEmail = $_POST['billing-form-email'];
    $billingPhone = $_POST['billing-form-phone'];
    $shippingName = $_POST['shipping-form-name'];
    $shippingCompanyName = $_POST['shipping-form-companyname'];
    $shippingAddress = $_POST['shipping-form-address'];
    $shippingAddress2 = $_POST['shipping-form-adress'];
    $shippingCity = $_POST['shipping-form-city'];
    $shippingMessage = $_POST['shipping-form-message'];



		  $sqlInsert = "Insert into billing_address(name,companyname,address,address_two,city,email,phone,id_user) 
		      values ('$billingFormName','$billingCompanyName','$billingAddress','$billingAddress2','$billingCity','$billingEmail','$billingPhone','$iduser') "; 
		 $careerqueryresult = mysqli_query($con,$sqlInsert);

		 $_SESSION['billingId'] = $con->insert_id;



		 $sqlInsert = "Insert into shipping_address(name,companyname,address,address_two,city,message,id_user) 
		      values ('$shippingName','$shippingCompanyName','$shippingAddress','$shippingAddress2','$shippingCity','$shippingMessage','$iduser') "; 
		 $careerqueryresult = mysqli_query($con,$sqlInsert);
		  $_SESSION['shippingId'] = $con->insert_id;


		  echo "<script>parent.location='payment-page.php'</script>";
		  exit;


}


?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Checkout | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

	<?php include('header.php');?>

	
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<form id="shipping-form" name="shipping-form" class="row mb-0" action="" method="post">
					    <div class="row col-mb-50 gutter-50">
						<div class="col-lg-6">
							<h3>Billing Address</h3>



								<div class="col-12 form-group">
									<label for="billing-form-name">Name:</label>
									<input type="text" id="billing-form-name" name="billing-form-name" value="" class="sm-form-control" />
								</div>


								<div class="w-100"></div>

								<div class="col-12 form-group">
									<label for="billing-form-companyname">Company Name:</label>
									<input type="text" id="billing-form-companyname" name="billing-form-companyname" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="billing-form-address">Address:</label>
									<input type="text" id="billing-form-address" name="billing-form-address" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<input type="text" id="billing-form-address2" name="billing-form-adress" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="billing-form-city">City / Town</label>
									<input type="text" id="billing-form-city" name="billing-form-city" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="billing-form-name">Email Address:</label>
									<input type="text" id="billing-form-email" name="billing-form-email" value="" class="sm-form-control" />
								</div>


								<div class="col-md-12 form-group">
									<label for="billing-form-phone">Phone:</label>
									<input type="text" id="billing-form-phone" name="billing-form-phone" value="" class="sm-form-control" />
								</div>

						</div>

						<div class="col-lg-6">
							<h3>Shipping Address</h3>

							

								<div class="col-md-12 form-group">
									<label for="shipping-form-name">Name:</label>
									<input type="text" id="shipping-form-name" name="shipping-form-name" value="" class="sm-form-control" />
								</div>

								<div class="w-100"></div>

								<div class="col-12 form-group">
									<label for="shipping-form-companyname">Company Name:</label>
									<input type="text" id="shipping-form-companyname" name="shipping-form-companyname" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="shipping-form-address">Address:</label>
									<input type="text" id="shipping-form-address" name="shipping-form-address" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<input type="text" id="shipping-form-address2" name="shipping-form-adress" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="shipping-form-city">City / Town</label>
									<input type="text" id="shipping-form-city" name="shipping-form-city" value="" class="sm-form-control" />
								</div>

								<div class="col-12 form-group">
									<label for="shipping-form-message">Notes <small>*</small></label>
									<textarea class="sm-form-control" id="shipping-form-message" name="shipping-form-message" rows="6" cols="30"></textarea>
								</div>

						</div>

						<div class="w-100"></div>

						<div class="col-lg-6">
							<h4>Your Orders</h4>

							<div class="table-responsive">
								<table class="table cart">
									<thead>
										<tr>
											<th class="cart-product-name">Product</th>
											<th class="cart-product-quantity">Quantity</th>
											<th class="cart-product-subtotal">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php for($k=0;$k<count($itemDisplayNameDetails);$k++) { 
											$categoryName = $itemDisplayNameDetails[$k]['category_name'];
								 	$subcategoryName = $itemDisplayNameDetails[$k]['sub_category_name'];
								 	$itemName = $itemDisplayNameDetails[$k]['item_name'];
								 	$price = $itemDisplayNameDetails[$k]['price'];
								 	$qty = $itemDisplayNameDetails[$k]['qty'];
								 	$tempid = $itemDisplayNameDetails[$k]['id'];
								 	$totalPrice = $qty * $price;
								 	$finaltotaprice = $totalPrice + $finaltotaprice;
								 	$j = $i + 1;
								 	?>
										<tr class="cart_item">
											

											<td class="cart-product-name">
												<a href="#"><?php echo $itemDisplayNameDetails[$k]['item_name'];?></a>
											</td>

											<td class="cart-product-quantity" style="width: 25%">
												<div class="quantity clearfix">
													<?php echo $qty .' * '.$price   ;?>
												</div>
											</td>

											<td class="cart-product-subtotal">
												<span class="amount"><?php echo number_format($totalPrice);?></span>
											</td>
										</tr>

									<?php } ?> 
										
									</tbody>

								</table>
							</div>
						</div>

						<div class="col-lg-6">
							<h4>Cart Totals</h4>

							<div class="table-responsive">
								<table class="table cart">
									<tbody>
										<tr class="cart_item">
											<td class="border-top-0 cart-product-name">
												<strong>Cart Subtotal</strong>
											</td>

											<td class="border-top-0 cart-product-name">
												<span class="amount"><?php echo number_format($finaltotaprice);?></span>
											</td>
										</tr>
										<tr class="cart_item">
											<td class="cart-product-name">
												<strong>Shipping</strong>
											</td>

											<td class="cart-product-name">
												<span class="amount">Free Delivery</span>
											</td>
										</tr>
										<tr class="cart_item">
											<td class="cart-product-name">
												<strong>Total</strong>
											</td>

											<td class="cart-product-name">
												<span class="amount color lead"><strong><?php echo number_format($finaltotaprice);?></strong></span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

							
							<button type="submit" class="button button-3d float-right">Place Order</submit>
						</div>
					    </div>
				    </form>

				</div>
			</div>
		</section><!-- #content end -->

	<?php include('footer.php');?>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>